<?php // AVTPL
return [
    // AVTPL config.ConfigConsoleLocalDev BEGIN
    'components' => [
        'urlManager' => [
            'baseUrl' => 'http://localhost/lassa/',
            'scriptUrl' => 'http://localhost/lassa/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    // AVTPL config.ConfigConsoleLocalDev END
];
