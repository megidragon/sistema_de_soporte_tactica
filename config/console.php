<?php // AVTPL
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'es',
    'controllerNamespace' => 'app\commands',
    'timeZone' => 'America/Argentina/Buenos_Aires',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/../app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        // AVTPL EVO INI ConfigI18nConsole
                        // AVTPL EVO END ConfigI18nConsole
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
