<?php // AVTPL

// AVTPL EVO INI ParamAddVariables
// AVTPL EVO END ParamAddVariables

return [
    // AVTPL general.EmailAdmin
    'adminEmail' => 'lassa@kaizen2b.com',
    // AVTPL general.EmailSupport
    'supportEmail' => 'lassa@kaizen2b.com',
    // AVTPL general.EmailSupport
    'notifEmail' => 'lassa@kaizen2b.com',
    // AVTPL general.HomeAjaxDefault
    'homeAjaxDefault' => '1',
    // AVTPL general.ParamUserPwdResetTokenExpire
    'user.passwordResetTokenExpire' => 300,
    // AVTPL general.ParamPermissionNotShown
    'permissions-not-shown' => ['setup', 'manage', 'report'],
    // AVTPL general.ParamPermissionSystem
    'permissions-system' => ['user-notif-view-del', 'user-notif-del', 'user-notif-view'],
    // AVTPL general.ParamPermissionOnOff
    'permissions-on-off' => ['incidente-create'],
    // AVTPL general.ParamMaxRecordsPrint
    'maxRecordsPrint' => '200',
    // AVTPL general.ParamMaxRecordsExport
    'maxRecordsExport' => '1000',
    // AVTPL general.ParamAlertShowMs
    'alertShowMs' => 5000,
    // AVTPL general.ParamAlertWaitMs
    'alertWaitMs' => 1000,
    // AVTPL general.ParamAlertTypes
    'alertTypes' => '',
    // These parameters must be modified manually in case of need to leave the site offline
    'in_maintenance' => 0,
    'maintenance_message' => 'Estará en línea nuevamente a las 0 horas. Disculpe las molestias',
    // AVTPL EVO INI ParamAdditionals
    'jsversion' => '8',
    'import_flag_tactica' => '/runtime/import/tactica.flag',
    'import_flag_web' => '/runtime/import/web.flag',
    'import_tipo_filepath' => '/runtime/import/lassa-exportacion-incidentestipos.txt',
    'import_tipo_rowdelim' => "\t<ENTER>\r\n",
    'import_tipo_celldelim' => "\t<TAB>\t",
    'import_empresa_filepath' => '/runtime/import/lassa-exportacion-empresas.txt',
    'import_empresa_rowdelim' => "\t<ENTER>\r\n",
    'import_empresa_celldelim' => "\t<TAB>\t",
    'import_contacto_filepath' => '/runtime/import/lassa-exportacion-contactos.txt',
    'import_contacto_rowdelim' => "\t<ENTER>\r\n",
    'import_contacto_celldelim' => "\t<TAB>\t",
    'import_producto_filepath' => '/runtime/import/lassa-exportacion-productos.txt',
    'import_producto_rowdelim' => "\t<ENTER>\r\n",
    'import_producto_celldelim' => "\t<TAB>\t",
	'import_soporte_filepath' => '/runtime/import/lassa-exportacion-soporte.txt',
	'import_soporte_rowdelim' => "\t<ENTER>\r\n",
	'import_soporte_celldelim' => "\t<TAB>\t",
    'import_logimp_filepath' => '/runtime/import/lassa-exportacion-log-importacion.txt',
    'import_logimp_rowdelim' => "\t<ENTER>\r\n",
    'import_logimp_celldelim' => "\t<TAB>\t",
    'export_ticket_savepath' => '/runtime/export',
    'export_ticket_filename' => 'lassa-importacion-tickets.xls',
    'export_flag_tactica' => '/runtime/export/tactica.flag',
    'export_flag_web' => '/runtime/export/web.flag',
    // AVTPL EVO END ParamAdditionals
];
