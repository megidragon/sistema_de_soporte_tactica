<?php // AVTPL
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use kartik\datecontrol\Module;

return [
    'id' => 'app-web',
    'name' => 'Latin America Solutions S.A.',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'es',
    'controllerNamespace' => 'app\controllers',
    'on beforeRequest' => function () {
            if (Yii::$app->params['in_maintenance'] == 1) {
                Yii::$app->catchAll = [
                  'site/offline',
                  'message' => Yii::$app->params['maintenance_message'],
                ];
            }
        },
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
            'authTimeout' => 1800,
            'identityCookie' => [
                'name' => 'LassaUser',
            ],
        ],
        'session' => [
            'name' => 'LassaSession',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['deb'],
                    'logFile' => '@app/runtime/logs/deb.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],
            ],
        ],
        'errorHandler' => [
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        //'basePath' => '@app/messages',
                        //'sourceLanguage' => 'en-US',
                        'fileMap' => [
                        'app' => 'app.php',
                        'app/alerts' => 'alerts.php',
                        'app/emails' => 'emails.php',
                        'app/errors' => 'errors.php',
                        'app/mail' => 'mail.php',
                        'app/permissions' => 'permissions.php',
                        'app/societe' => 'societe.php',
                        'app/societe_params' => 'societe_params.php',
                        'app/user' => 'user.php',
                        'app/user_add' => 'user_add.php',
                        // AVTPL EVO INI ConfigI18nWeb
                        'app/tipo' => 'tipo.php',
                        'app/subtipo' => 'subtipo.php',
                        'app/elemento' => 'elemento.php',
                        'app/empresa' => 'empresa.php',
                        'app/contacto' => 'contacto.php',
                        'app/prioridad' => 'prioridad.php',
                        'app/producto' => 'producto.php',
                        'app/ticket' => 'ticket.php',
                        // AVTPL EVO END ConfigI18nWeb
                    ],
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd/MM/yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => '$',
        ],
    ],
    'defaultRoute' => 'home',
    'params' => $params,
    'modules' => [
        'audit' => [
            'class' => 'bedezign\yii2\audit\Audit',
            'userIdentifierCallback' => ['app\models\User', 'userIdentifierCallback'],
            'panels' => [
                'audit/db' => ['maxAge' => 7],
                'audit/log' => ['maxAge' => 7],
                'audit/profiling' => ['maxAge' => 7],
                'audit/request' => ['maxAge' => 7],
                'audit/error' => ['maxAge' => 30],
                'audit/javascript' => ['maxAge' => 30],
                'audit/mail' => ['maxAge' => NULL],
                'audit/extra' => ['maxAge' => 30],
            ],
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',

            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                Module::FORMAT_DATE => 'dd/MM/yyyy',
                Module::FORMAT_TIME => 'HH:mm:ss',
                Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss', 
            ],

            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                Module::FORMAT_DATE => 'yyyy-MM-dd',
                // Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
                Module::FORMAT_TIME => 'php:H:i:s',
                Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],

            // set your display timezone
            //'displayTimezone' => 'America/Argentina/Buenos_Aires', // when I set it to here, it shows a day before
            //'displayTimezone' => 'Asia/Kolkata',

            // set your timezone for date saved to db
            'saveTimezone' => 'UTC',

            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,

            // use ajax conversion for processing dates from display format to save format.
            'ajaxConversion' => true,

            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true]], // example
                Module::FORMAT_DATETIME => [], // setup if needed
                Module::FORMAT_TIME => [], // setup if needed
            ],

            // custom widget settings that will be used to render the date input instead of kartik\widgets,
            // this will be used when autoWidget is set to false at module or widget level.
            'widgetSettings' => [
                Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker', // example
                    'options' => [
                        'dateFormat' => 'php:d-M-Y',
                        'options' => ['class'=>'form-control'],
                    ]
                ]
            ]
        // other settings
        ]
    ]
];
