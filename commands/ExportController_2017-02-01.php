<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Ticket;
use app\models\TicketExport;

class ExportController extends Controller
{
    private static $errors = [];

    public function actionIndex()
    {
        self::exportTicket();
        self::sendErrors();
    }

    private static function exportTicket() {
        $last_imported_tkt = 0;
        $filename = Yii::$app->basePath . Yii::$app->params['import_logimp_filepath'];
        if (file_exists($filename)) {
            $cell_delim = Yii::$app->params['import_logimp_celldelim'];
            $lines = file($filename, FILE_IGNORE_NEW_LINES);
            if (count($lines) == 2) {
                $cells = explode($cell_delim, utf8_encode($lines[1]));
                if (count($cells) == 7) {
                    $last_imported_tkt = $cells[6];
                } else {
                    array_push(self::$errors, "Error en la cantidad de campos del archivo \"$filename\"");
                }
            } else {
                array_push(self::$errors, "Error en la cantidad de líneas \"$filename\"");
            }
        }
        $ticket_file = Yii::$app->basePath . Yii::$app->params['export_ticket_savepath'] . '/' . Yii::$app->params['export_ticket_filename'];
        $process = false;
        $tickets = [];
        if (is_readable($ticket_file)) {
            $tickets_in_file = count(Ticket::findAll(['baja' => 1]));
            $tickets_to_export = count(Ticket::find()->andFilterCompare('id', $last_imported_tkt, '>')->all());
            $process = ($tickets_in_file != $tickets_to_export);
        } else {
            Ticket::updateAll(['baja' => 2], ['baja' => 1]);
            $process = count(Ticket::find()->where(['baja' => 0])->andFilterCompare('id', $last_imported_tkt, '>')->all());
        }
        if ($process) {
            $tickets = Ticket::find()->where(['baja' => [0, 1, 2]])->andFilterCompare('id', $last_imported_tkt, '>')->all();
        }
        $prioridades = ['ALTA' => 0, 'MEDIA' => 1, 'BAJA' => 2];
        $export = [];
        if (count($tickets)) {
            if (is_readable($ticket_file)) unlink($ticket_file);
            foreach ($tickets as $ticket) {
                $ticketExport = new TicketExport();
                $ticketExport->dummy = 'dummy';
                $ticketExport->id = $ticket->id;
                $ticketExport->empresa = $ticket->idEmpresa->nombre;
                $ticketExport->contactonombre = $ticket->idContacto->nombre;
                $ticketExport->contactoapellido = $ticket->idContacto->apellido;
                $ticketExport->fechaorigenweb = $ticket->fecha_creacion . ' ' . $ticket->hora_creacion;
                $ticketExport->origen = 'WEB';
                $ticketExport->prioridad = $prioridades[$ticket->prioridad];
                $ticketExport->tipo = $ticket->tipo;
                $ticketExport->incidentetipo = $ticket->idTipo->descripcion;
                $ticketExport->incidentesubtipo = $ticket->idSubtipo->descripcion;
                $ticketExport->incidenteelemento = $ticket->idElemento->descripcion;
                $ticketExport->codigoproducto = $ticket->idProducto->codigo;
                $ticketExport->problemadescripcion = $ticket->descripcion;
                $ticketExport->fecharequerida = $ticket->fecha_vencimiento . ' ' . $ticket->hora_vencimiento;
                array_push($export, $ticketExport);
                $ticket->baja = 1;
                $ticket->save();
            }
            \moonland\phpexcel\Excel::export([
                'models' => $export, 
                'format' => 'Excel5',
                'columns' => ['dummy', 'id', 'empresa', 'contactonombre', 'contactoapellido', 'fechaorigenweb', 'origen', 'prioridad', 'tipo', 'incidentetipo', 'incidentesubtipo', 'incidenteelemento', 'codigoproducto', 'problemadescripcion', 'fecharequerida'],
                'headers' => ['dummy' => 'DUMMY', 'id' => 'ID', 'empresa' => 'EMPRESA', 'contactonombre' => 'CONTACTONOMBRE', 'contactoapellido' => 'CONTACTOAPELLIDO', 'fechaorigenweb' => 'FECHAORIGENWEB', 'origen' => 'ORIGEN', 'prioridad' => 'PRIORIDAD', 'tipo' => 'TIPO', 'incidentetipo' => 'INCIDENTETIPO', 'incidentesubtipo' => 'INCIDENTESUBTIPO', 'incidenteelemento' => 'INCIDENTEELEMENTO', 'codigoproducto' => 'CODIGOPRODUCTO', 'problemadescripcion' => 'PROBLEMADESCRIPCION', 'fecharequerida' => 'FECHAREQUERIDA'],
                'savePath' => Yii::$app->basePath . Yii::$app->params['export_ticket_savepath'],
                'fileName' => Yii::$app->params['export_ticket_filename'],
            ]);
        }
    }

    private static function sendErrors() {
        if (count(self::$errors) == 0) return;
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Errores en importación')
            ->setTextBody(implode("\n", self::$errors))
            ->send();
        $fn = fopen(\Yii::$app->basePath . '/runtime/logs/exporterror.log', 'w');
        fwrite($fn, implode("\n", self::$errors));
        fclose($fn);
    }
}
