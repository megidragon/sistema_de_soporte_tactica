<?php

namespace app\commands;

use app\models\TicketSearch;
use Yii;
use yii\console\Controller;
use app\models\Tipo;
use app\models\Subtipo;
use app\models\Elemento;
use app\models\Empresa;
use app\models\Contacto;
use app\models\Producto;
use app\models\Prioridad;
use app\models\Ticket;
use app\models\User;
use app\models\Societe;
use yii\validators\StringValidator;

class ImportController extends Controller
{
    private static $errors = [];

    public function actionIndex()
    {
        self::importTipo();
        self::importEmpresa();
        self::importContacto();
        self::importProducto();
        self::importSoporte();
        self::sendErrors();
    }

    private static function importTipo() {
        $tipo_desc_col = 1;
        $subtipo_desc_col = 3;
        $elemento_desc_col = 5;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_tipo_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_tipo_rowdelim'];
        $cell_delim = Yii::$app->params['import_tipo_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $tipos_id = [];
        $subtipos_id = [];
        $elementos_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 6) {
                $tipo = Tipo::findOne(['descripcion' => $cells[$tipo_desc_col], 'baja' => 0]);
                if (is_null($tipo)) {
                    $tipo = new Tipo();
                    $tipo->descripcion = self::stringLength($tipo, 'descripcion', $cells[$tipo_desc_col]);
                    $tipo->id_societe = 1;
                    $tipo->baja = 0;
                    if (!$tipo->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($tipo) . " en \"$line\"");
                }
                array_push($tipos_id, $tipo->id);
                $subtipo = Subtipo::findOne(['id_tipo' => $tipo->id, 'descripcion' => $cells[$subtipo_desc_col], 'baja' => 0]);
                if (is_null($subtipo)) {
                    $subtipo = new Subtipo();
                    $subtipo->descripcion = self::stringLength($subtipo, 'descripcion', $cells[$subtipo_desc_col]);
                    $subtipo->id_tipo = $tipo->id;
                    $subtipo->id_societe = 1;
                    $subtipo->baja = 0;
                    if (!$subtipo->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($subtipo) . " en \"$line\"");
                }
                array_push($subtipos_id, $subtipo->id);
                $elemento = Elemento::findOne(['id_subtipo' => $subtipo->id, 'descripcion' => $cells[$elemento_desc_col], 'baja' => 0]);
                if (is_null($elemento)) {
                    $elemento = new Elemento();
                    $elemento->descripcion = self::stringLength($elemento, 'descripcion', $cells[$elemento_desc_col]);
                    $elemento->id_subtipo = $subtipo->id;
                    $elemento->id_societe = 1;
                    $elemento->baja = 0;
                    if (!$elemento->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($elemento) . " en \"$line\"");
                }
                array_push($elementos_id, $elemento->id);
            } else {
                if ($line) echo "ERROR ".$filename." ".date("Y-m-d H:i:s")." Error de cantidad de columnas \n";
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Tipo::find(), $tipos_id);
        self::removeNotUsed(Subtipo::find(), $subtipos_id);
        self::removeNotUsed(Elemento::find(), $elementos_id);
    }

    private static function importEmpresa() {
        $empresa_id_col = 0;
        $nombre_col = 1;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_empresa_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_empresa_rowdelim'];
        $cell_delim = Yii::$app->params['import_empresa_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $empresas_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 2) {
                $object = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col]]);
                if (is_null($object)) {
                    $object = new empresa();
                    $object->tacticaid = $cells[$empresa_id_col];
                    $object->id_societe = 1;
                }
                $object->nombre = $cells[$nombre_col];
                $object->baja = 0;
                if ($object->save()) {
                    array_push($empresas_id, $object->id);
                } else {
                    array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                }
            } else {
                if ($line) echo "ERROR ".$filename." ".date("Y-m-d H:i:s")." Error de cantidad de columnas \n";
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Empresa::find(), $empresas_id);
    }

    private static function importContacto() {
            	$empresa_id_col = 0;
        $id_col = 2;
        $nombre_col = 3;
        $apellido_col = 4;
        $email_col = 5;
        $sup_tec_col = 7;
        $sup_adm_col = 8;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_contacto_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_contacto_rowdelim'];
        $cell_delim = Yii::$app->params['import_contacto_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $objects_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 9)
            {
                $empresa = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col], 'baja' => 0]);
                if (!is_null($empresa)) {
                    $object = Contacto::findOne(['id_empresa' => $empresa->id, 'tacticaid' => $cells[$id_col]]);
                    if (is_null($object)) {
                        $object = new contacto();
                        $object->tacticaid = $cells[$id_col];
                        $object->id_empresa = $empresa->id;
                        $object->id_societe = 1;
                    }
                    $object->nombre = self::stringLength($object, 'nombre', $cells[$nombre_col]);
                    $object->apellido = self::stringLength($object, 'apellido', $cells[$apellido_col]);
                    $object->email = $cells[$email_col];
                    $object->supervisor_tecnico = $cells[$sup_tec_col];
                    $object->supervisor_administ = $cells[$sup_adm_col];
                    $object->baja = 0;
                    if ($object->save()) {
                        array_push($objects_id, $object->id);
                    } else {
                        array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                    }
                } else {
                    array_push(self::$errors, "$filename: Empresa no existe en \"$line\"");
                }
            } else {
                if ($line) echo "ERROR ".$filename.date("Y-m-d H:i:s")." Error de cantidad de columnas \n";
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Contacto::find(), $objects_id);
    }

    private static function importProducto() {
        $empresa_id_col = 0;
        $id_col = 1;
        $codigo_col = 2;
        $nroserie_col = 3;
        $nombreysector_col = 4;
        $datosred_col = 5;
        $macaddress_col = 6;
        $fabricante_col = 8;
        $posicion_col = 9;
        $prioridad_col = 10;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_producto_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_producto_rowdelim'];
        $cell_delim = Yii::$app->params['import_producto_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $objects_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 11) {
                $id_empresa = -1;
                if (str_replace(' ', '', $cells[$empresa_id_col])) {
                    $empresa = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col], 'baja' => 0]);
                    if (!is_null($empresa)) $id_empresa = $empresa->id;
                } else {
                    $id_empresa = null;
                }
                if ($id_empresa != -1) {
                    $object = Producto::findOne(['tacticaid' => $cells[$id_col]]);
                    if (is_null($object)) {
                        $object = new Producto();
                        $object->tacticaid = $cells[$id_col];
                        $object->id_societe = 1;
                    }
                    $prioridad = Prioridad::findOne(['codigo' => $cells[$prioridad_col], 'baja' => 0]);
                    if (is_null($prioridad)) {
                        $prioridad = new Prioridad();
                        $prioridad->codigo = $cells[$prioridad_col];
                        $prioridad->unidad = 'DIA';
                        $prioridad->cantidad = 0;
                        $prioridad->id_societe = 1;
                        $prioridad->baja = 0;
                        $prioridad->save();
                    }
                    $object->codigo = $cells[$codigo_col];
                    $object->nro_serie = self::stringLength($object, 'nro_serie', $cells[$nroserie_col]);
                    $object->nombre_sector = self::stringLength($object, 'nombre_sector', $cells[$nombreysector_col]);
                    $object->datos_red = self::stringLength($object, 'datos_red', $cells[$datosred_col]);
                    $object->mac_address = self::stringLength($object, 'mac_address', $cells[$macaddress_col]);
                    $object->posicion = self::stringLength($object, 'posicion', $cells[$posicion_col]);
                    $object->fabricante = self::stringLength($object, 'fabricante', $cells[$fabricante_col]);
                    $object->id_prioridad = $prioridad->id;
                    $object->id_empresa = $id_empresa;
                    $object->baja = 0;
                    if ($object->save()) {
                        array_push($objects_id, $object->id);
                    } else {
                        array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                    }
                } else {
                    array_push(self::$errors, "$filename: Empresa no existe en \"$line\"");
                }
            } else {
                if ($line) echo "ERROR ".$filename." ".date("Y-m-d H:i:s")." Error de cantidad de columnas \n";
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Producto::find(), $objects_id);
    }

    private static function importSoporte() {
	    $id_ticket_col                  = 0;
	    $tactica_estado_col             = 2;
        $fecha_modificacion_col         = 3;
        $usuario_modificacion_col       = 4;
        $fecha_cierre_col               = 5;
        $usuario_cierre_col             = 6;
	    $tactica_solucion_desc_col      = 7;
        $tactica_soporte_usuario_asign_col  = 11;
        $tactica_soporte_prioridad_col      = 13;
        $tactica_soporte_descripcion_col    = 14;

        $filename = \Yii::$app->basePath . Yii::$app->params['import_soporte_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_soporte_rowdelim'];
        $cell_delim = Yii::$app->params['import_soporte_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
//echo "SOPORTE FECHA ".date("Y-m-d H:i:s")." CANTIDAD DE FILAS ".count($lines)."\n";
        $objects_id = [];
        $objects_cerrados = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 15)        // CANTIDAD DE COLUMNAS USABLES QUE VIENEN EN EL TXT
            {
                $object = Ticket::findOne(['id' => $cells[$id_ticket_col]]);
                if (!is_null($object) && ($object->baja !== 4)){
                    $object->id = $cells[$id_ticket_col];

                    $atributesToUpdate = [];
                    
                    if(self::validToUpdateField($cells[$usuario_modificacion_col])){
						if($object->tactica_user_modificacion!==self::stringLength($object, 'tactica_user_modificacion', $cells[$usuario_modificacion_col])){
							$object->tactica_user_modificacion = self::stringLength($object, 'tactica_user_modificacion', $cells[$usuario_modificacion_col]);
							array_push($atributesToUpdate, 'tactica_user_modificacion');
						}
                    }
					
	                if(self::validToUpdateField($cells[$fecha_modificacion_col])) {

						$testdatetimefechamodif = date_create_from_format('Y-m-d H:i:s', $cells[$fecha_modificacion_col]);
// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
						if($testdatetimefechamodif && $testdatetimefechamodif->format('Y-m-d H:i:s') === $cells[$fecha_modificacion_col]){

							if($object->tactica_fecha_modificacion!==self::getFechaFromString($cells[$fecha_modificacion_col])){
								$object->tactica_fecha_modificacion = self::getFechaFromString($cells[$fecha_modificacion_col]);
								array_push($atributesToUpdate, 'tactica_fecha_modificacion');
							}
							
							if($object->tactica_hora_modificacion!==self::getHoraFromString($cells[$fecha_modificacion_col])){
								$object->tactica_hora_modificacion = self::getHoraFromString($cells[$fecha_modificacion_col]);
								array_push($atributesToUpdate, 'tactica_hora_modificacion');
							}
						}
	                }
	                
	                if(self::validToUpdateField($cells[$tactica_soporte_usuario_asign_col])) {
						if($object->tactica_soporte_usuario_asign !== $cells[$tactica_soporte_usuario_asign_col]){
							$object->tactica_soporte_usuario_asign  = $cells[$tactica_soporte_usuario_asign_col];
							array_push($atributesToUpdate, 'tactica_soporte_usuario_asign');
						}
	                }
	
	                if(self::validToUpdateField($cells[$tactica_soporte_prioridad_col])) {
						if($object->prioridad!==$cells[$tactica_soporte_prioridad_col]){
							$object->prioridad      = $cells[$tactica_soporte_prioridad_col];
							array_push($atributesToUpdate, 'prioridad');
						}
	                }
	
	                if(self::validToUpdateField($cells[$tactica_soporte_descripcion_col])) {
						if($object->descripcion !== $cells[$tactica_soporte_descripcion_col]){
							$object->descripcion    = $cells[$tactica_soporte_descripcion_col];
							array_push($atributesToUpdate, 'descripcion');
						}
	                }

                    if($cells[$tactica_estado_col] == 1){
                        $object->baja = 4;
                        array_push($atributesToUpdate, 'baja');
                        array_push($objects_cerrados, $object->id);

						if(self::validToUpdateField($cells[$usuario_cierre_col])) {
							$object->tactica_user_cierre  = $cells[$usuario_cierre_col];
							array_push($atributesToUpdate, 'tactica_user_cierre');
						}

						if(self::validToUpdateField($cells[$fecha_cierre_col])) {
							$testdatetimefechacierre = date_create_from_format('Y-m-d H:i:s', $cells[$fecha_cierre_col]);
// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
							if($testdatetimefechacierre && $testdatetimefechacierre->format('Y-m-d H:i:s') === $cells[$fecha_cierre_col]){
								$object->tactica_fecha_cierre = self::getFechaFromString($cells[$fecha_cierre_col]);
								$object->tactica_hora_cierre = self::getHoraFromString($cells[$fecha_cierre_col]);
								array_push($atributesToUpdate, 'tactica_fecha_cierre');
								array_push($atributesToUpdate, 'tactica_hora_cierre');
							}
						}

						if(self::validToUpdateField($cells[$tactica_solucion_desc_col])) {
							$object->tactica_solucion_descr = $cells[$tactica_solucion_desc_col];
							array_push($atributesToUpdate, 'tactica_solucion_descr');							
						}
					}else{
						if(count($atributesToUpdate)>0) {
                            $object->baja = 3;
                            array_push($atributesToUpdate, 'baja');
                        }
                    }

					if (count($atributesToUpdate)>0){
						echo "SOPORTE ".date("Y-m-d H:i:s")." Ticket: ".$object->id." cantidad campos: ".count($atributesToUpdate)." ";
//print_r ($atributesToUpdate);
						try{
							if($object->update(true, $atributesToUpdate)) {
	//                      if($object->save()) {
								if ($object->baja == 4) {
									echo " CERRADO. ";
								}else{
									echo " MODIFICADO. ";
								}
								echo "Ticket actualizado en MYSQL en forma CORRECTA. \n";
								array_push($objects_id, $object->id);
							}else{
								echo "Error al actualizar el ticket. ".$object->getErrors()."\n";
							}
						}catch (\Exception $e){
							echo "Excepcion al actualizar el ticket. ".$e->getMessage()."\n";
	//                        array_push(self::$errors, "Error on update ticket: ".$e->getMessage());
						}
					}
			
                }else{
					  if (is_null($object)){
                          echo "Ticket con id: ".$cells[$id_ticket_col] ." no encontrado \n";
                          array_push(self::$errors, "$filename: Ticket no existe en \"$line\"");
					  }  
                }
            } else {
                      if ($line) echo "ERROR ".$filename." ".date("Y-m-d H:i:s")." Error de cantidad de columnas \n";
                      if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        

        if(count($objects_cerrados) > 0){
            self::sendMailTicketsCerrados($objects_cerrados);
        }
    }
    
    private static function validToUpdateField($field)
    {
		if(!is_null($field)){
			return ($field !== "" && strtolower($field) !== 'null' && $field !== " ");
		}
    }

    private static function removeNotUsed($classFind, $id_array) {
        $objects = $classFind->where(['baja' => 0])->all();
        foreach ($objects as $object) {
            if (array_search($object->id, $id_array) === false) {
                $object->baja = 1;
                $object->save();
            }
        }
    }

    private static function sendErrors() {
        if (count(self::$errors) == 0) return;
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Errores en importación')
            ->setTextBody(implode("\n", self::$errors))
            ->send();
        $fn = fopen(\Yii::$app->basePath . '/runtime/logs/importerror.log', 'w');
        fwrite($fn, implode("\n", self::$errors));
        fclose($fn);
    }

    /**
     * @param Ticket[] $objects_cerrados
     */
    private static function sendMailTicketsCerrados($objects_cerrados)
    {
        $objects    = [];
        for($i = 0; $i < count($objects_cerrados); $i++){
            $id = $objects_cerrados[$i];
            $object = Ticket::find()->where(['ticket.id' => $id])->
                    joinWith('idEmpresa idEmpresa')->
                    joinWith('idContacto idContacto')->
                    joinWith('idTipo idTipo')->
                    joinWith('idSubtipo idSubtipo')->
                    joinWith('idElemento idElemento')->
                    joinWith('idProducto idProducto')->
                    one();

            $objects[] = $object;
        }

        foreach($objects as $object)
        {
            $contact = Contacto::findIdentity($object->id_contacto);
            $societe = Societe::findOne($contact->id_societe);
            $send_cc = [$societe->email];
            $supervisor_fld = ($object->tipo == 'TECNICO') ? 'supervisor_tecnico' : 'supervisor_administ';
            $contactos = Contacto::find()->where(['id_empresa' => $object->id_empresa, $supervisor_fld => 1, 'baja' => 0])->all();
            foreach ($contactos as $contacto) {
                array_push($send_cc, $contacto->email);
            }

            $objectPrioridad    = Prioridad::find()->where(['prioridad.id' => $object->idProducto->id_prioridad])->one();

            $ticket_body    =   "El ticket generado en la web de Latin America Solutions SA ha sido CERRADO <br><br><br>";    //INTRODUCCION
            $ticket_body    .=  "Numero de ticket: ".$object->id."<br>";
            $ticket_body    .=  "Estado del ticket: Cerrado"."<br>";
            $ticket_body    .=  "Fecha y hora de creacion: ".$object->fecha_creacion." ".$object->hora_creacion."<br>";
            $ticket_body    .=  "Fecha y hora de vencimiento: ".$object->fecha_vencimiento." ".$object->hora_vencimiento."<br>";
            $ticket_body    .=  "Empresa cliente: ".$object->idEmpresa->nombre."<br>";
            $ticket_body    .=  "Contacto cliente: ".$object->idContacto->nombre." ".$object->idContacto->apellido."<br>";
            $ticket_body    .=  "Tipo pedido: ".$object->tipo."<br>";
            $ticket_body    .=  "Prioridad del ticket: ".$object->prioridad."<br>";
            $ticket_body    .=  "Clasificador tipo: ".$object->idTipo->descripcion."<br>";
            $ticket_body    .=  "Clasificador subtipo: ".$object->idSubtipo->descripcion."<br>";
            $ticket_body    .=  "Clasificador elemento: ".$object->idElemento->descripcion."<br>";
            $ticket_body    .=  "Dispositivo: ".$object->idProducto->codigo."<br>";
            $ticket_body    .=  "Descripción 2: ".$object->idProducto->datos_red."<br>";
            $ticket_body    .=  "Descripción 3: ".$object->idProducto->mac_address."<br>";
            $ticket_body    .=  "Prioridad del producto: ".$objectPrioridad->codigo." (".$objectPrioridad->cantidad.") ".$objectPrioridad->unidad."<br>";
            $ticket_body    .=  "Numero de serie: ".$object->idProducto->nro_serie."<br>";
            $ticket_body    .=  "Nombre del sector: ".$object->idProducto->nombre_sector."<br>";
            $ticket_body    .=  "Fabricante: ".$object->idProducto->fabricante."<br>";
            $ticket_body    .=  "Posicion: ".$object->idProducto->posicion."<br>";
            $ticket_body    .=  "Descripcion pedido: ".$object->descripcion."<br>";
            $ticket_body    .=  "Fecha y hora de cierre: ".$object->tactica_fecha_cierre." ".$object->tactica_hora_cierre."<br>";
            $ticket_body    .=  "Usuario Cierre: ".$object->tactica_user_cierre."<br>";
            $ticket_body    .=  "Solucion descripcion: ".$object->tactica_solucion_descr."<br>";

            $ticket_body    .=  "<br><br>";
            $ticket_body    .=  "Quedamos a disposicion por cualquier consulta o necesidad.<br>
                                    Muchas gracias, Latin America Solutions SA. <br>
                                    http://lassa.com.ar/soporte";

            $subject    = Yii::t('app/ticket', 'LASSA WEB SOPORTE Ticket #{id} CERRADO', ['id' => $object->id]);
            
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => $societe->name])
                ->setTo($contact->email)
                ->setCc($send_cc)
                ->setSubject($subject)
                ->setHtmlBody($ticket_body)
                ->send();
        }
    }

    private static function removeSpecialChars($array) {
        $newarray = [];
        foreach ($array as $value) {
            $value = str_replace("\r", "", $value);
            $value = str_replace("\n", "", $value);
            $value = str_replace("\t", "", $value);
            $value = str_replace("\x00", "", $value);
            $value = str_replace("\x01", "", $value);
            array_push($newarray, $value);
        }
        return $newarray;
    }

    private static function getObjectErrors($object) {
        $errors_txt = '';
        foreach ($object->getErrors() as $variable => $errors) {
            $errors_txt .= $variable . ': ';
            foreach ($errors as $error) {
                $errors_txt .= $error . ', ';
            }
        }
        return $errors_txt;
    }

    private static function stringLength($model, $fieldName, $value) {
        foreach ($model->getValidators($fieldName) as $validator) {
            if ($validator instanceof StringValidator && $validator->max !== null) {
                $result = substr($value, 0, $validator->max);
                if ($result === false) $result = ""; // workaround for problem found on production
                return $result;
            }
        }
        return $value;
    }

    private static function getFechaFromString($string){
        $fecha  = substr($string, 0, 10);
        if(self::fechaEnCero($fecha)){
            $fecha  = null;
		}
        return $fecha;
    }
    private static function getHoraFromString($string){
        $hora   = substr($string, 11, 8);
        if(self::horaEnCero($hora)){
            $hora  = null;
		}
        return $hora;
    }
    private static function fechaEnCero($fecha){
        return $fecha == "0000-00-00";
    }
    private static function horaEnCero($hora){
        return $hora == "00:00:00";
    }
}
