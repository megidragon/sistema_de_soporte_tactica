<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\Tipo;
use app\models\Subtipo;
use app\models\Elemento;
use app\models\Empresa;
use app\models\Contacto;
use app\models\Producto;
use app\models\Prioridad;
use app\models\Ticket;
use yii\validators\StringValidator;

class ImportController extends Controller
{
    private static $errors = [];

    public function actionIndex()
    {
//        self::importTipo();
//        self::importEmpresa();
//        self::importContacto();
//        self::importProducto();
        self::importSoporte();
        self::sendErrors();
    }

    private static function importTipo() {
        $tipo_desc_col = 1;
        $subtipo_desc_col = 3;
        $elemento_desc_col = 5;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_tipo_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_tipo_rowdelim'];
        $cell_delim = Yii::$app->params['import_tipo_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $tipos_id = [];
        $subtipos_id = [];
        $elementos_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 6) {
                $tipo = Tipo::findOne(['descripcion' => $cells[$tipo_desc_col], 'baja' => 0]);
                if (is_null($tipo)) {
                    $tipo = new Tipo();
                    $tipo->descripcion = self::stringLength($tipo, 'descripcion', $cells[$tipo_desc_col]);
                    $tipo->id_societe = 1;
                    $tipo->baja = 0;
                    if (!$tipo->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($tipo) . " en \"$line\"");
                }
                array_push($tipos_id, $tipo->id);
                $subtipo = Subtipo::findOne(['id_tipo' => $tipo->id, 'descripcion' => $cells[$subtipo_desc_col], 'baja' => 0]);
                if (is_null($subtipo)) {
                    $subtipo = new Subtipo();
                    $subtipo->descripcion = self::stringLength($subtipo, 'descripcion', $cells[$subtipo_desc_col]);
                    $subtipo->id_tipo = $tipo->id;
                    $subtipo->id_societe = 1;
                    $subtipo->baja = 0;
                    if (!$subtipo->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($subtipo) . " en \"$line\"");
                }
                array_push($subtipos_id, $subtipo->id);
                $elemento = Elemento::findOne(['id_subtipo' => $subtipo->id, 'descripcion' => $cells[$elemento_desc_col], 'baja' => 0]);
                if (is_null($elemento)) {
                    $elemento = new Elemento();
                    $elemento->descripcion = self::stringLength($elemento, 'descripcion', $cells[$elemento_desc_col]);
                    $elemento->id_subtipo = $subtipo->id;
                    $elemento->id_societe = 1;
                    $elemento->baja = 0;
                    if (!$elemento->save()) array_push(self::$errors, "$filename: " . self::getObjectErrors($elemento) . " en \"$line\"");
                }
                array_push($elementos_id, $elemento->id);
            } else {
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Tipo::find(), $tipos_id);
        self::removeNotUsed(Subtipo::find(), $subtipos_id);
        self::removeNotUsed(Elemento::find(), $elementos_id);
    }

    private static function importEmpresa() {
        $empresa_id_col = 0;
        $nombre_col = 1;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_empresa_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_empresa_rowdelim'];
        $cell_delim = Yii::$app->params['import_empresa_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $empresas_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 2) {
                $object = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col]]);
                if (is_null($object)) {
                    $object = new empresa();
                    $object->tacticaid = $cells[$empresa_id_col];
                    $object->id_societe = 1;
                }
                $object->nombre = $cells[$nombre_col];
                $object->baja = 0;
                if ($object->save()) {
                    array_push($empresas_id, $object->id);
                } else {
                    array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                }
            } else {
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Empresa::find(), $empresas_id);
    }

    private static function importContacto() {
            	$empresa_id_col = 0;
        $id_col = 2;
        $nombre_col = 3;
        $apellido_col = 4;
        $email_col = 5;
        $sup_tec_col = 7;
        $sup_adm_col = 8;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_contacto_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_contacto_rowdelim'];
        $cell_delim = Yii::$app->params['import_contacto_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $objects_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 9)
            {
                $empresa = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col], 'baja' => 0]);
                if (!is_null($empresa)) {
                    $object = Contacto::findOne(['id_empresa' => $empresa->id, 'tacticaid' => $cells[$id_col]]);
                    if (is_null($object)) {
                        $object = new contacto();
                        $object->tacticaid = $cells[$id_col];
                        $object->id_empresa = $empresa->id;
                        $object->id_societe = 1;
                    }
                    $object->nombre = self::stringLength($object, 'nombre', $cells[$nombre_col]);
                    $object->apellido = self::stringLength($object, 'apellido', $cells[$apellido_col]);
                    $object->email = $cells[$email_col];
                    $object->supervisor_tecnico = $cells[$sup_tec_col];
                    $object->supervisor_administ = $cells[$sup_adm_col];
                    $object->baja = 0;
                    if ($object->save()) {
                        array_push($objects_id, $object->id);
                    } else {
                        array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                    }
                } else {
                    array_push(self::$errors, "$filename: Empresa no existe en \"$line\"");
                }
            } else {
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Contacto::find(), $objects_id);
    }

    private static function importProducto() {
        $empresa_id_col = 0;
        $id_col = 1;
        $codigo_col = 2;
        $nroserie_col = 3;
        $nombreysector_col = 4;
        $datosred_col = 5;
        $macaddress_col = 6;
        $fabricante_col = 8;
        $posicion_col = 9;
        $prioridad_col = 10;
        $filename = \Yii::$app->basePath . Yii::$app->params['import_producto_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_producto_rowdelim'];
        $cell_delim = Yii::$app->params['import_producto_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $objects_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 11) {
                $id_empresa = -1;
                if (str_replace(' ', '', $cells[$empresa_id_col])) {
                    $empresa = Empresa::findOne(['tacticaid' => $cells[$empresa_id_col], 'baja' => 0]);
                    if (!is_null($empresa)) $id_empresa = $empresa->id;
                } else {
                    $id_empresa = null;
                }
                if ($id_empresa != -1) {
                    $object = Producto::findOne(['tacticaid' => $cells[$id_col]]);
                    if (is_null($object)) {
                        $object = new Producto();
                        $object->tacticaid = $cells[$id_col];
                        $object->id_societe = 1;
                    }
                    $prioridad = Prioridad::findOne(['codigo' => $cells[$prioridad_col], 'baja' => 0]);
                    if (is_null($prioridad)) {
                        $prioridad = new Prioridad();
                        $prioridad->codigo = $cells[$prioridad_col];
                        $prioridad->unidad = 'DIA';
                        $prioridad->cantidad = 0;
                        $prioridad->id_societe = 1;
                        $prioridad->baja = 0;
                        $prioridad->save();
                    }
                    $object->codigo = $cells[$codigo_col];
                    $object->nro_serie = self::stringLength($object, 'nro_serie', $cells[$nroserie_col]);
                    $object->nombre_sector = self::stringLength($object, 'nombre_sector', $cells[$nombreysector_col]);
                    $object->datos_red = self::stringLength($object, 'datos_red', $cells[$datosred_col]);
                    $object->mac_address = self::stringLength($object, 'mac_address', $cells[$macaddress_col]);
                    $object->posicion = self::stringLength($object, 'posicion', $cells[$posicion_col]);
                    $object->fabricante = self::stringLength($object, 'fabricante', $cells[$fabricante_col]);
                    $object->id_prioridad = $prioridad->id;
                    $object->id_empresa = $id_empresa;
                    $object->baja = 0;
                    if ($object->save()) {
                        array_push($objects_id, $object->id);
                    } else {
                        array_push(self::$errors, "$filename: " . self::getObjectErrors($object) . " en \"$line\"");
                    }
                } else {
                    array_push(self::$errors, "$filename: Empresa no existe en \"$line\"");
                }
            } else {
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
        self::removeNotUsed(Producto::find(), $objects_id);
    }

    private static function importSoporte() {
	    $id_ticket_col              = 0;
	    $tactica_estado_col         = 2;
        $fecha_modificacion_col     = 3;
        $usuario_modificacion_col   = 4;
        $fecha_cierre_col           = 5;
        $usuario_cierre_col         = 6;
	    $tactica_solucion_desc_col  = 7;
        $empresa_id_col             = 9;

        $filename = \Yii::$app->basePath . Yii::$app->params['import_soporte_filepath'];
        if (!file_exists($filename)) return;
        $row_delim = Yii::$app->params['import_soporte_rowdelim'];
        $cell_delim = Yii::$app->params['import_soporte_celldelim'];
        $lines = explode($row_delim, substr(strstr(file_get_contents($filename), "\r\n"), 2));
        $objects_id = [];
        foreach ($lines as $line) {
            $cells = self::removeSpecialChars(explode($cell_delim, utf8_encode($line)));
            if (count($cells) == 11)        // CANTIDAD DE COLUMNAS USABLES QUE VIENEN EN EL TXT
            {
                $object = Ticket::findOne(['id' => $cells[$id_ticket_col]]);
                if (!is_null($object)) {
                    $object->id = $cells[$id_ticket_col];
                    $object->tactica_user_modificacion  = $cells[$usuario_modificacion_col];
                    $object->tactica_fecha_modificacion = self::getFechaFromString($cells[$fecha_modificacion_col]);
                    $object->tactica_hora_modificacion  = self::getHoraFromString($cells[$fecha_modificacion_col]);

                    $object->tactica_user_cierre  = $cells[$usuario_cierre_col];
                    $object->tactica_fecha_cierre = self::getFechaFromString($cells[$fecha_cierre_col]);
                    $object->tactica_hora_cierre  = self::getHoraFromString($cells[$fecha_cierre_col]);

                    $object->tactica_solucion_descr = $cells[$tactica_solucion_desc_col];

                    $atributesToUpdate = ['tactica_user_modificacion', 'tactica_fecha_modificacion', 'tactica_hora_modificacion',
                        'tactica_user_cierre', 'tactica_fecha_cierre', 'tactica_hora_cierre', 'tactica_solucion_descr'];

                    if($cells[$tactica_estado_col] == 1){
                        $object->baja = 4;
                        array_push($atributesToUpdate, 'baja');
                    }

                    echo "Trabajando sobre ticket: ".$object->id."\n";
                    echo "Modificacion -> Usuario: ".$object->tactica_user_modificacion;
                    echo " Fecha: ".$object->tactica_fecha_modificacion;
                    echo " Hora: ".$object->tactica_hora_modificacion;
                    echo "\n";

                    try{
                        if($object->update(true, $atributesToUpdate)) {
                            echo "Ticket actualizado \n";
                            array_push($objects_id, $object->id);
                        }
                    }catch (\Exception $e){
//                        array_push(self::$errors, "Error on update ticket: ".$e->getMessage());
                    }
                }else{
                    echo "Ticket con id: ".$cells[$id_ticket_col] ." no encontrado \n";
                    array_push(self::$errors, "$filename: Ticket no existe en \"$line\"");
                }
            } else {
//                echo "Error de cantidad de lineas. ".count($lines)." lineas \n";
                if ($line) array_push(self::$errors, "$filename: Cantidad de columnas incorrecta en \"$line\"");
            }
        }
    }

    private static function removeNotUsed($classFind, $id_array) {
        $objects = $classFind->where(['baja' => 0])->all();
        foreach ($objects as $object) {
            if (array_search($object->id, $id_array) === false) {
                $object->baja = 1;
                $object->save();
            }
        }
    }

    private static function sendErrors() {
        if (count(self::$errors) == 0) return;
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Errores en importación')
            ->setTextBody(implode("\n", self::$errors))
            ->send();
        $fn = fopen(\Yii::$app->basePath . '/runtime/logs/importerror.log', 'w');
        fwrite($fn, implode("\n", self::$errors));
        fclose($fn);
    }

    private static function removeSpecialChars($array) {
        $newarray = [];
        foreach ($array as $value) {
            $value = str_replace("\r", "", $value);
            $value = str_replace("\n", "", $value);
            $value = str_replace("\t", "", $value);
            $value = str_replace("\x00", "", $value);
            $value = str_replace("\x01", "", $value);
            array_push($newarray, $value);
        }
        return $newarray;
    }

    private static function getObjectErrors($object) {
        $errors_txt = '';
        foreach ($object->getErrors() as $variable => $errors) {
            $errors_txt .= $variable . ': ';
            foreach ($errors as $error) {
                $errors_txt .= $error . ', ';
            }
        }
        return $errors_txt;
    }

    private static function stringLength($model, $fieldName, $value) {
        foreach ($model->getValidators($fieldName) as $validator) {
            if ($validator instanceof StringValidator && $validator->max !== null) {
                $result = substr($value, 0, $validator->max);
                if ($result === false) $result = ""; // workaround for problem found on production
                return $result;
            }
        }
        return $value;
    }

    private static function getFechaFromString($string){
        $fecha  = substr($string, 0, 10);
        if(self::fechaEnCero($fecha)){
            $fecha  = null;
        }
        return $fecha;
    }
    private static function getHoraFromString($string){
        $hora   = substr($string, 11, 8);
        if(self::horaEnCero($hora)){
            $hora  = null;
        }
        return $hora;
    }
    private static function fechaEnCero($fecha){
        return $fecha == "0000-00-00";
    }
    private static function horaEnCero($hora){
        return $hora == "00:00:00";
    }
}
