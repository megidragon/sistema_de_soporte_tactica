<?php

namespace app\commands;

use app\models\TicketUpload;
use Yii;
use yii\console\Controller;
use app\models\Ticket;
use app\models\TicketExport;

class ExportController extends Controller
{
    private static $errors = [];

    public function actionIndex()
    {
        self::exportTicket();
//        self::sendErrors();
    }

    private static function exportTicket() {
        $last_imported_tkt = 0;
        $filename = Yii::$app->basePath . Yii::$app->params['import_logimp_filepath'];
        if (file_exists($filename)) {
            $cell_delim = Yii::$app->params['import_logimp_celldelim'];
            $lines = file($filename, FILE_IGNORE_NEW_LINES);
            if (count($lines) == 2) {
                $cells = explode($cell_delim, utf8_encode($lines[1]));
                if (count($cells) == 7) {
                    $last_imported_tkt = $cells[6];
                } else {
                    array_push(self::$errors, "Error en la cantidad de campos del archivo \"$filename\"");
                }
            } else {
                array_push(self::$errors, "Error en la cantidad de líneas \"$filename\"");
            }
        }
        $ticket_file = Yii::$app->basePath . Yii::$app->params['export_ticket_savepath'] . '/' . Yii::$app->params['export_ticket_filename'];
        $process = false;
        $tickets = [];
        if (is_readable($ticket_file)) {
            $tickets_in_file = count(Ticket::findAll(['baja' => 1]));
            $tickets_to_export = count(Ticket::find()->andFilterCompare('id', $last_imported_tkt, '>')->all());
            $process = ($tickets_in_file != $tickets_to_export);
        } else {
            Ticket::updateAll(['baja' => 2], ['baja' => 1]);
            $process = count(Ticket::find()->where(['baja' => 0])->andFilterCompare('id', $last_imported_tkt, '>')->all());
        }
        if ($process) {
            $tickets = Ticket::find()->where(['baja' => [0, 1, 2]])->andFilterCompare('id', $last_imported_tkt, '>')->all();
        }

        /* SE AGREGA PARA MOSTRAR LAS URL CORRESPONDIENTES A LOS ARCHIVOS ADJUNTOS DEL TICKET */
        $archivosAdjuntos   = TicketUpload::find()->andFilterCompare('id_ticket', $last_imported_tkt, '>')->all();

        // die(var_dump($tickets));
        $prioridades = ['ALTA' => 0, 'MEDIA' => 1, 'BAJA' => 2];
        $export = [];
        if (count($tickets) > 0) {
            if (is_readable($ticket_file)) unlink($ticket_file);
            foreach ($tickets as $ticket) {

                $archivosAdjuntosDeTicket   = "";
                foreach($archivosAdjuntos as $ticketUpload)
                {
                    if($ticket->id == $ticketUpload->id_ticket){

//                        $rutaFile       = Yii::getAlias('@rootweb')."/".\app\models\TicketUpload::PATH.$ticketUpload->upload;

// 2018-09-19 CPS COMENTADO 
//                        $rutaFile       = "http://lassa.com.ar/soporte/".Yii::$app->params['dir_uploads'].$ticketUpload->upload;
//                        $rutaFile       = Yii::$app->basePath."/".Yii::$app->params['dir_uploads'].$ticketUpload->upload;

// 2018-09-19 CPS MODIFICADO

	                  //DETECTO EN QUE DIRECTORIO ESTÁ EL ARCHIVO
	                  $numDirectorio  = 1;
	                  $miles          = floor($ticket->id / 1000) * 1000;
	                  $numDirectorio  +=  $miles;
	                  $numCompletoDirectorio = 100000000 + $numDirectorio;
	                  $directorio = substr($numCompletoDirectorio, 1, 8)."/";

//                          $pathFile       = Yii::$app->basePath."/web/".$rutaFile;
//                          $pathFile       = \app\models\TicketUpload::PATH;

                          $pathFile       = Yii::$app->params['dir_uploads'];

                          $nombreArchivo  = $ticketUpload->upload;

//                          $rutaFile       = Yii::$app->params['dir_uploads'].$directorio."/".$nombreArchivo;
//                          $downloadFile   = "../".$rutaFile;

                        $rutaFile       = "http://lassa.com.ar/soporte/".$pathFile.$directorio.$nombreArchivo;

                        $archivosAdjuntosDeTicket   .=  $rutaFile." \n";
                    }
                }

                $ticketExport = new TicketExport();
                $ticketExport->dummy = 'dummy';
                $ticketExport->id = $ticket->id;
                $ticketExport->empresa = $ticket->idEmpresa->nombre;
                $ticketExport->contactonombre = $ticket->idContacto->nombre;
                $ticketExport->contactoapellido = $ticket->idContacto->apellido;
                $ticketExport->fechaorigenweb = $ticket->fecha_creacion . ' ' . $ticket->hora_creacion;
                $ticketExport->origen = 'WEB';
                $ticketExport->prioridad = $prioridades[$ticket->prioridad];
                $ticketExport->tipo = $ticket->tipo;
                $ticketExport->incidentetipo = $ticket->idTipo->descripcion;
                $ticketExport->incidentesubtipo = $ticket->idSubtipo->descripcion;
                $ticketExport->incidenteelemento = $ticket->idElemento->descripcion;
                $ticketExport->codigoproducto = $ticket->idProducto->codigo;

//2018-09-19 CPS MODIFICADO

                if(!is_null($archivosAdjuntosDeTicket) && $archivosAdjuntosDeTicket !== ""){
                    $ticketExport->problemadescripcion = $ticket->descripcion."\n<LINK-ARCHIVOS-ADJUNTOS-INICIO>\n".$archivosAdjuntosDeTicket."<LINK-ARCHIVOS-ADJUNTOS-FIN>";
                }else{
                    $ticketExport->problemadescripcion = $ticket->descripcion;
                }

                $ticketExport->fecharequerida = $ticket->fecha_vencimiento . ' ' . $ticket->hora_vencimiento;

//2018-09-19 CPS COMENTADO
//                $ticketExport->tacticasoluciondescripcion   = $ticket->tactica_solucion_descr;
//                $ticketExport->archivosadjuntos = $archivosAdjuntosDeTicket;
                array_push($export, $ticketExport);
                $ticket->baja = 1;
                $ticket->save();
            }
            \moonland\phpexcel\Excel::export([
                'models' => $export, 
                'format' => 'Excel5',

//2018-09-19 CPS MODIFICADO

//                'columns' => ['dummy', 'id', 'empresa', 'contactonombre', 'contactoapellido', 'fechaorigenweb', 'origen', 'prioridad', 'tipo', 'incidentetipo', 'incidentesubtipo', 'incidenteelemento', 'codigoproducto', 'problemadescripcion', 'fecharequerida', 'tacticasoluciondescripcion'],
//                'headers' => ['dummy' => 'DUMMY', 'id' => 'ID', 'empresa' => 'EMPRESA', 'contactonombre' => 'CONTACTONOMBRE',
//	                            'contactoapellido' => 'CONTACTOAPELLIDO', 'fechaorigenweb' => 'FECHAORIGENWEB',
//	                            'origen' => 'ORIGEN', 'prioridad' => 'PRIORIDAD', 'tipo' => 'TIPO', 'incidentetipo' =>
//		                        'INCIDENTETIPO', 'incidentesubtipo' => 'INCIDENTESUBTIPO', 'incidenteelemento' =>
//		                        'INCIDENTEELEMENTO', 'codigoproducto' => 'CODIGOPRODUCTO', 'problemadescripcion' =>
//		                        'PROBLEMADESCRIPCION', 'fecharequerida' => 'FECHAREQUERIDA', 'tacticasoluciondescripcion' => 'SOLUCIONDESCRIPCION'],

                'columns' => ['dummy', 'id', 'empresa', 'contactonombre', 'contactoapellido', 'fechaorigenweb', 'origen', 'prioridad', 'tipo', 'incidentetipo', 'incidentesubtipo', 'incidenteelemento', 'codigoproducto', 'problemadescripcion', 'fecharequerida'],
                'headers' => ['dummy' => 'DUMMY', 'id' => 'ID', 'empresa' => 'EMPRESA', 'contactonombre' => 'CONTACTONOMBRE',
	                            'contactoapellido' => 'CONTACTOAPELLIDO', 'fechaorigenweb' => 'FECHAORIGENWEB',
	                            'origen' => 'ORIGEN', 'prioridad' => 'PRIORIDAD', 'tipo' => 'TIPO', 'incidentetipo' =>
		                        'INCIDENTETIPO', 'incidentesubtipo' => 'INCIDENTESUBTIPO', 'incidenteelemento' =>
		                        'INCIDENTEELEMENTO', 'codigoproducto' => 'CODIGOPRODUCTO', 'problemadescripcion' =>
		                        'PROBLEMADESCRIPCION', 'fecharequerida' => 'FECHAREQUERIDA'],

                'savePath' => Yii::$app->basePath . Yii::$app->params['export_ticket_savepath'],
                'fileName' => Yii::$app->params['export_ticket_filename'],
            ]);
        }
    }

    private static function sendErrors() {
        if (count(self::$errors) == 0) return;
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Errores en importación')
            ->setTextBody(implode("\n", self::$errors))
            ->send();
        $fn = fopen(\Yii::$app->basePath . '/runtime/logs/exporterror.log', 'w');
        fwrite($fn, implode("\n", self::$errors));
        fclose($fn);
    }
}
