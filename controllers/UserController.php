<?php // AVTPL

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Societe;
use app\models\SocieteParams;
use app\utils\Printer;
use app\utils\RegisterErrors;
use app\utils\FiltersService;
use app\models\PrintParams;



/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    // Defined also in crud.js
    const SUBMIT_MAIN = 1;
    const SUBMIT_DUPLICATE = 2;
    const SUBMIT_RECOVER = 3;
    const SUBMIT_DELETE = 4;
    const SUBMIT_PRINT = 5;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'update', 'delete', 'print', 'log', 'change-password', 'user-config'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'print'],
                        'allow' => true,
                        'roles' => ['users-view'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['users-new'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['users-upd'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['users-del'],
                    ],
                    [
                        'actions' => ['log'],
                        'allow' => true,
                        'roles' => ['history'],
                    ],[
                        'actions' => ['change-password', 'user-config'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-user' => ['post'],
                ],
            ],
        ];
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params so POST actions work
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }

    public function actionUserConfig()
    {
        $id = Yii::$app->user->getId();
        try {
            $model = $this->findModel($id);
            $model->setScenario('admin-update');
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->authManager->revokeAll($id);
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $id);
                    self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
                    return $this->redirect(['user-config']);
                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to update the element'));
                    return $this->redirect(['user-config']);
                }
            } else {
                return $this->render('user_config', [
                    'model' => $model,
                ]);
            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['user-config']);
        }
    }

    public function actionChangePassword() {
        $model = new \app\models\PasswordForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        $modeluser = User::findOne(Yii::$app->user->getId());
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                try {
                    $modeluser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
                    $modeluser->setScenario('update-passwd');
                    if ($modeluser->save()) {
                        self::setResultFlash(self::FLASH_UPDATED, Yii::t('app/user_add', 'Password changed'));
                        return $this->redirect(['user-config']);
                    } else {
                        self::setResultFlash(self::FLASH_ERROR, Yii::t('app/user_add', 'Password not changed'));
                        return $this->redirect(['user-config']);
                    }
                } catch(Exception $e) {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
                    return $this->redirect(['user-config']);
                }
            } else {
                self::setResultFlash(self::FLASH_ERROR, Yii::t('app/user_add', 'Password not changed'));
                return $this->redirect(['user-config']);
            }
        } else {
            return $this->renderAjax('user_password',[
                'model'=>$model
            ]);
        }
    }

    /**
     * Sends an email with the user and password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($user, $password)
    {
        $societe_id = $user->id_societe;
        $societe = Societe::findOne($societe_id);
        return \Yii::$app->mailer->compose(['html' => 'userWelcome-html', 'text' => 'userWelcome-text'], 
                ['user' => $user, 'password' => $password])
            ->setFrom([Yii::$app->params['supportEmail'] => $societe->name])
            ->setTo($user->email)
            ->setReplyTo($societe->email)
            ->setSubject(Yii::t('app/emails', 'Welcome to {name}!', ['name' => $societe->name]))
            ->send();
    }

    public function actionExistsContact($id_empresa, $id_contacto)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $prevuser = User::findOne(['id_empresa' => $id_empresa, 'id_contacto' => $id_contacto, 'baja' => 0]);
        return ['exists' => !is_null($prevuser)];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($baja = "0")
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $queryParams = Yii::$app->request->queryParams;
        FiltersService::getSessionFilters($queryParams, 'user_params');
        $queryParams['UserSearch']['baja'] = $baja;

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($queryParams, $baja);

        $printParams = new PrintParams();
        $printParams->queryParams = json_encode($queryParams);

        $arrayStatus = User::getArrayStatus();
        $arrayRole = User::getArrayRole($user->id_societe);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'printParams' => $printParams,
            'user' => $user,
            'baja' => $baja,
            'arrayStatus' => $arrayStatus,
            'arrayRole' => $arrayRole,
        ]);
    }

    public function actionPrint()
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $printParams = new PrintParams();
        if ($printParams->load(Yii::$app->request->post())) {
            $searchModel = new UserSearch();
            $searchModel->load(json_decode($printParams->queryParams, true));
            $dataProvider = $searchModel->searchMin(Yii::$app->params['maxRecordsPrint']);
            $dataProvider->sort = false;
            $societe = Societe::findOne($user->id_societe);
            $printCustom = '/web/socs/' . $societe->id . '/views/user/print';
            $printView = (is_readable(Yii::$app->basePath . $printCustom . '.php')) ? '@app/' . $printCustom : 'print';
            $save_pdf_report = SocieteParams::find()->where(['id_societe'=>$societe->id, 'name'=>'save_pdf_report'])->one()->value;
            $arrayStatus = User::getArrayStatus();
            $arrayRole = User::getArrayRole($user->id_societe);
            $content = $this->renderPartial($printView, [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'societe' => $societe,
                'arrayStatus' => $arrayStatus,
                'arrayRole' => $arrayRole,
            ]);
            Printer::printHtml($content, $save_pdf_report);
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $ajax = 0, $aclose = 0)
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = $this->findModel($id);
        if ($model->id_societe != $user->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        
        
        if ($ajax == 0) {
            return $this->render('view_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        } else {
            return $this->renderAjax('view_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ajax = 0, $aclose = 0)
    {
            $user = User::findIdentity(Yii::$app->user->getId());
            $model = new User(['scenario' => 'admin-create']);

            if ($model->load(Yii::$app->request->post())) {
                $password = Yii::$app->getSecurity()->generateRandomString(6);
                $model->setPassword($password);
                $prevuser = User::findOne(['id_empresa' => $model->id_empresa, 'id_contacto' => $model->id_contacto, 'baja' => 0]);
                if (!is_null($prevuser)) {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app/user_add', 'There is already a user created for that company and contact'));
                    if (isset($_POST['aclose']) && $_POST['aclose']) {
                        return $this->render('aclose');
                    } else {
                        return $this->redirect(['index']);
                    }
                }
                $model->id_societe = $user->id_societe;
                $model->baja = '0';
                if ($model->save()) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->id);
                    if ($model->role == 's#' . $model->id_societe . '-admin') Yii::$app->authManager->assign(Yii::$app->authManager->getRole('admin'), $model->id);

                    try {
                        if ($this->sendEmail($model, $password)) {
                            self::setResultFlash(self::FLASH_CREATED, Yii::t('app/user_add', 'User created successfully. An email was sent to the user with the login info.'));
                        } else {
                            self::setResultFlash(self::FLASH_ERROR, Yii::t('app/user_add', 'User created successfully. The email with the login info was not sent.'));
                        }
                    }catch (\Exception $exc){
                        self::setResultFlash(self::FLASH_ERROR, Yii::t('app/user_add', 'User created successfully. The email with the login info was not sent.'));
                    }

                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to create the element'));
                }
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                $societe = Societe::findOne($user->id_societe);
                $model->id_societe = $societe->id;
                $model->sidebar_collapse = $societe->sidebar_collapse;
                if ($ajax == 0) {
                    return $this->render('create', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose,]);
                } else {
                    return $this->renderAjax('create', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose,]);
                }
            }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $ajax = 0, $aclose = 0)
    {
        try {
            $user = User::findIdentity(Yii::$app->user->getId());
            $model = $this->findModel($id);
            if (!Yii::$app->user->can('setup') && $model->id_societe != $user->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            $model->setScenario('admin-update');
            if ($model->load(Yii::$app->request->post())) {
                
                if ($_POST['submitType'] == self::SUBMIT_RECOVER) $model->baja = 0;
                if ($_POST['submitType'] == self::SUBMIT_DELETE) $model->baja = 1;
                if ($model->save()) {
                    Yii::$app->authManager->revokeAll($id);
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->id);
                    if ($model->role == 's#' . $model->id_societe . '-admin') Yii::$app->authManager->assign(Yii::$app->authManager->getRole('admin'), $model->id);
                    if ($model->role == 's#' . $model->id_societe . '-driver') {
                        $routeUser = \frontend\models\RouteUser::find()->where(['id_user' => $id])->one();
                        if (!is_null($routeUser) && $routeUser->id_chofer != $model->id_chofer) {
                            $routeUser->id_chofer = $model->id_chofer;
                            $routeUser->save();
                        }
                    }
                    self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to update the element'));
                }
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => ($_POST['submitType'] == self::SUBMIT_DELETE) ? -1 : $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                $candelete = ($aclose != 0);
                
                if ($ajax == 0) {
                    return $this->render('update', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                } else {
                    return $this->renderAjax('update', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                }
            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }

    public function actionValidateCreate() {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = new User(['scenario' => 'admin-create']);
        $model->id_societe = $user->id_societe;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionValidateUpdate($id) {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $ajax = 0, $aclose = 0)
    {
        try {
            $user = User::findIdentity(Yii::$app->user->getId());
            $model = $this->findModel($id);
            if ($model->id_societe != $user->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            
            if (Yii::$app->request->isPost) {
                $model->setScenario('admin-update');
                $model->baja = '1';
                $model->save(false); // do not run validations
                
                self::setResultFlash(self::FLASH_DELETED, Yii::t('app', 'Element Successfully Removed'));
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                
                if ($ajax == 0) {
                    return $this->render('delete_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                } else {
                    return $this->renderAjax('delete_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                }

            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }

    public function actionAclose() {
        return $this->render('aclose', ['id' => 0]);
    }

    const FLASH_CREATED = 0;
    const FLASH_UPDATED = 1;
    const FLASH_DUPLICATED = 2;
    const FLASH_DELETED = 3;
    const FLASH_ERROR = 4;

    private static function setResultFlash($type, $message) {
        $flashType = ($type != self::FLASH_ERROR) ? 'success' : 'danger';
        switch ($type) {
            case self::FLASH_CREATED:
                $icon = 'glyphicon glyphicon-plus-sign';
                break;
            case self::FLASH_UPDATED:
                $icon = 'glyphicon glyphicon-pencil';
                break;
            case self::FLASH_DUPLICATED:
                $icon = 'glyphicon glyphicon-duplicate';
                break;
            case self::FLASH_DELETED:
                $icon = 'glyphicon glyphicon-trash';
                break;
            case self::FLASH_ERROR:
                $icon = 'glyphicon glyphicon-exclamation-sign';
                break;
        }
        Yii::$app->getSession()->setFlash($flashType, [
            'type' => $flashType,
            'duration' => 5000,
            'icon' => $icon,
            'message' => $message,
            'title' => 'Usuarios',
            'positonY' => 'top',
            'positonX' => 'center'
        ]);
    }

    public function actionLog($id)
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = $this->findModel($id);
        if ($model->id_societe != $user->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        return $this->render('log', ['model' => $model, 'user' => $user]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionSelectQuick($callbackFunction, $text, $autoSelect = false)
    {
        // If $text is set using $.load() with post, the PJAX does not work
        // Workaround: send parameter in the request, using js escape() function
        $text = urldecode($text);
        // utf8_encode was not working properly
        $enc = mb_detect_encoding($text, "UTF-8,ISO-8859-1");
        $text = iconv($enc, "UTF-8", $text);
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->quickSearch(Yii::$app->request->queryParams, $text, '0', 10);
        $arrayStatus = User::getArrayStatus();
        $arrayRole = User::getArrayRole($user->id_societe);

        return $this->renderAjax('select', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'callbackFunction' => $callbackFunction,
            'autoSelect' => $autoSelect,
            'arrayStatus' => $arrayStatus,
            'arrayRole' => $arrayRole,
        ]);
    }

    public function actionGetUser($id) {
        if (($model = User::findOne($id)) === null) {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
    }

// TODO: Remove or refactor this method! (action should not build html... pointless to create it as a template function... should be adhoc)

    public function actionGetForEmpresa($id_empresa, $id) {
        $countUser = User::find()
            ->where(['id_empresa' => $id_empresa, 'baja' => 0])
            ->count();

        $filteredUser = User::find()
            ->where(['id_empresa' => $id_empresa, 'baja' => 0])
            ->all();

        if ($countUser > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredUser as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForContacto($id_contacto, $id) {
        $countUser = User::find()
            ->where(['id_contacto' => $id_contacto, 'baja' => 0])
            ->count();

        $filteredUser = User::find()
            ->where(['id_contacto' => $id_contacto, 'baja' => 0])
            ->all();

        if ($countUser > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredUser as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForSociete($id_societe, $id) {
        $countUser = User::find()
            ->where(['id_societe' => $id_societe, 'baja' => 0])
            ->count();

        $filteredUser = User::find()
            ->where(['id_societe' => $id_societe, 'baja' => 0])
            ->all();

        if ($countUser > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredUser as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

}