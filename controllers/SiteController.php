<?php // AVTPL
namespace app\controllers;

use Yii;
use app\models\Societe;
use app\models\User;
use app\utils\RegisterErrors;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\utils\AlertsService;
// AVTPL EVO INI SiteControllerUseAdd
// AVTPL EVO END SiteControllerUseAdd

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'error-wrong-societe', 'open-notification', 'login', 'hist'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'error-wrong-societe', 'open-notification'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['hist'],
                        'allow' => true,
                        'roles' => ['history'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'clear-alert-popup' => ['post'],
                    'clear-alert-new' => ['post'],
                ],
            ],
        ];
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    // AVTPL EVO INI SiteIndexAction
    public function actionIndex()
    {
        if (Yii::$app->user->identity->role != 's#1-admin'){
            return $this->render('index');
        }

        // Trae solo los datos de los ultimos 6 meses
        $date = new \DateTime();
        $date->modify("-6 month");

        $connection = Yii::$app->getDb();
        // Abiertos en ese mes
        // Fecha de cierre en el grupo de mes del cierre
        // Los que quedaron pendientes de los abiertos si no se cerraron el mismo mes
        $command = $connection->createCommand("
            SELECT
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') as 'month',
              COUNT(T.id) as 'open'
            FROM ticket T
            WHERE
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') >= :start_date
            GROUP BY month
            ORDER BY month;
        ", [':start_date' => $date->format("Y-m")]);
        $open = $command->queryAll();

        $command = $connection->createCommand("
            SELECT
              DATE_FORMAT(T.tactica_fecha_cierre, '%Y-%m') as 'month',
              COUNT(T.id) as 'closed'
            FROM ticket T
            WHERE
              DATE_FORMAT(T.tactica_fecha_cierre, '%Y-%m') >= :start_date
            GROUP BY month
            ORDER BY month;
        ", [':start_date' => $date->format("Y-m")]);
        $closed = $command->queryAll();

        /*$command = $connection->createCommand("
            SELECT
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') as 'month',
              COUNT(IF(DATE_FORMAT(T.fecha_creacion, '%Y-%m') != DATE_FORMAT(T.tactica_fecha_cierre, '%Y-%m'), 1, IF(T.baja != 4, 1, null))) as 'pending'
            FROM ticket T
            WHERE
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') >= :start_date
            GROUP BY month
            ORDER BY month;
        ", [':start_date' => $date->format("Y-m")]);*/

        $command = $connection->createCommand("
            SELECT
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') as 'month',
              COUNT(T.fecha_creacion),
              (
                SELECT COUNT(T2.id)
                FROM ticket T2
                WHERE
                  DATE_FORMAT(T2.fecha_creacion, '%Y-%m') >= :start_date
                  AND DATE_FORMAT(T2.fecha_creacion, '%Y-%m') <= DATE_FORMAT(T.fecha_creacion, '%Y-%m') 
                  AND (T2.tactica_fecha_cierre IS NULL OR DATE_FORMAT(T2.tactica_fecha_cierre, '%Y-%m') > DATE_FORMAT(T.fecha_creacion, '%Y-%m'))
                ) as 'pending'
            FROM ticket T 
            WHERE
                DATE_FORMAT(T.fecha_creacion, '%Y-%m') >= :start_date
            GROUP BY month, pending
            ORDER BY month;
        ", [':start_date' => $date->format("Y-m")]);
        $pending = $command->   queryAll();

        $date->modify("-6 month");
        $command = $connection->createCommand("
            SELECT
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') as 'month',
              AVG(IF(T.baja = 4, DATEDIFF(T.tactica_fecha_cierre, T.fecha_creacion), null)) as 'diff'
            FROM ticket T
            WHERE
              DATE_FORMAT(T.fecha_creacion, '%Y-%m') >= :start_date
            GROUP BY month
            ORDER BY month;
        ", [':start_date' => $date->format("Y-m")]);
        $avg_time = $command->queryAll();

        $open_tickets = [];
        $closed_tickets = [];
        $pending_tickets = [];
        $time_avg_points = [];

        foreach ($open as $i => $month)
        {
            $open_tickets[] = !empty($open[$i]) ? ['label'=>$month['month'],  'y'=>$open[$i]['open'], 'color' => '#20b2aa'] : ['label'=>$month['month'],  'y'=>0, 'color' => '#20b2aa'];
            $closed_tickets[] = !empty($closed[$i]) ? ['label'=>$month['month'],  'y'=>$closed[$i]['closed'], 'color' => '#d38583'] : ['label'=>$month['month'],  'y'=>0, 'color' => '#d38583'];
            $pending_tickets[] = !empty($pending[$i]) ? ['label'=>$month['month'],  'y'=>$pending[$i]['pending']] : ['label'=>$month['month'],  'y'=>0];
        }

        foreach ($avg_time as $month)
        {
            $time_avg_points[] = !empty($month) ? ['label'=>$month['month'],  'y' => $month['diff']] : ['y' => 0];
        }

        return $this->render('dashboard', compact('open_tickets', 'closed_tickets', 'pending_tickets', 'time_avg_points'));
    }
    // AVTPL EVO END SiteIndexAction

	public function actionDb()
	{
		return $this->render('update-db');
	}

	public function actionInfo()
	{
		return $this->render('info');
	}

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->actionLogin();
    }

    public function actionLogin()
    {
        if (YII_ENV_PROD) {
            $cookies = Yii::$app->request->cookies;
            $id_societe = $cookies->getValue('societe', 0);
            if ($id_societe) {
                $societe = Societe::findOne($id_societe);
                return $this->redirect($societe->url);
            } else {
                return $this->goHome();
            }
        } else {
            return $this->goHome();
        }
    }

    public function actionErrorWrongSociete() {
        return $this->render('error-wrong-societe');
    }

    

    public function actionHist($type, $id)
    {
        $model = null;
        if ($type == 'Liquidaciones') {
            $model = \app\models\Liquidaciones::findOne($id);
        } elseif ($type == 'LiquidacionViaje') {
            $model = \app\models\LiquidacionViaje::findOne($id);
        } else {
            return 'Wrong Type!';
        }
        if ($model == null) return 'Element not found!';
        if ($model->id_societe != User::findIdentity(Yii::$app->user->getId())->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        return $this->render('log', ['model' => $model]);
    }

    private static function getImage($path) {
        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'image/jpeg');
        $response->format = Response::FORMAT_RAW;
        if (!is_resource($response->stream = fopen(Yii::$app->basePath . $path, 'r')) ) {
           throw new \yii\web\ServerErrorHttpException('file access failed: permission deny');
        }
        return $response->send();
    }

    public function actionOffline($message) {
        $this->layout = 'offline';
        if (!\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        return $this->render('offline', ['message' => $message]);
    }

    public function actionClearAlertPopup($name) {
        AlertsService::clearPopup($name);
    }

    public function actionClearAlertNew($name) {
        AlertsService::clearNew($name);
    }

    // AVTPL EVO INI SiteAdditionalActions
    
    // AVTPL EVO END SiteAdditionalActions

}
