<?php // AVTPL

namespace app\controllers;

use Yii;
use app\models\Societe;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\User;

/**
 * SocieteController implements the CRUD actions for Societe model.
 */
class SocieteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'view', 'log'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['societe-view'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['societe-upd'],
                    ],
                    [
                        'actions' => ['log'],
                        'allow' => true,
                        'roles' => ['history'],
                    ],
                ],
            ],
		];
	}

    public function actionView()
    {
        $id = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Societe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $id = $user->id_societe;
		try {
			$model = $this->findModel($id);
			if ($model->load(Yii::$app->request->post())) {
                $emails = json_decode($model->email_hist, true);
                // Original name is used to prevent to set the email of another agency
                if (!$model->ori_name) {
                    $model->ori_name = $model->name;
                }
                if (!in_array($model->email, $emails)) {
                    if (Societe::find()->where(['not', ['ori_name' => $model->ori_name]])->andFilterWhere(['like', 'email_hist', '"' . $model->email . '"'])->count() > 0) {
                        Yii::$app->getSession()->setFlash('danger', [
                            'type' => 'danger',
                            'duration' => 5000,
                            'icon' => 'glyphicon glyphicon-exclamation-sign',
                            'message' => Yii::t('app/societe', 'Invalid Reply Email Address. Contact us if you think this is a system error'),
                            'title' => 'Empresa',
                            'positonY' => 'top',
                            'positonX' => 'center'
                        ]);
                        return $this->redirect(['update']);
                    }
                    array_push($emails, $model->email);
                    $model->email_hist = json_encode($emails);
                }
				$model->save();
                if ($user->sidebar_collapse != $model->sidebar_collapse) {
                    $user->sidebar_collapse = $model->sidebar_collapse;
                    $user->save(false);
                }
				Yii::$app->getSession()->setFlash('success', [
					'type' => 'success',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-pencil',
					'message' => 'Parámetros actualizados correctamente',
					'title' => 'Empresa',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->redirect(['update']);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		} catch (NotFoundHttpException $e) {
			Yii::$app->getSession()->setFlash('danger', [
				'type' => 'danger',
				'duration' => 5000,
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'message' => $e->getMessage(),
				'title' => 'Empresa',
				'positonY' => 'top',
				'positonX' => 'center'
			]);
			return $this->redirect(['update']);
		}
    }

    public function actionLog()
    {
        $id = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $model = $this->findModel($id);
        return $this->render('log', ['model' => $model]);
    }

    /**
     * Finds the Societe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Societe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Societe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
    }
}