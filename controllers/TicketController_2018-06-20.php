<?php // AVTPL

namespace app\controllers;

use Yii;
use app\models\Ticket;
use app\models\TicketSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Societe;
use app\models\SocieteParams;
use app\models\Producto;
use app\models\Prioridad;
use app\models\Contacto;
use app\models\Empresa;
use app\utils\Printer;
use app\utils\RegisterErrors;
use app\utils\FiltersService;
use app\models\PrintParams;
use app\models\User;
use app\utils\Excel\ExcelIncidentesExporter;


/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
    // Defined also in crud.js
    const SUBMIT_MAIN = 1;
    const SUBMIT_DUPLICATE = 2;
    const SUBMIT_RECOVER = 3;
    const SUBMIT_DELETE = 4;
    const SUBMIT_PRINT = 5;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'update', 'delete', 'print', 'log'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'print'],
                        'allow' => true,
                        'roles' => ['incidente-view'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['incidente-create'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['none'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['none'],
                    ],
                    [
                        'actions' => ['log'],
                        'allow' => true,
                        'roles' => ['history'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-ticket' => ['post'],
                ],
            ],
        ];
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params so POST actions work
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }



    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;

        //ESTO ES PARA QUE EL FILTRO DE CUALQUIER ESTADO ABIERTO SE APLIQUE POR IGUAL.
        if(isset($queryParams['TicketSearch'])){
            if($queryParams["TicketSearch"]['baja'] < 4 && $queryParams["TicketSearch"]['baja'] != ""){
                $queryParams["TicketSearch"]['baja'] = [0, 1, 2, 3];
            }
        }


        FiltersService::getSessionFilters($queryParams, 'ticket_params');

        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search($queryParams);
        $dataProvider->sort->defaultOrder = ['fecha_creacion' => SORT_DESC, 'hora_creacion' => SORT_DESC];

        $printParams = new PrintParams();
        $printParams->queryParams = json_encode($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'printParams' => $printParams,
        ]);
    }

    public function actionPrint()
    {
        $printParams = new PrintParams();
        if ($printParams->load(Yii::$app->request->post())) {
            $searchModel = new TicketSearch();
            $searchModel->load(json_decode($printParams->queryParams, true));
            $dataProvider = $searchModel->searchMin(Yii::$app->params['maxRecordsPrint']);
            $dataProvider->sort = false;
            $societe = Societe::findOne(User::findIdentity(Yii::$app->user->getId())->id_societe);
            $printCustom = '/web/socs/' . $societe->id . '/views/ticket/print';
            $printView = (is_readable(Yii::$app->basePath . $printCustom . '.php')) ? '@app/' . $printCustom : 'print';
            $save_pdf_report = SocieteParams::find()->where(['id_societe'=>$societe->id, 'name'=>'save_pdf_report'])->one()->value;
            
            $content = $this->renderPartial($printView, [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'societe' => $societe,
                
            ]);
            Printer::printHtml($content, $save_pdf_report);
        }
    }

    public function actionExcel()
    {
	    $queryParams = Yii::$app->request->queryParams;
	    FiltersService::getSessionFilters($queryParams, 'ticket_params');

	    $searchModel = new TicketSearch();
	    $dataProvider = $searchModel->search($queryParams, -1);
	    $dataProvider->sort->defaultOrder = ['fecha_creacion' => SORT_DESC, 'hora_creacion' => SORT_DESC];

	    $user       = User::findIdentity(Yii::$app->user->getId());
	    $empresa    = null;
	    if(!is_null($user->id_empresa)){
		    $empresa    = Empresa::findIdentity($user->id_empresa);
	    }

	    $excel = new ExcelIncidentesExporter();
	    $excel->setUser($user);
	    $excel->setEmpresa($empresa);
	    $excel->generar($dataProvider);

    }

    /**
     * Displays a single Ticket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $ajax = 0, $aclose = 0)
    {
        $model = $this->findModel($id);
        $user = User::findIdentity(Yii::$app->user->getId());
        if ($model->id_societe != User::findIdentity(Yii::$app->user->getId())->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        
        
        if ($ajax == 0) {
            return $this->render('view_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        } else {
            return $this->renderAjax('view_form', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        }
    }

	public function actionCreating()
	{
		$array          = json_decode($_POST['array']);
		$arrayDatos = array(
			"id_producto" => $array->id_producto
		);
		$id_producto    = $arrayDatos['id_producto'];

		/*$queryParams = Yii::$app->request->queryParams;
		FiltersService::getSessionFilters($queryParams, 'ticket_params');*/

		/*$searchModel = new TicketSearch();
		$dataProvider = $searchModel->searchMin(-1);

		$hayTicketCreado    = false;
		if(count($dataProvider->getModels()) > 0)
		{
			foreach($dataProvider->getModels() as $data) {
				if($this->validateCreatingProducto($data->id_producto, $arrayDatos['id_producto']) && $this->validateCreatingEstado($data->baja)){
					$hayTicketCreado = true;
				}
			}
		}*/

        /*$cantidad = Ticket::find()->where([
            'id_producto' => $array['id_producto'],
            'baja' => [0, 1, 2, 3]
        ])->count();
        echo ($hayTicketCreado) ? 1 : 0;*/


        $cantidad = Ticket::find()->where(['id_producto' => $id_producto, 'baja' => [0, 1, 2, 3]])->count();
		echo ($cantidad > 0) ? 1 : 0;
	}
	private function validateCreatingProducto($data_idProd, $id_prod)
	{
		return ($data_idProd == $id_prod);
	}
	//DIFERENTE DE 4 ES ESTADO ABIERTO
	private function validateCreatingEstado($data_baja)
	{
		return ($data_baja != 4);
	}

	// METODO PARA CORRER LA ACTUALIZACION DE TACTICA EN LA TABLA DE TICKETS
	public function actionDb()
    {
        return $this->render('update-db-tactica');
    }

    // METODO PARA CORRER LA ACTUALIZACION DE TACTICA EN LA TABLA DE TICKETS
    public function actionCreateTableTicketEstados()
    {
        return $this->render('create-table-ticket-estados');
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ajax = 0, $aclose = 0)
    {
    	echo "Aca";
        $model = new Ticket();
        $user = User::findIdentity(Yii::$app->user->getId());
        $model->id_empresa = $user->id_empresa;
        $model->id_contacto = $user->id_contacto;

        //VERIFICO SI ES CARGA DE NUEVO TICKET, O SE ESTÁ EN LA VISTA
        if ($model->load(Yii::$app->request->post())) {

            $producto = Producto::findOne($model->id_producto);
            $prioridad = Prioridad::findOne($producto->id_prioridad);
            $vencimiento = new \DateTime();
            $model->fecha_creacion = $vencimiento->format('Y-m-d');
            $model->hora_creacion = $vencimiento->format('H:i:s');
            if ($prioridad->unidad == 'HORA') {
                $vencimiento->add(new \DateInterval('PT' . $prioridad->cantidad . 'H'));
            } elseif ($prioridad->unidad == 'DIA') {
                $vencimiento->add(new \DateInterval('P' . $prioridad->cantidad . 'D'));
            } elseif ($prioridad->unidad == 'SEMANA') {
                $vencimiento->add(new \DateInterval('P' . $prioridad->cantidad . 'W'));
            } elseif ($prioridad->unidad == 'MES') {
                $vencimiento->add(new \DateInterval('P' . $prioridad->cantidad . 'M'));
            }
            $model->fecha_vencimiento = $vencimiento->format('Y-m-d');
            $model->hora_vencimiento = $vencimiento->format('H:i:s');
            $model->id_societe = $user->id_societe;
            $model->baja = '0';

            if ($model->save()) {
                if ($this->sendEmail($model)) {
                    self::setResultFlash(self::FLASH_CREATED, Yii::t('app/ticket', 'Ticket Successfully Created'));
                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app/ticket', 'Ticket Successfully Created. The email to the administrator was not sent'));
                }
            } else {
                self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to create the element'));
            }
            if (isset($_POST['aclose']) && $_POST['aclose']) {
                return $this->render('aclose', ['id' => $model->id]);
            } else {
                return $this->redirect(['site/index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'ajax' => $ajax,
                'aclose' => $aclose,
            ]);
        }
    }

    private function sendEmail($model) {
        $user = User::findIdentity(Yii::$app->user->getId());
        $societe = Societe::findOne($user->id_societe);
        $send_cc = [$societe->email];
        $supervisor_fld = ($model->tipo == 'TECNICO') ? 'supervisor_tecnico' : 'supervisor_administ';
        $contactos = Contacto::find()->where(['id_empresa' => $model->id_empresa, $supervisor_fld => 1, 'baja' => 0])->all();
        foreach ($contactos as $contacto) {
            array_push($send_cc, $contacto->email);
        }
        return \Yii::$app->mailer->compose(['html' => 'newTicket-html', 'text' => 'newTicket-text'], 
                ['ticket' => $model])
            ->setFrom([Yii::$app->params['supportEmail'] => $societe->name])
            ->setTo($user->email)
            ->setCc($send_cc)
            ->setSubject(Yii::t('app/ticket', 'New Ticket from Web #{id}', ['id' => $model->id]))
            ->send();
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $ajax = 0, $aclose = 0)
    {
        try {
            $model = $this->findModel($id);
            if (!Yii::$app->user->can('setup') && $model->id_societe != User::findIdentity(Yii::$app->user->getId())->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            
            if ($model->load(Yii::$app->request->post())) {
                
                if ($_POST['submitType'] == self::SUBMIT_RECOVER) $model->baja = 0;
                if ($_POST['submitType'] == self::SUBMIT_DELETE) $model->baja = 1;
                if ($model->save()) {
                    
                    self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to update the element'));
                }
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => ($_POST['submitType'] == self::SUBMIT_DELETE) ? -1 : $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                $candelete = ($aclose != 0);
                
                if ($ajax == 0) {
                    return $this->render('update', ['model' => $model, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                } else {
                    return $this->renderAjax('update', ['model' => $model, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                }
            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }


    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $ajax = 0, $aclose = 0)
    {
        try {
            $model = $this->findModel($id);
            if ($model->id_societe != User::findIdentity(Yii::$app->user->getId())->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            
            if (Yii::$app->request->isPost) {
                
                $model->baja = '1';
                $model->save(false); // do not run validations
                
                self::setResultFlash(self::FLASH_DELETED, Yii::t('app', 'Element Successfully Removed'));
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                
                if ($ajax == 0) {
                    return $this->render('delete_form', ['model' => $model, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                } else {
                    return $this->renderAjax('delete_form', ['model' => $model, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                }

            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }

    public function actionAclose() {
        return $this->render('aclose', ['id' => 0]);
    }

    const FLASH_CREATED = 0;
    const FLASH_UPDATED = 1;
    const FLASH_DUPLICATED = 2;
    const FLASH_DELETED = 3;
    const FLASH_ERROR = 4;

    private static function setResultFlash($type, $message) {
        $flashType = ($type != self::FLASH_ERROR) ? 'success' : 'danger';
        switch ($type) {
            case self::FLASH_CREATED:
                $icon = 'glyphicon glyphicon-plus-sign';
                break;
            case self::FLASH_UPDATED:
                $icon = 'glyphicon glyphicon-pencil';
                break;
            case self::FLASH_DUPLICATED:
                $icon = 'glyphicon glyphicon-duplicate';
                break;
            case self::FLASH_DELETED:
                $icon = 'glyphicon glyphicon-trash';
                break;
            case self::FLASH_ERROR:
                $icon = 'glyphicon glyphicon-exclamation-sign';
                break;
        }
        Yii::$app->getSession()->setFlash($flashType, [
            'type' => $flashType,
            'duration' => 5000,
            'icon' => $icon,
            'message' => $message,
            'title' => 'Tickets',
            'positonY' => 'top',
            'positonX' => 'center'
        ]);
    }

    public function actionLog($id)
    {
        $model = $this->findModel($id);
        if ($model->id_societe != User::findIdentity(Yii::$app->user->getId())->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        return $this->render('log', ['model' => $model]);
    }

    public function actionGetTicket($id) {
        if (($model = Ticket::findOne($id)) === null) {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
    }

// TODO: Remove or refactor this method! (action should not build html... pointless to create it as a template function... should be adhoc)

    public function actionGetForEmpresa($id_empresa, $id) {
        $countTicket = Ticket::find()
            ->where(['id_empresa' => $id_empresa])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_empresa' => $id_empresa])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForContacto($id_contacto, $id) {
        $countTicket = Ticket::find()
            ->where(['id_contacto' => $id_contacto])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_contacto' => $id_contacto])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForTipo($id_tipo, $id) {
        $countTicket = Ticket::find()
            ->where(['id_tipo' => $id_tipo])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_tipo' => $id_tipo])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForSubtipo($id_subtipo, $id) {
        $countTicket = Ticket::find()
            ->where(['id_subtipo' => $id_subtipo])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_subtipo' => $id_subtipo])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForElemento($id_elemento, $id) {
        $countTicket = Ticket::find()
            ->where(['id_elemento' => $id_elemento])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_elemento' => $id_elemento])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForProducto($id_producto, $id) {
        $countTicket = Ticket::find()
            ->where(['id_producto' => $id_producto])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_producto' => $id_producto])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

    public function actionGetForSociete($id_societe, $id) {
        $countTicket = Ticket::find()
            ->where(['id_societe' => $id_societe])
            ->count();

        $filteredTicket = Ticket::find()
            ->where(['id_societe' => $id_societe])
            ->all();

        if ($countTicket > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredTicket as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

}