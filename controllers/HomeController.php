<?php // AVTPL
namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\User;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\DemoForm;
use app\models\Societe;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
// AVTPL EVO INI HomeControllerAddUse
// AVTPL EVO END HomeControllerAddUse

/**
 * Home controller
 */
class HomeController extends Controller
{
	public $layout = 'landing';
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            // AVTPL EVO INI HomeCorsFilter
            // AVTPL EVO END HomeCorsFilter
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['@'],
                        // Original: 'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
        // AVTPL EVO INI HomeBeforeActionDisableCors
        // AVTPL EVO END HomeBeforeActionDisableCors
		return parent::beforeAction($action);
	}
	
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }
        return $this->render('index');
    }

    public function actionLogin($ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
			if ($model->login()) {
				return $this->goBack();
			} else {
				Yii::$app->getSession()->setFlash('danger', [
					'type' => 'danger',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-exclamation-sign',
					'message' => 'Usuario o contraseña inválidos',
					'title' => 'Ingreso',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
			}
        } else {
            if ($ajax == 0) {
                return $this->render('login', ['model' => $model, 'ajax' => $ajax]);
            } else {
                return $this->renderAjax('login', ['model' => $model, 'ajax' => $ajax]);
            }
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact($ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', [
					'type' => 'success',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-envelope',
					'message' => 'Muchas gracias por contactarnos. Le responderemos a la brevedad',
					'title' => 'Contacto',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
            } else {
				Yii::$app->getSession()->setFlash('danger', [
					'type' => 'danger',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-exclamation-sign',
                    'message' => 'Ocurrió un error al enviar el mail. Por favor, comuníquese con ' . Yii::$app->params['supportEmail'],
					'title' => 'Contacto',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
            }
        } else {
            if ($ajax == 0) {
                return $this->render('contact', ['model' => $model, 'ajax' => $ajax]);
            } else {
                return $this->renderAjax('contact', ['model' => $model, 'ajax' => $ajax]);
            }
        }
    }

    public function actionDemo($ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        $model = new DemoForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', [
					'type' => 'success',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-envelope',
					'message' => 'Muchas gracias por contactarnos. Le responderemos a la brevedad',
					'title' => 'Solicitud de Demo',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
            } else {
				Yii::$app->getSession()->setFlash('danger', [
					'type' => 'danger',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-exclamation-sign',
                    'message' => 'Ocurrió un error al enviar el mail. Por favor, comuníquese con ' . Yii::$app->params['supportEmail'],
					'title' => 'Solicitud de Demo',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
            }
        } else {
            if ($ajax == 0) {
                return $this->render('demo', ['model' => $model, 'ajax' => $ajax]);
            } else {
                return $this->renderAjax('demo', ['model' => $model, 'ajax' => $ajax]);
            }
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset($ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
			if ($model->validate() && $model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', [
					'type' => 'success',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-envelope',
					'message' => 'Revise las instrucciones que le enviamos por email.',
					'title' => 'Reestablecer contraseña',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
            } else {
				Yii::$app->getSession()->setFlash('danger', [
					'type' => 'danger',
					'duration' => 5000,
					'icon' => 'glyphicon glyphicon-exclamation-sign',
					'message' => 'Lamentablemente no es posible reestablecer la contraseña para el usuario y email provistos',
					'title' => 'Reestablecer contraseña',
					'positonY' => 'top',
					'positonX' => 'center'
				]);
				return $this->goHome();
			}
        }

        if ($ajax == 0) {
            return $this->render('requestPasswordResetToken', ['model' => $model, 'ajax' => $ajax]);
        } else {
            return $this->renderAjax('requestPasswordResetToken', ['model' => $model, 'ajax' => $ajax]);
        }
    }

    public function actionExternalRequestPasswordReset()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [];
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->sendEmail()) {
                $result["success"] = true;
                $result["message"] = 'Revise las instrucciones que le enviamos por email.';
            } else {
                $result["success"] = false;
                $result["message"] = 'Lamentablemente no es posible reestablecer la contraseña para el usuario y email provistos';
            }
        } else {
            $result["success"] = false;
            $result["code"] = "Invalid request";
        }
        Yii::info("Result: " . implode(" ", $result), 'deb');
        return $result;
    }

    public function actionResetPasswordCall($token = "", $ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        if ($ajax == 0) {
            return $this->redirect('reset-password', ['token' => $token, 'ajax' => $ajax]);
        } else {
            if (empty($token) || !is_string($token)) {
                Yii::$app->getSession()->setFlash('danger', [
                    'type' => 'danger',
                    'duration' => 5000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',
                    'message' => 'Parámetro inválido',
                    'title' => 'Reestablecer contraseña',
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);
                return $this->goHome();
            }
            return $this->render('resetPasswordCall', ['token' => $token]);
        }
    }
	
    public function actionResetPassword($token, $ajax = -1)
    {
        if ($ajax == -1) $ajax = Yii::$app->params['homeAjaxDefault'];
        try {
	       $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
			Yii::$app->getSession()->setFlash('danger', [
				'type' => 'danger',
				'duration' => 5000,
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'message' => 'Parámetro inválido',
				'title' => 'Reestablecer contraseña',
				'positonY' => 'top',
				'positonX' => 'center'
			]);
			return $this->goHome();
            //throw new BadRequestHttpException($e->getMessage()); al mostrar el error lo hace en el layout main en vez de landing
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', [
				'type' => 'success',
				'duration' => 5000,
				'icon' => 'glyphicon glyphicon-user',
				'message' => 'La contraseña se ha guardado con éxito.',
				'title' => 'Reestablecer contraseña',
				'positonY' => 'top',
				'positonX' => 'center'
			]);
            $user = $model->getUser();
            $societe = Societe::findOne($user->id_societe);
            return $this->redirect($societe->url);
        }

        $user = User::findByPasswordResetToken($token);
        if (!$user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
		
        if ($ajax == 0) {
            return $this->render('resetPassword', ['model' => $model, 'user' => $user, 'ajax' => $ajax]);
        } else {
            return $this->renderAjax('resetPassword', ['model' => $model, 'user' => $user, 'ajax' => $ajax]);
        }
    }

    // AVTPL EVO INI HomeAdditionalActions
    // AVTPL EVO END HomeAdditionalActions

}
