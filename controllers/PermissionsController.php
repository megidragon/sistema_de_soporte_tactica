<?php // AVTPL

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use app\utils\Printer;
use app\utils\PermissionsService;
use app\utils\FiltersService;
use app\models\PermissionMinimumForm;
use app\models\PermissionSwitchForm;
use app\models\User;

class PermissionsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update-minimum', 'update-switch'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['permissions-view'],
                    ],
                    [
                        'actions' => ['update-minimum', 'update-switch'],
                        'allow' => true,
                        'roles' => ['permissions-upd'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        FiltersService::getSessionFilters($queryParams, 'permisos_params');
        Yii::$app->getAuthManager()->invalidateCache();
        $id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $rows = PermissionsService::getTable($id_societe);
        $roles = User::getArrayRole($id_societe);
        return $this->render('index', [
            'rows' => $rows,
            'roles' => $roles,
            'id_societe' => $id_societe,
        ]);
    }

    public function actionInvalidateCache() {
        Yii::$app->getAuthManager()->invalidateCache();
        return 'Cache Invalidated OK';
    }

    public function actionUpdateMinimum($name)
    {
        $id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $rows = PermissionsService::getTable($id_societe);
        $rowsel = [];
        foreach ($rows as $row) {
            if ($row['name'] == $name) {
                $rowsel = $row;
                break;
            }
        }
        $model = new PermissionMinimumForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->role != $rowsel['minrole']) {
                $auth = Yii::$app->getAuthManager();
                $permission = $auth->getPermission($model->name);
                $role = $auth->getRole($rowsel['minrole']);
                $auth->removeChild($role, $permission);
                $role = $auth->getRole($model->role);
                $auth->addChild($role, $permission);
                $auth->invalidateCache();
            }
            self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
            return $this->redirect(['index']);
        } else {
            $roles = User::getArrayRole($id_societe, User::ROLES_NO_BASIC);
            $model->name = $name;
            $model->desc = $rowsel['permission'];
            $model->role = $rowsel['minrole'];
            return $this->renderAjax('update-minimum', ['model' => $model, 'roles' => $roles]);
        }
    }

    public function actionUpdateSwitch($name)
    {
        $id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $rows = PermissionsService::getTable($id_societe);
        $rowsel = [];
        foreach ($rows as $row) {
            if ($row['name'] == $name) {
                $rowsel = $row;
                break;
            }
        }
        $type = 'client';
        $roleName = 's#' . $id_societe . '-' . $type;
        $model = new PermissionSwitchForm();
        if ($model->load(Yii::$app->request->post())) {
            $oldEnabled = $rowsel[$roleName];
            $newEnabled = ($model->ison == '1');
            if ($oldEnabled != $newEnabled) {
                $auth = Yii::$app->getAuthManager();
                $permission = $auth->getPermission($model->name);
                $role = $auth->getRole($roleName);
                if ($newEnabled) {
                    $auth->addChild($role, $permission);
                } else {
                    $auth->removeChild($role, $permission);
                }
                $auth->invalidateCache();
            }
            self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
            return $this->redirect(['index']);
        } else {
            $model->name = $name;
            $model->desc = $rowsel['permission'];
            $model->ison = $rowsel[$roleName];
            return $this->renderAjax('update-switch', ['model' => $model]);
        }
    }

    const FLASH_CREATED = 0;
    const FLASH_UPDATED = 1;
    const FLASH_DUPLICATED = 2;
    const FLASH_DELETED = 3;
    const FLASH_ERROR = 4;

    private static function setResultFlash($type, $message) {
        $flashType = ($type != self::FLASH_ERROR) ? 'success' : 'danger';
        switch ($type) {
            case self::FLASH_UPDATED:
                $icon = 'glyphicon glyphicon-pencil';
                break;
            case self::FLASH_ERROR:
                $icon = 'glyphicon glyphicon-exclamation-sign';
                break;
        }
        Yii::$app->getSession()->setFlash($flashType, [
            'type' => $flashType,
            'duration' => 5000,
            'icon' => $icon,
            'message' => $message,
            'title' => Yii::t('app/permissions', 'Permissions'),
            'positonY' => 'top',
            'positonX' => 'center'
        ]);
    }

}