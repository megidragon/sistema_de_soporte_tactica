<?php // AVTPL

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\AuditMail;
use app\models\AuditMailSearch;
use app\models\Societe;
use app\models\User;
use app\utils\FiltersService;
use app\utils\RegisterErrors;
use bedezign\yii2\audit\components\Helper;

/**
 * MailController
 * @package bedezign\yii2\audit\controllers
 */
class MailController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['email-view'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AuditMail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        FiltersService::getSessionFilters($queryParams, 'mails_params');
        $searchModel = new AuditMailSearch;
        $societe = Societe::findOne(User::findIdentity(Yii::$app->user->getId())->id_societe);
        $emails = json_decode($societe->email_hist, true);
        $dataProvider = $searchModel->search($queryParams, $emails);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single AuditMail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = AuditMail::findOne($id);
        $societe = Societe::findOne(User::findIdentity(Yii::$app->user->getId())->id_societe);
        $emails = json_decode($societe->email_hist, true);
        if (!in_array($model->reply, $emails)) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        if (!$model) {
            throw new NotFoundHttpException('The requested mail does not exist.');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Download an AuditMail file as eml.
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDownload($id)
    {
        $model = AuditMail::findOne($id);
        $societe = Societe::findOne(User::findIdentity(Yii::$app->user->getId())->id_societe);
        $emails = json_decode($societe->email_hist, true);
        if (!in_array($model->reply, $emails)) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        if (!$model) {
            throw new NotFoundHttpException('The requested mail does not exist.');
        }
        Yii::$app->response->sendContentAsFile(Helper::uncompress($model->data), $model->id . '.eml');
    }
}
