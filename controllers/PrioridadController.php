<?php // AVTPL

namespace app\controllers;

use Yii;
use app\models\Prioridad;
use app\models\PrioridadSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Societe;
use app\models\SocieteParams;
use app\utils\Printer;
use app\utils\RegisterErrors;
use app\utils\FiltersService;
use app\models\PrintParams;
use app\models\User;


/**
 * PrioridadController implements the CRUD actions for Prioridad model.
 */
class PrioridadController extends Controller
{
    // Defined also in crud.js
    const SUBMIT_MAIN = 1;
    const SUBMIT_DUPLICATE = 2;
    const SUBMIT_RECOVER = 3;
    const SUBMIT_DELETE = 4;
    const SUBMIT_PRINT = 5;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'update', 'delete', 'print', 'log'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'print'],
                        'allow' => true,
                        'roles' => ['incidente-config-view'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['none'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['incidente-config-upd'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['none'],
                    ],
                    [
                        'actions' => ['log'],
                        'allow' => true,
                        'roles' => ['history'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-prioridad' => ['post'],
                ],
            ],
        ];
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params so POST actions work
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }



    /**
     * Lists all Prioridad models.
     * @return mixed
     */
    public function actionIndex($baja = "0")
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $queryParams = Yii::$app->request->queryParams;
        FiltersService::getSessionFilters($queryParams, 'prioridad_params');
        $queryParams['PrioridadSearch']['baja'] = $baja;

        $searchModel = new PrioridadSearch();
        $dataProvider = $searchModel->search($queryParams, $baja);

        $printParams = new PrintParams();
        $printParams->queryParams = json_encode($queryParams);

        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'printParams' => $printParams,
            'user' => $user,
            'baja' => $baja,
            
        ]);
    }

    public function actionPrint()
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $printParams = new PrintParams();
        if ($printParams->load(Yii::$app->request->post())) {
            $searchModel = new PrioridadSearch();
            $searchModel->load(json_decode($printParams->queryParams, true));
            $dataProvider = $searchModel->searchMin(Yii::$app->params['maxRecordsPrint']);
            $dataProvider->sort = false;
            $societe = Societe::findOne($user->id_societe);
            $printCustom = '/web/socs/' . $societe->id . '/views/prioridad/print';
            $printView = (is_readable(Yii::$app->basePath . $printCustom . '.php')) ? '@app/' . $printCustom : 'print';
            $save_pdf_report = SocieteParams::find()->where(['id_societe'=>$societe->id, 'name'=>'save_pdf_report'])->one()->value;
            
            $content = $this->renderPartial($printView, [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'societe' => $societe,
                
            ]);
            Printer::printHtml($content, $save_pdf_report);
        }
    }

    /**
     * Displays a single Prioridad model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $ajax = 0, $aclose = 0)
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = $this->findModel($id);
        if ($model->id_societe != $user->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        
        
        if ($ajax == 0) {
            return $this->render('view', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        } else {
            return $this->renderAjax('view', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
        }
    }

    /**
     * Creates a new Prioridad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ajax = 0, $aclose = 0)
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = new Prioridad();
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->id_societe = $user->id_societe;
            $model->baja = '0';
            if ($model->save()) {
                
                self::setResultFlash(self::FLASH_CREATED, Yii::t('app', 'Element Successfully Created'));
            } else {
                self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to create the element'));
            }
            if (isset($_POST['aclose']) && $_POST['aclose']) {
                return $this->render('aclose', ['id' => $model->id]);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            
            if ($ajax == 0) {
                return $this->render('create', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
            } else {
                return $this->renderAjax('create', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
            }
        }
    }

    /**
     * Updates an existing Prioridad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $ajax = 0, $aclose = 0)
    {
        try {
            $user = User::findIdentity(Yii::$app->user->getId());
            $model = $this->findModel($id);
            if (!Yii::$app->user->can('setup') && $model->id_societe != $user->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            
            if ($model->load(Yii::$app->request->post())) {
                
                if ($_POST['submitType'] == self::SUBMIT_RECOVER) $model->baja = 0;
                if ($_POST['submitType'] == self::SUBMIT_DELETE) $model->baja = 1;
                if ($model->save()) {
                    
                    self::setResultFlash(self::FLASH_UPDATED, Yii::t('app', 'Element Successfully Updated'));
                } else {
                    self::setResultFlash(self::FLASH_ERROR, Yii::t('app', 'There was an error trying to update the element'));
                }
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => ($_POST['submitType'] == self::SUBMIT_DELETE) ? -1 : $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                $candelete = ($aclose != 0);
                
                if ($ajax == 0) {
                    return $this->render('update', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                } else {
                    return $this->renderAjax('update', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, 'candelete' => $candelete, ]);
                }
            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }


    /**
     * Deletes an existing Prioridad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $ajax = 0, $aclose = 0)
    {
        try {
            $user = User::findIdentity(Yii::$app->user->getId());
            $model = $this->findModel($id);
            if ($model->id_societe != $user->id_societe) {
                RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
                return $this->redirect(['site/error-wrong-societe']);
            }
            
            if (Yii::$app->request->isPost) {
                
                $model->baja = '1';
                $model->save(false); // do not run validations
                
                self::setResultFlash(self::FLASH_DELETED, Yii::t('app', 'Element Successfully Removed'));
                if (isset($_POST['aclose']) && $_POST['aclose']) {
                    return $this->render('aclose', ['id' => $model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            } else {
                
                if ($ajax == 0) {
                    return $this->render('delete', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                } else {
                    return $this->renderAjax('delete', ['model' => $model, 'user' => $user, 'ajax' => $ajax, 'aclose' => $aclose, ]);
                }

            }
        } catch (NotFoundHttpException $e) {
            self::setResultFlash(self::FLASH_ERROR, Yii::t('app', $e->getMessage()));
            return $this->redirect(['index']);
        }
    }

    public function actionAclose() {
        return $this->render('aclose', ['id' => 0]);
    }

    const FLASH_CREATED = 0;
    const FLASH_UPDATED = 1;
    const FLASH_DUPLICATED = 2;
    const FLASH_DELETED = 3;
    const FLASH_ERROR = 4;

    private static function setResultFlash($type, $message) {
        $flashType = ($type != self::FLASH_ERROR) ? 'success' : 'danger';
        switch ($type) {
            case self::FLASH_CREATED:
                $icon = 'glyphicon glyphicon-plus-sign';
                break;
            case self::FLASH_UPDATED:
                $icon = 'glyphicon glyphicon-pencil';
                break;
            case self::FLASH_DUPLICATED:
                $icon = 'glyphicon glyphicon-duplicate';
                break;
            case self::FLASH_DELETED:
                $icon = 'glyphicon glyphicon-trash';
                break;
            case self::FLASH_ERROR:
                $icon = 'glyphicon glyphicon-exclamation-sign';
                break;
        }
        Yii::$app->getSession()->setFlash($flashType, [
            'type' => $flashType,
            'duration' => 5000,
            'icon' => $icon,
            'message' => $message,
            'title' => 'Prioridades',
            'positonY' => 'top',
            'positonX' => 'center'
        ]);
    }

    public function actionLog($id)
    {
        $user = User::findIdentity(Yii::$app->user->getId());
        $model = $this->findModel($id);
        if ($model->id_societe != $user->id_societe) {
            RegisterErrors::registerWrongSociete(Yii::$app->user->getId(), Yii::$app->request->url);
            return $this->redirect(['site/error-wrong-societe']);
        }
        return $this->render('log', ['model' => $model, 'user' => $user]);
    }

    public function actionGetPrioridad($id) {
        if (($model = Prioridad::findOne($id)) === null) {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $model;
    }

    /**
     * Finds the Prioridad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prioridad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prioridad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('No se encuentra el objeto.');
        }
    }

// TODO: Remove or refactor this method! (action should not build html... pointless to create it as a template function... should be adhoc)

    public function actionGetForSociete($id_societe, $id) {
        $countPrioridad = Prioridad::find()
            ->where(['id_societe' => $id_societe, 'baja' => 0])
            ->count();

        $filteredPrioridad = Prioridad::find()
            ->where(['id_societe' => $id_societe, 'baja' => 0])
            ->all();

        if ($countPrioridad > 0) {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
            foreach ($filteredPrioridad as $object) {
                $selected = ($object->id == $id) ? "selected" : "";
                echo "<option value='" . $object->id . "' " . $selected . ">" . $object->descripcion . "</option>";
            }
        } else {
            echo "<option value=''>" . Yii::t('app', 'Choose') . "</option>";
        }
    }

}