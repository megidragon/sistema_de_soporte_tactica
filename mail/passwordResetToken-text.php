<?php // AVTPL

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['home/reset-password-call', 'token' => $user->password_reset_token]);
?>
<?= \Yii::t('app/emails', 'Hello {username}', ['username' => $user->username]) ?>,

<?= \Yii::t('app/emails', 'Use the folloging link to reset your password:') ?>

<?= $resetLink ?>
