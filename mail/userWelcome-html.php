<?php // AVTPL
use yii\helpers\Html;
use app\models\User;
use app\models\Societe;

$appLink = \Yii::$app->urlManager->createAbsoluteUrl(['home']);
$id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
$societe = Societe::findOne($id_societe);
$societe_name = $societe->name;
$societe_url = $societe->url;

?>
<div class="welcome-user">
    <p><?= \Yii::t('app/emails', 'Welcome to {name}!', ['name' => $societe_name]) ?></p>

    <p><?= \Yii::t('app/emails', 'Your user is: {username}', ['username' => Html::encode($user->username)]) ?></p>

    <p><?= \Yii::t('app/emails', 'Your password is: {password}', ['password' => Html::encode($password)]) ?></p>

    <p><?= \Yii::t('app/emails', 'Access URL: {url}', ['url' => Html::a($societe_name, Html::encode($societe_url))]) ?></p>

    <p><?= \Yii::t('app/emails', 'Please check the welcome notification once you login, for instructions on how to change your password.') ?></p>

    <!-- AVTPL EVO INI WelcomeMailAddInfo -->
    <!-- AVTPL EVO END WelcomeMailAddInfo -->
</div>
