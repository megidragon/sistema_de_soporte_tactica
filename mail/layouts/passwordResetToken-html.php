<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['home/reset-password-call', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p><?= \Yii::t('app/emails', 'Hello {username}', ['username' => Html::encode($user->username)]) ?>,</p>

    <p><?= \Yii::t('app/emails', 'Use the folloging link to reset your password:') ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
