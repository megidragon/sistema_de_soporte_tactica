<?php
use app\models\User;
use app\models\Societe;

$id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
$societe = Societe::findOne($id_societe);
$societe_name = $societe->name;
$tipo = [
    'TECNICO' => 'Soporte Técnico',
    'ADMINISTRACION' => 'Comercial / Administración',
];
$prioridad = [
    'BAJA' => 'Baja',
    'MEDIA' => 'Media',
    'ALTA' => 'Alta',
];
?>
<div class="welcome-user">
    <p><?= \Yii::t('app/ticket', 'New ticket generated in the web of {name}', ['name' => $societe_name]) ?></p>

    <p><?= \Yii::t('app/ticket', 'Ticket Number: ') . $ticket->id ?></p>

    <p><?= \Yii::t('app/ticket', 'Creation Date: ') . Yii::$app->formatter->asDate($ticket->fecha_creacion) . ' ' . substr($ticket->hora_creacion, 0, 5) ?></p>

    <p><?= \Yii::t('app/ticket', 'Due Date: ') . Yii::$app->formatter->asDate($ticket->fecha_vencimiento) . ' ' . substr($ticket->hora_vencimiento, 0, 5) ?></p>

    <p><?= $ticket->getAttributeLabel('id_empresa') . ': ' . $ticket->idEmpresa->nombre ?></p>

    <p><?= $ticket->getAttributeLabel('id_contacto') . ': ' . $ticket->idContacto->nombre . ' ' . $ticket->idContacto->apellido ?></p>

    <p><?= $ticket->getAttributeLabel('tipo') . ': ' . $tipo[$ticket->tipo] ?></p>

    <p><?= $ticket->getAttributeLabel('prioridad') . ': ' . $prioridad[$ticket->prioridad] ?></p>

    <p><?= $ticket->getAttributeLabel('id_tipo') . ': ' . $ticket->idTipo->descripcion ?></p>

    <p><?= $ticket->getAttributeLabel('id_subtipo') . ': ' . $ticket->idSubtipo->descripcion ?></p>

    <p><?= $ticket->getAttributeLabel('id_elemento') . ': ' . $ticket->idElemento->descripcion ?></p>

    <p><?= $ticket->getAttributeLabel('id_producto') . ': ' . $ticket->idProducto->codigo ?></p>

    <p><?= $ticket->getAttributeLabel('datos_red') . ': ' . $ticket->idProducto->datos_red ?></p>

    <p><?= $ticket->getAttributeLabel('mac_address') . ': ' . $ticket->idProducto->mac_address ?></p>

    <p><?= $ticket->getAttributeLabel('nro_serie') . ': ' . $ticket->idProducto->nro_serie ?></p>

    <p><?= $ticket->getAttributeLabel('nombre_sector') . ': ' . $ticket->idProducto->nombre_sector ?></p>

    <p><?= $ticket->getAttributeLabel('fabricante') . ': ' . $ticket->idProducto->fabricante ?></p>

    <p><?= $ticket->getAttributeLabel('posicion') . ': ' . $ticket->idProducto->posicion ?></p>

    <p><?= $ticket->getAttributeLabel('prioridad_producto') . ': ' . $ticket->idProducto->idPrioridad->codigo ?></p>

    <p><?= $ticket->getAttributeLabel('descripcion') . ': ' . $ticket->descripcion ?></p>

</div>
