<?php
use app\models\User;
use app\models\Societe;

$id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
$societe = Societe::findOne($id_societe);
$societe_name = $societe->name;
$tipo = [
    'TECNICO' => 'Soporte Técnico',
    'ADMINISTRACION' => 'Comercial / Administración',
];
$prioridad = [
    'BAJA' => 'Baja',
    'MEDIA' => 'Media',
    'ALTA' => 'Alta',
];
?>

<?= \Yii::t('app/ticket', 'New ticket generated in the web of {name}', ['name' => $societe_name]) ?>

<?= \Yii::t('app/ticket', 'Ticket Number: ') . $ticket->id ?>

<?= \Yii::t('app/ticket', 'Creation Date: ') . Yii::$app->formatter->asDate($ticket->fecha_creacion) . ' ' . substr($ticket->hora_creacion, 0, 5) ?>

<?= \Yii::t('app/ticket', 'Due Date: ') . Yii::$app->formatter->asDate($ticket->fecha_vencimiento) . ' ' . substr($ticket->hora_vencimiento, 0, 5) ?>

<?= $ticket->getAttributeLabel('id_empresa') . ': ' . $ticket->idEmpresa->nombre ?>

<?= $ticket->getAttributeLabel('id_contacto') . ': ' . $ticket->idContacto->nombre . ' ' . $ticket->idContacto->apellido ?>

<?= $ticket->getAttributeLabel('tipo') . ': ' . $tipo[$ticket->tipo] ?>

<?= $ticket->getAttributeLabel('prioridad') . ': ' . $prioridad[$ticket->prioridad] ?>

<?= $ticket->getAttributeLabel('id_tipo') . ': ' . $ticket->idTipo->descripcion ?>

<?= $ticket->getAttributeLabel('id_subtipo') . ': ' . $ticket->idSubtipo->descripcion ?>

<?= $ticket->getAttributeLabel('id_elemento') . ': ' . $ticket->idElemento->descripcion ?>

<?= $ticket->getAttributeLabel('id_producto') . ': ' . $ticket->idProducto->codigo ?>

<?= $ticket->getAttributeLabel('datos_red') . ': ' . $ticket->idProducto->datos_red ?>

<?= $ticket->getAttributeLabel('mac_address') . ': ' . $ticket->idProducto->mac_address ?>

<?= $ticket->getAttributeLabel('nro_serie') . ': ' . $ticket->idProducto->nro_serie ?>

<?= $ticket->getAttributeLabel('nombre_sector') . ': ' . $ticket->idProducto->nombre_sector ?>

<?= $ticket->getAttributeLabel('fabricante') . ': ' . $ticket->idProducto->fabricante ?>

<?= $ticket->getAttributeLabel('posicion') . ': ' . $ticket->idProducto->posicion ?>

<?= $ticket->getAttributeLabel('prioridad_producto') . ': ' . $ticket->idProducto->idPrioridad->codigo ?>

<?= $ticket->getAttributeLabel('descripcion') . ': ' . $ticket->descripcion ?>
