<?php // AVTPL
use app\models\User;
use app\models\Societe;

$appLink = \Yii::$app->urlManager->createAbsoluteUrl(['home']);
$id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
$societe = Societe::findOne($id_societe);
$societe_name = $societe->name;
$societe_url = $societe->url;

?>
<?= \Yii::t('app/emails', 'Welcome to {name}!', ['name' => $societe_name]) ?>

<?= \Yii::t('app/emails', 'Your user is: {username}', ['username' => $user->username]) ?>

<?= \Yii::t('app/emails', 'Your password is: {password}', ['password' => $password]) ?>

<?= \Yii::t('app/emails', 'Access URL: {url}', ['url' => $societe_url]) ?>

<?= \Yii::t('app/emails', 'Please check the welcome notification once you login, for instructions on how to change your password.') ?>

<?php
// AVTPL EVO INI WelcomeMailAddInfo
// AVTPL EVO END WelcomeMailAddInfo
?>
