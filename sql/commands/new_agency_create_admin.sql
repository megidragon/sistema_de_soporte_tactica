-- AVTPL
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `id_societe`, `status`, `baja`) VALUES
(@id_start + 1, 'admin@id_societe', '9cGAKcKaU8G6mZpbANbgDVr84DLDh_8Y', '$2y$13$txauotNfroro7MdkzVtUXOLdOf5IJ3eSQZ46vzmTCu/SyipfdzIbi', '', 'lassa@kaizen2b.com', 's#@id_societe-admin', @id_societe, 10, 0);

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('s#@id_societe-admin', @id_start + 1, 1435181074);

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', @id_start + 1, 1435181074);
