CREATE TABLE `ticket_uploads` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `upload` varchar(50) DEFAULT NULL,
  `id_ticket` int(11),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_ticket`) REFERENCES `ticket` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;