ALTER TABLE ticket
  ADD (
  `tactica_user_modificacion` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `tactica_fecha_modificacion` date DEFAULT NULL,
  `tactica_hora_modificacion` TIME DEFAULT NULL,
  `tactica_user_cierre` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `tactica_fecha_cierre` date DEFAULT NULL,
  `tactica_hora_cierre` TIME DEFAULT NULL,
  `tactica_solucion_descr` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `tactica_soporte_usuario_asign` TEXT CHARACTER SET utf8 DEFAULT NULL
  );

/* PARA AGREGAR EL NUEVO CAMPO (TACTICA_SOPORTE_USUARIO_ASIGN) */
/*ALTER TABLE ticket
  ADD (
  `tactica_soporte_usuario_asign` TEXT CHARACTER SET utf8 DEFAULT NULL
  );*/

/* PARA ELIMINAR LOS CAMPOS CREADOS DE LA TABLA TICKET*/

/*ALTER TABLE ticket
  DROP COLUMN `tactica_user_modificacion`,
  DROP COLUMN `tactica_fecha_modificacion`,
  DROP COLUMN `tactica_user_cierre`,
  DROP COLUMN `tactica_fecha_cierre`,
  DROP COLUMN `tactica_solucion_descr`;
  DROP COLUMN `tactica_soporte_usuario_asign`;
*/