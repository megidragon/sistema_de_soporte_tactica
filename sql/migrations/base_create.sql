-- AVTPL
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `societe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `url` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `email_bcc` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `email_hist` TEXT DEFAULT NULL,
  `ori_name` varchar(45) DEFAULT NULL,
  `sidebar_collapse` boolean NULL DEFAULT 0,
  -- AVTPL EVO INI SocieteSqlAddFields
  -- AVTPL EVO END SocieteSqlAddFields
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- AVTPL variables in societe.UserSqlAddFields and societe.UserSqlAddForeigns
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `role` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `status` smallint(6) DEFAULT '10',
  `nombre` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_empresa` int(11),
  `id_contacto` int(11),
  `id_societe` int(11),
  `sidebar_collapse` boolean NULL DEFAULT 0,
  `baja` boolean,
  PRIMARY KEY (`id`),
    --FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) PRESENT FOR AVTPL,
  --FOREIGN KEY (`id_contacto`) REFERENCES `contacto` (`id`) PRESENT FOR AVTPL,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `date_reg` date DEFAULT NULL,
  `time_reg` time DEFAULT NULL,
  `id_societe` int(11),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



CREATE TABLE `societe_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `value` text DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `only_admin` boolean,
  `desc_es` text DEFAULT NULL,
  `id_societe` int(11),
  `baja` boolean,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `user_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notif_popup` boolean DEFAULT 0,
  `notif_new` int(4) DEFAULT 0,
  `notif_id` int(11) DEFAULT NULL,
  `ticket_popup` boolean DEFAULT 0,
  `ticket_new` int(4) DEFAULT 0,
  `ticket_id` int(11) DEFAULT NULL,
  `schedule_popup` boolean DEFAULT 0,
  `schedule_new` int(4) DEFAULT 0,
  `schedule_id` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_societe` int(11),
  `baja` boolean DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
