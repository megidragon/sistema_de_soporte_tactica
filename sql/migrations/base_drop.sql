-- AVTPL
DROP TABLE IF EXISTS `societe_params`;

DROP TABLE IF EXISTS `notificacion_usuario`;

DROP TABLE IF EXISTS `notificaciones`;

DROP TABLE IF EXISTS `errors`;

DROP TABLE IF EXISTS `user`;

DROP TABLE IF EXISTS `societe`;

DROP TABLE IF EXISTS `auth_assignment`;

DROP TABLE IF EXISTS `auth_item_child`;

DROP TABLE IF EXISTS `auth_item`;

DROP TABLE IF EXISTS `auth_rule`;
