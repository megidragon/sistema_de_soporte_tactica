CREATE TABLE `tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) DEFAULT NULL,
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `subtipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) DEFAULT NULL,
  `id_tipo` int(11),
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) DEFAULT NULL,
  `id_subtipo` int(11),
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_subtipo`) REFERENCES `subtipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tacticaid` varchar(20) DEFAULT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tacticaid` varchar(20) DEFAULT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `apellido` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `supervisor_tecnico` boolean DEFAULT 0,
  `supervisor_administ` boolean DEFAULT 0,
  `id_empresa` int(11),
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `prioridad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(500) DEFAULT NULL,
  `unidad` varchar(20) DEFAULT NULL,
  `cantidad` int(3) DEFAULT NULL,
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tacticaid` varchar(20) DEFAULT NULL,
  `codigo` varchar(500) DEFAULT NULL,
  `nro_serie` varchar(500) DEFAULT NULL,
  `nombre_sector` varchar(500) DEFAULT NULL,
  `datos_red` varchar(500) DEFAULT NULL,
  `mac_address` varchar(500) DEFAULT NULL,
  `fabricante` varchar(500) DEFAULT NULL,
  `posicion` varchar(500) DEFAULT NULL,
  `id_prioridad` int(11),
  `id_empresa` int(11),
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_prioridad`) REFERENCES `prioridad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11),
  `id_contacto` int(11),
  `prioridad` varchar(20) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `id_tipo` int(11),
  `id_subtipo` int(11),
  `id_elemento` int(11),
  `id_producto` int(11),
  `descripcion` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `hora_creacion` time DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `hora_vencimiento` time DEFAULT NULL,
  `id_societe` int(11),
  `baja` int(1) default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_contacto`) REFERENCES `contacto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_subtipo`) REFERENCES `subtipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_elemento`) REFERENCES `elemento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (`id_societe`) REFERENCES `societe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

