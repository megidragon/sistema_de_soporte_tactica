-- Ignore Error --
ALTER TABLE `contacto`
ADD COLUMN `supervisor_tecnico` boolean DEFAULT 0 COMMENT '' AFTER `email`;

-- Ignore Error --
ALTER TABLE `contacto`
ADD COLUMN `supervisor_administ` boolean DEFAULT 0 COMMENT '' AFTER `supervisor_tecnico`;
