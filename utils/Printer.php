<?php // AVTPL

namespace app\utils;

use Yii;
use kartik\mpdf\Pdf;

class Printer
{
    public static function printHtml($content, $save_pdf_report) {
        $params = self::getPrintParams($content);
        if ($save_pdf_report) {
            $params['destination'] = Pdf::DEST_DOWNLOAD;
            $params['filename'] = 'report.pdf';
        } else {
            $params['destination'] = Pdf::DEST_BROWSER;
        }
        $pdf = new Pdf($params);
        return $pdf->render();
    }

    public static function sendPdf($content, $mailView, $mailFrom, $mailTo, $mailReply, $mailBcc, $mailSubject, $pdfname) {
        $uniqueid = md5(uniqid(rand(), true));
        $filename = "/tmp/report-$uniqueid.pdf";
        $params = self::getPrintParams($content);
        $params['destination'] = Pdf::DEST_FILE;
        $params['filename'] = $filename;
        $pdf = new Pdf($params);
        $pdf->render();
        $composer = Yii::$app->mailer->compose($mailView)
            ->setFrom($mailFrom)
            ->setTo($mailTo)
            ->setSubject($mailSubject)
            ->attach($filename, ['fileName' => $pdfname]);
        if (!empty($mailReply)) $composer->setReplyTo($mailReply);
        if (!empty($mailBcc)) $composer->setBcc($mailBcc);
        $composer->send();
        unlink($filename);
    }

    private static function getPrintParams(&$content) {
        $header = self::getPrintParam($content, 'header');
        $footer = self::getPrintParam($content, 'footer');
        $orient = self::getPrintParam($content, 'orient');
        $format = self::getPrintParam($content, 'format');
        $margintop = self::getPrintParam($content, 'margintop');
        $marginbottom = self::getPrintParam($content, 'marginbottom');
        $cssinline = self::getPrintParam($content, 'cssinline');

        if ($header == null) $header = '';
        if ($footer == null) $footer = '';
        if ($orient == null) $orient = Pdf::ORIENT_PORTRAIT;
        if ($format == null) $format = Pdf::FORMAT_A4;
        if ($margintop == null) $margintop = 20;
        if ($marginbottom == null) $marginbottom = 20;
        if ($cssinline == null) $cssinline = '';

        define('_MPDF_TTFONTDATAPATH','/tmp/'); // to solve https://github.com/kartik-v/yii2-mpdf/issues/9
        $params = [
            'mode' => Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => $orient,
            'content' => $content,
            'marginTop' => $margintop,
            'marginBottom' => $marginbottom,
            'cssInline' => $cssinline,
            'methods' => [
                'SetHtmlHeader' => [$header],
                'SetHtmlFooter' => [$footer],
            ]
        ];
        return $params;
    }

    private static function getPrintParam(&$content, $param) {
        $result = null;
        $opentag = '<' . $param . '>';
        $opentagpos = strpos($content, $opentag);
        if ($opentagpos !== false) {
            $opentaglen = strlen($opentag);
            $closetagpos = strpos($content, '</' . $param . '>');
            $result = substr($content, $opentagpos + $opentaglen, $closetagpos - $opentagpos - $opentaglen);
            $content = substr($content, 0, $opentagpos) . substr($content, $closetagpos + $opentaglen + 1);
        }
        return $result;
    }

    public static function getReportsList() {
        $result = [];
        $files = self::rglob(\Yii::$app->basePath . '/views/*/print*.php');
        foreach ($files as $key => $value) {
            $module_report = str_replace(\Yii::$app->basePath . '/views/', '', $value);
            list($module_dir, $report_file) = explode('/', $module_report);
            $module = implode(' ', array_map('ucfirst', explode('-', $module_dir)));
            $report = ($report_file == 'print.php' || $report_file == 'print-list.php') ? 'Lista' : ucfirst(str_replace('print-', '', str_replace('.php', '', $report_file)));
            array_push($result, ['id' => $key, 'module' => $module, 'report' => $report, 'module_report' => $module_report, 'module_dir' => $module_dir, 'report_file' => $report_file]);
        }
        return $result;
    }

    private static function rglob($pattern, $flags = 0) {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, self::rglob($dir.'/'.basename($pattern), $flags));
        }
        return $files;
    }

}