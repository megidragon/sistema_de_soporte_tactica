<?php // AVTPL

namespace app\utils;

use Yii;
use app\models\UserAlerts;

class AlertsService
{
    public static function getUserAlerts() {
        $alertTypes = Yii::$app->params['alertTypes'];
        $alertTypeArray = [];
        if ($alertTypes != '') $alertTypeArray = explode(',', $alertTypes);
        $new = [];
        $alerts = [];
        foreach ($alertTypeArray as $alertType) {
            $new[$alertType] = 0;
        }
        $alertObj = UserAlerts::findOne(['id_user' => Yii::$app->user->getId(), 'baja' => 0]);
        if (!is_null($alertObj)) {
            foreach ($alertTypeArray as $alertType) {
                $newAlert = $alertObj->getAttribute($alertType . '_new');
                $mustAlert = $alertObj->getAttribute($alertType . '_popup');
                $idAlert = $alertObj->getAttribute($alertType . '_id');
                $new[$alertType] = $newAlert;
                if ($mustAlert) {
                    array_push($alerts, ['name'=>$alertType, 'number'=>$newAlert, 'id'=>$idAlert]);
                }
            }
        }
        return [
            'new' => $new,
            'alerts' => $alerts,
        ];
    }

    public static function clearPopup($name) {
        $alertObj = UserAlerts::findOne(['id_user' => Yii::$app->user->getId(), 'baja' => 0]);
        if (!is_null($alertObj)) {
            $alertObj->setAttribute($name . '_popup', 0);
            $alertObj->save();
        }
    }

    public static function clearNew($name) {
        $alertObj = UserAlerts::findOne(['id_user' => Yii::$app->user->getId(), 'baja' => 0]);
        if (!is_null($alertObj)) {
            $alertObj->setAttribute($name . '_popup', 0);
            $alertObj->setAttribute($name . '_new', 0);
            $alertObj->save();
        }
    }

    public static function addAlert($id_user, $name, $id) {
        $alertObj = UserAlerts::findOne(['id_user' => $id_user, 'baja' => 0]);
        if (is_null($alertObj)) {
            $alertObj = new UserAlerts();
            $alertObj->id_user = $id_user;
            $alertObj->setAttribute($name . '_new', 0);
            $alertObj->id_societe = 1;
        }
        $newAlert = $alertObj->getAttribute($name . '_new');
        $alertObj->setAttribute($name . '_popup', 1);
        $alertObj->setAttribute($name . '_new', $newAlert + 1);
        $alertObj->setAttribute($name . '_id', $id);
        $alertObj->save();
    }
}
