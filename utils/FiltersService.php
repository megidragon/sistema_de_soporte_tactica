<?php // AVTPL

namespace app\utils;

use Yii;

class FiltersService
{
    public static function getSessionFilters(&$params, $sessionVariable) {
        $paramsPage = null;
        $paramsSort = null;
        $paramsCount = count($params);
        if (array_key_exists('page', $params)) {
            $paramsPage = $params['page'];
            $paramsCount--;
        }
        if (array_key_exists('sort', $params)) {
            $paramsSort = $params['sort'];
            $paramsCount--;
        }
        if ($paramsCount < 1) {
            $params = Yii::$app->session[$sessionVariable];
            if (!is_null($paramsPage)) {
                $_GET['page'] = $paramsPage;
                $sessionVars = Yii::$app->session[$sessionVariable];
                $sessionVars['page'] = $paramsPage;
                Yii::$app->session[$sessionVariable] = $sessionVars;
            } elseif (isset(Yii::$app->session[$sessionVariable]['page'])) {
                $_GET['page'] = Yii::$app->session[$sessionVariable]['page'];
            }
            if (!is_null($paramsSort)) {
                $_GET['sort'] = $paramsSort;
                $sessionVars = Yii::$app->session[$sessionVariable];
                $sessionVars['sort'] = $paramsSort;
                Yii::$app->session[$sessionVariable] = $sessionVars;
            } elseif (isset(Yii::$app->session[$sessionVariable]['sort'])) {
                $_GET['sort'] = Yii::$app->session[$sessionVariable]['sort'];
            }
        } else {
            Yii::$app->session[$sessionVariable] = $params;
        }
    }
}
