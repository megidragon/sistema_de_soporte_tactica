<?php // AVTPL

namespace app\utils;

use Yii;

class HtmlService
{
    public static function clickGridRow($view, $gridName, $clickIdPrefix, $exceptLastCol = true, $exceptNCol = -1) {
        if ($exceptNCol > -1) $view->registerJs("$('#" . $gridName . "-container table tr td:nth-child(" . $exceptNCol . ")').addClass('rowNoClick');");
        if ($exceptLastCol) $view->registerJs("$('#" . $gridName . "-container table tr td:last-child').addClass('rowNoClick');");
        $view->registerJs("$('#" . $gridName . "-container table tr td:not(.rowNoClick)').css('cursor', 'pointer');");
        $view->registerJs("$('#" . $gridName . "-container table tr td:not(.rowNoClick)').click(function () { document.getElementById('" . $clickIdPrefix . "' + $(this).parent().data('key')).click(); });");
    }

    public static function setWidthLastCol($view, $gridName, $width) {
        $view->registerJs("$('#" . $gridName . "-container table tr td:last-child').css('min-width', " . $width . ");");
    }
}
