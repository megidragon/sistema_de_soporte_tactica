<?php

namespace app\utils\Excel;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Empresa;

class ExcelIncidentesExporter extends \PHPExcel
{
	private $arrayLetras    = array();
	private $arrayTitulos   = array();
	private $fileName;

	const TYPE                      = "Excel2007";


	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Empresa
	 */
	private $empresa;

	public function __construct() {
		parent::__construct();
		$this->arrayLetras = $this->getArrayLetras();
		$this->setArrayTitulos();

//		$this->user     = User::findIdentity(Yii::$app->user->getId());
//		$this->empresa  = Empresa::findIdentity($this->user->id_empresa);
	}


	public function generar(ActiveDataProvider $dataProvider)
	{
		$arrayLetras    = $this->arrayLetras;
		$this->setPropiedades();

		$hoja   = $this->setActiveSheetIndex(0);
		$hoja->setTitle("LISTADO INCIDENTES");

		$fila   = 1;
		$col    = 0;
		$colInicial     = $col;
		$filaInicial    = $fila;
		$rangoCeldasTitulos = $this->setHeader($fila, $col, $hoja);

		//GENERO EL CONTENIDO
		$fila   = 6;
		foreach($dataProvider->getModels() as $data)
		{
			$col = $colInicial;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->id);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_empresa;
			if(isset($data->idEmpresa)){
				$textoCelda = $data->idEmpresa->nombre;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_contacto;
			if(isset($data->idContacto)){
				$textoCelda = $data->idContacto->nombre." ".$data->idContacto->apellido;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->prioridad);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->tipo);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_tipo;
			if(isset($data->idTipo)) {
				$textoCelda = $data->idTipo->descripcion;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_subtipo;
			if(isset($data->idSubtipo)) {
				$textoCelda = $data->idTipo->descripcion;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_elemento;
			if(isset($data->idElemento)) {
				$textoCelda = $data->idElemento->descripcion;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$textoCelda     = $data->id_producto;
			if(isset($data->idProducto)) {
				$textoCelda = $data->idProducto->codigo." ".$data->idProducto->nombre_sector;
			}
			$hoja->setCellValue($rangoCeldas, $textoCelda);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->descripcion);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->fecha_creacion);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->hora_creacion);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->fecha_vencimiento);
			$col++;

			$rangoCeldas    = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $data->hora_vencimiento);
			$col++;


			$fila++;
		}
		$fila--;
		$col--;


		//AJUSTO EL ANCHO DE LAS COLUMNAS
		for($iCol = $colInicial; $iCol <= $col; $iCol++)
		{
			$this->ajustarAnchoColumna($hoja, $arrayLetras[$iCol]);
		}

		$cod            = date("YmdHis");
//		$nombreEmpresa  = ($this->empresa != null) ? $this->empresa->nombre : "NULL";
		$nombreEmpresa  = $this->getNombreEmpresa();
		$nombreUsuario  = $this->getNombreUsuario();
		$nombreArchivo  = "LASSA-IncidentesWeb_".$nombreEmpresa."_".$nombreUsuario."_".$cod.".xlsx";
		$this->exportar($nombreArchivo);
	}



	private function setHeader($fila, $col, \PHPExcel_Worksheet $hoja)
	{
		$arrayLetras    = $this->arrayLetras;
		$arrayTitulos   = $this->arrayTitulos;
		$rangoReturn    = "";

		$colInicial     = $col;
		$filaInicial    = $fila;

		//FALTA HABILITAR EL MODULO EN EL SERVIDOR
//		$this->crearLogo($hoja);

		$col+=4;
		$fecha      = date("Y-m-d");
		$hora       = date("H:i:s");
//		$nombreEmpresa  = ($this->empresa != null) ? $this->empresa->nombre : "NULL";
		$nombreEmpresa  = $this->getNombreEmpresa();
		$nombreUsuario  = $this->getNombreUsuario();
		$tituloReporte  = " LASSA - Reporte de Incidentes Web - Empresa ".$nombreEmpresa." - Usuario ".$nombreUsuario. " - Fecha de Reporte ".$fecha." ".$hora;
		$rangoCeldas        = $arrayLetras[$col].$fila.":".$arrayLetras[($col+10)].$fila;
		$hoja->mergeCells($rangoCeldas);
		$rangoCeldas        = $arrayLetras[$col].$fila;
		$hoja->setCellValue($rangoCeldas, $tituloReporte);

		$fila+=4;
		$col-=4;
		foreach($arrayTitulos as $titulo)
		{
			$rangoCeldas        = $arrayLetras[$col].$fila;
			$hoja->setCellValue($rangoCeldas, $titulo);

			$col++;
		}
		$col--;

		$rangoReturn    = $arrayLetras[$colInicial].$filaInicial.":".$arrayLetras[$col].$fila;
		return $rangoReturn;
	}

	private function getNombreEmpresa()
	{
		$nombre = null;
		if($this->empresa != null){
			$nombre = str_replace(" ", "-", $this->empresa->nombre);  //REEMPLAZO LOS ESPACIOS POR GUIONES
		}

		return $nombre;
	}
	private function getNombreUsuario()
	{
		$nombre = null;
		if($this->user != null){
			$nombre = str_replace(" ", "-", $this->user->username);  //REEMPLAZO LOS ESPACIOS POR GUIONES
		}

		return $nombre;
	}

	protected function getArrayLetras()
	{
		$arrayLetras    = array();

		for($i = 'A'; $i <= 'Z'; $i++) {
			$arrayLetras[] = $i;
		}
		return $arrayLetras;
	}

	private function setArrayTitulos()
	{
		$arrayTitulos   = array(
			"ID", "EMPRESA", "CONTACTO", "PRIORIDAD", "TIPO PEDIDO", "CLASIFICADOR TIPO", "CLASIFICADOR SUBTIPO",
			"ELEMENTO", "DISPOSITIVO", "DESCRIPCION", "FECHA_CREACION", "HORA_CREACION", "FECHA_VENCIMIENTO",
			"HORA_VENCIMIENTO"
		);
		$this->arrayTitulos = $arrayTitulos;
	}

	public function setFileName($fileName){$this->fileName = $fileName;}


	/* ************* FUNCIONES PARA EL EXCEL PROPIAMENTE DICHO **************** */
	private function setPropiedades()
	{
		// Establecer propiedades
		$this   ->getProperties()
			->setCreator("Kaizen")
			->setLastModifiedBy("Kaizen")
			->setTitle("Reporte")
			->setSubject("Excel reporte")
			->setDescription("Excel ")
			->setKeywords("Excel reporte")
			->setCategory("Reporte");
	}

	protected function crearLogo($hoja)
	{
//		$rutaLogo       = Yii::getAlias('@webroot'). "/web/socs/1/logo.png";
		$rutaLogo       = "http://lassa.com.ar/soporte/socs/1/logo.png";
		$rangoCeldas    = "A1:C3";
		$this->crearImagen($rutaLogo, $hoja, $rangoCeldas, "A1", 60);
	}
	protected function crearImagen($rutaImagen, \PHPExcel_Worksheet $hoja, $rangoCeldas, $celdaUbicacion, $alto)
	{
		$size = getimagesize($rutaImagen);
		$w = $size[0];
		$h = $size[1];

		$gdImage    = imagecreatefrompng($rutaImagen);
		$white      = imagecolorallocate($gdImage, 255, 255, 255);
		imagefill($gdImage,0,0,$white);
//		$gdImage = imagecreatetruecolor($w, $h);
//		imagealphablending($gdImage, false);
//		imagesavealpha($gdImage, true);
//		imagecopyresampled($gdImage, $rutaImagen, 0, 0, 0, 0, $target_width, $target_height, $asset->a_image_width, $asset->a_image_height);


		$objDrawing = new \PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Sample image');
		$objDrawing->setDescription('Sample image');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(\PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
		$objDrawing->setMimeType(\PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight($alto);
		$objDrawing->setCoordinates($celdaUbicacion);
		$objDrawing->setWorksheet($hoja);
	}
	private function createImage($img)
	{
		$source_image = imagecreatefrompng($img);

		$source_imagex = imagesx($source_image);
		$source_imagey = imagesy($source_image);

		$dest_imagex = 200;
		$dest_imagey = 60;
		$dest_image = imagecreatetruecolor($dest_imagex, $dest_imagey);

		imagecopyresampled($dest_image, $source_image, 0, 0, 0, 0, $dest_imagex, $dest_imagey, $source_imagex, $source_imagey);

		imagesavealpha($dest_image, true);
		$trans_colour = imagecolorallocatealpha($dest_image, 0, 0, 0, 127);
		imagefill($dest_image, 0, 0, $trans_colour);

		imagepng($dest_image,"test1.png",1);
	}

	protected function ajustarAnchoColumna(\PHPExcel_Worksheet $hoja, $columna)
	{
		$hoja->getColumnDimension($columna)->setAutoSize(true);
	}

	private function exportar($nombreArchivo)
	{
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename=".$nombreArchivo);
		header('Cache-Control: max-age=0');

		$objWriter=\PHPExcel_IOFactory::createWriter($this, self::TYPE);

		//DESCARGA DE ARCHIVO
		$objWriter->save('php://output');
		exit;
	}


	public function getUser(){return $this->user;}
	public function setUser($user){$this->user = $user;}

	public function getEmpresa(){return $this->empresa;}
	public function setEmpresa($empresa){$this->empresa = $empresa;}



}