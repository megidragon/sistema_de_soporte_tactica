<?php // AVTPL

namespace app\utils;

use Yii;
use app\models\User;
use yii\rbac\Permission;

class PermissionsService
{

    public static function getTable($id_societe)
    {
        $authManager = Yii::$app->getAuthManager();
        $permissions = $authManager->getPermissions();
        $roles = array_keys(User::getArrayRole($id_societe));

        $rows = [];
        foreach ($permissions as $permission) {
            array_push($rows, [
                'name' => $permission->name,
                'permission' => Yii::t('app/permissions', $permission->description),
            ]);
        }
        foreach ($roles as $role) {
            $rolePermissions = $authManager->getPermissionsByRole($role);
            $permNumber = 0;
            foreach ($permissions as $permission) {
                $found = false;
                foreach ($rolePermissions as $rolePermission) {
                    if ($rolePermission->name == $permission->name) {
                        $found = true;
                        break;
                    }
                }
                $rows[$permNumber][$role] = $found;
                $permNumber++;
            }
        }
        // Delete rows for which all permissions are false
        // Delete unwanted permissions
        // Determine minimum role with permission
        for ($i = count($rows) - 1; $i >= 0; $i--) { 
            $anyTrue = false;
            foreach ($roles as $role) {
                if ($rows[$i][$role]) {
                    $anyTrue = true;
                    $rows[$i]['minrole'] = $role;
                    break;
                }
            }
            if ((!$anyTrue || in_array($rows[$i]['name'], Yii::$app->params['permissions-not-shown'])) && !in_array($rows[$i]['name'], Yii::$app->params['permissions-on-off'])) {
                unset($rows[$i]);
            }
        }
        return $rows;
    }
}
