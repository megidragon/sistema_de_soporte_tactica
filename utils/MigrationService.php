<?php // AVTPL

namespace app\utils;

use Yii;
use yii\rbac\DbManager;
use yii\base\InvalidConfigException;
use app\models\Societe;

class MigrationService
{
    public static function runSqlFile($file, $id_societe = 1, $id_start = 0) {
        $file_content = file($file);
        $connection = Yii::$app->db;
        $query = "";
        $ignore_error = false;
        foreach($file_content as $sql_line){
            if (strtolower(trim($sql_line)) == "-- ignore error --") $ignore_error = true;
            if (trim($sql_line) != "" && strpos($sql_line, "--") === false) {
                $query .= $sql_line;
                if (substr(rtrim($query), -1) == ';'){
                    echo $query;
                    $query = str_replace('@id_societe', $id_societe, $query);
                    $query = str_replace('@id_start', $id_start, $query);
                    if ($ignore_error) {
                        try {
                            $connection->createCommand($query)->execute();
                        } catch (\yii\db\Exception $e) {
                        }
                    } else {
                        $connection->createCommand($query)->execute();
                    }
                    $query = "";
                    $ignore_error = false;
                }
            }
        }
    }

    public static function insSafeAgencyParam($name, $value, $type, $only_admin, $desc_es) {
        self::deleteAgencyParam($name);
        $connection = Yii::$app->db;
        $query = "INSERT INTO `societe_params` (`name`, `value`, `type`, `only_admin`, `desc_es`, `id_societe`, `baja`) SELECT '@name', '@value', '$type', $only_admin, '$desc_es', `id`, 0 FROM `societe`";
        $query = str_replace('@name', $name, $query);
        $query = str_replace('@value', $value, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function insertAgencyParam($name, $value, $type, $only_admin, $desc_es) {
        $connection = Yii::$app->db;
        $query = "INSERT INTO `societe_params` (`name`, `value`, `type`, `only_admin`, `desc_es`, `id_societe`, `baja`) SELECT '@name', '@value', '$type', $only_admin, '$desc_es', `id`, 0 FROM `societe`";
        $query = str_replace('@name', $name, $query);
        $query = str_replace('@value', $value, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function replaceAgencyParam($name, $old_value, $new_value) {
        $connection = Yii::$app->db;
        $query = "UPDATE `societe_params` SET `value` = REPLACE(`value`, '@old_value', '@new_value') WHERE `name` = '@name'";
        $query = str_replace('@name', $name, $query);
        $query = str_replace('@old_value', $old_value, $query);
        $query = str_replace('@new_value', $new_value, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function resetAgencyParam($name, $new_value) {
        $connection = Yii::$app->db;
        $query = "UPDATE `societe_params` SET `value` = '@new_value' WHERE `name` = '@name'";
        $query = str_replace('@name', $name, $query);
        $query = str_replace('@new_value', $new_value, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function deleteAgencyParam($name) {
        $connection = Yii::$app->db;
        $query = "DELETE FROM `societe_params` WHERE `name` = '@name'";
        $query = str_replace('@name', $name, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function updateAgencyParam($societe, $name, $value) {
        $connection = Yii::$app->db;
        $query = "UPDATE `societe_params` socpar JOIN `societe` soc ON soc.`id` = socpar.`id_societe` AND socpar.`name` = '@param_name' AND soc.`name` = '@societe_name' SET socpar.`value` = '@value'";
        $query = str_replace('@societe_name', $societe, $query);
        $query = str_replace('@param_name', $name, $query);
        $query = str_replace('@value', $value, $query);
        echo $query . "\n";
        $connection->createCommand($query)->execute();
    }

    public static function createUniqueRole($roleName, $roleDesc) {
        $auth = self::getAuthManager();
        $role = $auth->createRole($roleName);
        $role->description = $roleDesc;
        $auth->add($role);
    }

    public static function createUniqueRoleAllSocs($roleName, $roleDesc) {
        $auth = self::getAuthManager();
        $role = $auth->createRole($roleName);
        $role->description = $roleDesc;
        $auth->add($role);
        $societes = Societe::find()->all();
        foreach ($societes as $societe) {
            $role = $auth->createRole('s#' . $societe->id . '-' . $roleName);
            $role->description = $roleDesc;
            $auth->add($role);
        }
    }

    public static function assignUniquePermission($roleName, $permName) {
        $auth = self::getAuthManager();
        $role = $auth->getRole($roleName);
        $permission = $auth->getPermission($permName);
        $auth->addChild($role, $permission);
    }

    public static function assignUniqueSubrole($roleName, $subroleName) {
        $auth = self::getAuthManager();
        $role = $auth->getRole($roleName);
        $subrole = $auth->getRole($subroleName);
        $auth->addChild($role, $subrole);
    }

    public static function assignUniqueSubroleAllSocs($roleName, $subroleName) {
        $auth = self::getAuthManager();
        $role = $auth->getRole($roleName);
        $subrole = $auth->getRole($subroleName);
        $auth->addChild($role, $subrole);
        $societes = Societe::find()->all();
        foreach ($societes as $societe) {
            $role = $auth->getRole('s#' . $societe->id . '-' . $roleName);
            $subrole = $auth->getRole('s#' . $societe->id . '-' . $subroleName);
            $auth->addChild($role, $subrole);
        }
    }

    public static function createPermission($roleName, $permName, $permDesc) {
        $auth = self::getAuthManager();
        $permission = $auth->createPermission($permName);
        $permission->description = $permDesc;
        $auth->add($permission);
        $role = $auth->getRole($roleName);
        $auth->addChild($role, $permission);
    }

    public static function createPermissionAllSocs($roleName, $permName, $permDesc) {
        $auth = self::getAuthManager();
        $permission = $auth->createPermission($permName);
        $permission->description = $permDesc;
        $auth->add($permission);
        $role = $auth->getRole($roleName);
        $auth->addChild($role, $permission);
        $societes = Societe::find()->all();
        foreach ($societes as $societe) {
            $role = $auth->getRole('s#' . $societe->id . '-' . $roleName);
            $auth->addChild($role, $permission);
        }
    }

    public static function removePermission($roleName, $permName) {
        $auth = self::getAuthManager();
        $permission = $auth->getPermission($permName);
        $role = $auth->getRole($roleName);
        $auth->removeChild($role, $permission);
        $auth->remove($permission);
    }

    public static function removePermissionAllSocs($roleName, $permName) {
        $auth = self::getAuthManager();
        $permission = $auth->getPermission($permName);
        $role = $auth->getRole($roleName);
        $auth->removeChild($role, $permission);
        $societes = Societe::find()->all();
        foreach ($societes as $societe) {
            $role = $auth->getRole('s#' . $societe->id . '-' . $roleName);
            $auth->removeChild($role, $permission);
        }
        $auth->remove($permission);
    }

    public static function createPermissionsOneSoc($id_societe) {
        $sql = "select * from auth_item where type = 1 and name not like 's#%';";
        $command = Yii::$app->db->createCommand($sql);
        $authitems = $command->queryAll();
        foreach ($authitems as $value) {
            self::createUniqueRole('s#' . $id_societe . '-' . $value['name'], $value['description']);
        }
        $sql = "select ai.name as parent, aic.child as child, aic2.type as typechild from auth_item ai, auth_item_child aic, auth_item aic2 where ai.name not like 's#%' and ai.name = aic.parent and aic.child = aic2.name;";
        $command = Yii::$app->db->createCommand($sql);
        $authitems = $command->queryAll();
        foreach ($authitems as $value) {
            if ($value['typechild'] == 1) {
                self::assignUniqueSubrole('s#' . $id_societe . '-' . $value['parent'], 's#' . $id_societe . '-' . $value['child']);
            } elseif ($value['typechild'] == 2) {
                self::assignUniquePermission('s#' . $id_societe . '-' . $value['parent'], $value['child']);
            }
        }
    }

    public static function removePermissionsOneSoc($id_societe) {
        $sql = "delete from auth_item_child where parent like 's#" . $id_societe . "-%';";
        $command = Yii::$app->db->createCommand($sql);
        $command->execute();
        $sql = "delete from auth_item where name like 's#" . $id_societe . "-%';";
        $command = Yii::$app->db->createCommand($sql);
        $command->execute();
    }

    public static function invalidatePermissionsCache() {
        $auth = self::getAuthManager();
        $auth->invalidateCache();
    }

    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    private static function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }
}