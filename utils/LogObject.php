<?php // AVTPL

namespace app\utils;

use Yii;

class LogObject
{

    public static function logObj($object) {
        $filename = \Yii::$app->basePath . '/runtime/logs/objects.html';
        $fn = fopen($filename, 'a');
        ob_start();
        var_dump($object);
        $logstr = ob_get_clean();
        fwrite($fn, $logstr);
        fclose($fn);
    }

    public static function logDebTxt($text) {
        Yii::info($text, 'deb');
    }
}