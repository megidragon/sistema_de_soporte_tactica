<?php // AVTPL

namespace app\utils;

use Yii;
use app\models\Datalog;

class RegisterDatalog
{

    public static function register($code, $data, $info, $date = '', $time = '') {
        if ($date == '') $date = date('Y-m-d');
        if ($time == '') $time = date('H:i:s');
        $datalog = new Datalog();
        $datalog->code = $code;
        $datalog->data = $data;
        $datalog->info = $info;
        $datalog->date_reg = $date;
        $datalog->time_reg = $time;
        $datalog->save();
    }

    public static function retrieve($code) {
        $logs = Datalog::find()->orderBy('id desc')->all();
        $result = '<table>';
        foreach ($logs as $log) {
            $result .= '<tr>';
            $result .= '<td>' . $log->code . '</td>';
            $result .= '<td>' . $log->data . '</td>';
            $result .= '<td>' . $log->info . '</td>';
            $result .= '<td>' . $log->date_reg . '</td>';
            $result .= '<td>' . $log->time_reg . '</td>';
            $result .= '</tr>';
        }
        $result .= '</table>';
        return $result;
    }

}