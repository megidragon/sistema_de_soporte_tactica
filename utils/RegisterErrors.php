<?php // AVTPL

namespace app\utils;

use Yii;
use app\models\Errors;
use app\models\User;

class RegisterErrors
{

    public static function registerWrongSociete($userId, $url) {
        self::register($userId, $url, 'WRONG_SOCIETE');
    }

    public static function registerWrongClientID($userId, $url) {
        self::register($userId, $url, 'WRONG_CLIENT_ID');
    }

    public static function registerWrongDriverID($userId, $url) {
        self::register($userId, $url, 'WRONG_DRIVER_ID');
    }

    private static function register($userId, $url, $type) {
        $error = new Errors();
        $error->type = $type;
        $error->id_user = $userId;
        $error->url = $url;
        $error->date_reg = date('Y-m-d');
        $error->time_reg = date('H:i:s');
        $error->id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $error->save();
    }


}