<?php // AVTPL

namespace app\utils;

use Yii;
use app\models\Societe;
use app\models\SocieteParams;
use app\models\User;

class MainService
{
    public static function getAgencyParameters($id_societe = null) {
        if ($id_societe == null) $id_societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $societe = Societe::findOne(['id' => $id_societe]);
        $array = $societe->getAttributes(['name', 'sidebar_collapse', ]);
        $params = SocieteParams::find()->where(['id_societe' => $id_societe])->all();
        foreach ($params as $param) {
            $array[$param->name] = $param->value;
        }
        // AVTPL EVO INI SocieteParamAddParameters
        // AVTPL EVO END SocieteParamAddParameters
        return $array;
    }

}
