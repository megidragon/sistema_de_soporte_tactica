<?php // AVTPL
return [
    'MODAL_LOAD_ERROR_HEADER' => '<h4>Error</h4>',
    'MODAL_LOAD_ERROR_CONTENT' => '<div class="alert alert-danger">There was an error while the Web server was processing your request</div><p>Please verify your internet connection and whether you are still logged into the application.</p><p>Please contact us if you think this is a server error. Thank you.</p><br><button type="button" class="btn btn-default" onclick="location.reload()">Reload</button>',
];
