<?php // AVTPL
return [
    'Id' => 'Id',
    'Tacticaid' => 'ID Táctica',
    'Codigo' => 'Código',
    'Nro Serie' => 'Nro Serie',
    'Nombre Sector' => 'Nombre Sector',
    'Datos Red' => 'Datos de Red',
    'Mac Address' => 'MAC Address',
    'Fabricante' => 'Fabricante',
    'Posicion' => 'Posición',
    'Id Prioridad' => 'Prioridad',
    'Id Empresa' => 'Empresa',
    'Id Societe' => 'Id Societe',
    'Baja' => 'Baja',
    
];