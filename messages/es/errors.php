<?php // AVTPL
return [
    'MODAL_LOAD_ERROR_HEADER' => '<h4>Error</h4>',
    'MODAL_LOAD_ERROR_CONTENT' => '<div class="alert alert-danger">Ha ocurrido un error al procesar su consulta</div><p>Verifique que está conectado a internet y que continúa registrado en el sistema.</p><p>Por favor contáctenos si piensa que este es un error del sistema. Muchas gracias.</p><br><button type="button" class="btn btn-default" onclick="location.reload()">Recargar</button>',
];
