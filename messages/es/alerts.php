<?php // AVTPL
return [
    'ONE_NEW_F' => '1 Nueva',
    'MANY_NEW_F' => '_NUM_ Nuevas',
    'ONE_NEW_M' => '1 Nuevo',
    'MANY_NEW_M' => '_NUM_ Nuevos',
    'Notifications' => 'Notificaciones',
    'Press here to access' => 'Presione aquí para acceder',
    // AVTPL EVO INI AlertMessagesAdd
    // AVTPL EVO END AlertMessagesAdd
];
