<?php // AVTPL
return [
    'Id' => 'Id',
    'Username' => 'Usuario',
    'Auth Key' => 'Auth Key',
    'Password Hash' => 'Password Hash',
    'Password Reset Token' => 'Password Reset Token',
    'Email' => 'Email',
    'Role' => 'Rol',
    'Status' => 'Estado',
    'Nombre' => 'Nombre',
    'Id Empresa' => 'Empresa',
    'Id Contacto' => 'Contacto',
    'Id Societe' => 'Id Societe',
    'Sidebar Collapse' => 'Estado Menú',
    'Baja' => 'Baja',
    
];