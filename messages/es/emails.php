<?php // AVTPL
return [
    'Welcome to {name}!' => 'Bienvenido a {name}!',
    'Your user is: {username}' => 'Su usuario es: {username}',
    'Your password is: {password}' => 'Su contraseña es: {password}',
    'Access URL: {url}' => 'URL de acceso: {url}',
    'Please check the welcome notification once you login, for instructions on how to change your password.' => 'Por favor, compruebe la notificación de bienvenida una vez que se conecte, para obtener instrucciones sobre cómo cambiar su contraseña.',
    'Hello {username}' => 'Hola {username}',
    'Use the folloging link to reset your password:' => 'Utilice el siguiente enlace para establecer su clave:',
    'We send you a trip schedule made from {agency}\'s application!' => 'Te enviamos los datos de una reserva de servicio realizada desde la aplicación de {agency}!',
    'We send you a just dispatched trip made from {agency}\'s application!' => 'Te enviamos los datos de un servicio recién despachado desde la aplicación de {agency}!',
    'We send you a just modified trip made from {agency}\'s application!' => 'Te enviamos los datos de un servicio recién modificado desde la aplicación de {agency}!',
    'We send you a liquidation made from {agency}\'s application!' => 'Te enviamos una liquidación realizada desde la aplicación de {agency}!',
    'New Scheduled Trip from {name}! (#{num})' => 'Nueva reserva de {name}! (#{num})',
    'Dispatched Trip from {name}! (#{num})' => 'Servicio despachado de {name}! (#{num})',
    'Modified Trip from {name}! (#{num})' => 'Modificación de servicio de {name}! (#{num})',
    'New Liquidation from {name}!' => 'Nueva liquidación de {name}!',
];
