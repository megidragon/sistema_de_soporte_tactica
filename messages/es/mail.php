<?php // AVTPL
return [
    'ID' => 'ID',
    'Entry ID' => 'ID Entrada',
    'Created' => 'Creado',
    'Successful' => 'Enviado OK',
    'From' => 'De',
    'To' => 'Para',
    'Reply' => 'Responder',
    'CC' => 'CC',
    'BCC' => 'BCC',
    'Subject' => 'Asunto',
    'Text Body' => 'Cuerpo Texto',
    'HTML Body' => 'Cuerpo HTML',
    'Data' => 'Información',
    'Download eml file' => 'Descargar archivo eml',
    'Email #{id}' => 'Email #{id}',
    'Download' => 'Descargar',
];
