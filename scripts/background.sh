#!/bin/bash

# Script called by job service in /etc/init/tacticajob.conf:
#   description "Job that imports / exports data for Tactica"
#   author "Ariel Vila"
#   start on runlevel [2345]
#   exec /var/www/html/soporte/scripts/background.sh

# If this script must be modified, then it is necessary to manually restart the service by using:
#   sudo service tacticajob stop
#   sudo service tacticajob start

BASEPATH="`dirname $0`/.."

while true;
do
    $BASEPATH/scripts/process.sh $BASEPATH
done
