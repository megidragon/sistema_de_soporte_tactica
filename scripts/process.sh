#!/usr/bin/env bash
# This script is called by background.sh

BASEPATH=$1

# Import ticket configuration
if [ ! -f $BASEPATH/runtime/import/tactica.flag ]
then
    if [ -e $BASEPATH/runtime/import/lassa-exportacion-contactos.txt ]
    then
        echo "dummy" > $BASEPATH/runtime/import/web.flag
        $BASEPATH/yii import >> $BASEPATH/runtime/logs/import.log 2>&1
        echo `date` > $BASEPATH/runtime/logs/import.last
        mkdir -p $BASEPATH/runtime/import/backup/txts 2> /dev/null
        mv $BASEPATH/runtime/import/lassa-exportacion-contactos.txt $BASEPATH/runtime/import/backup/txts
        mv $BASEPATH/runtime/import/lassa-exportacion-empresas.txt $BASEPATH/runtime/import/backup/txts
        mv $BASEPATH/runtime/import/lassa-exportacion-incidentestipos.txt $BASEPATH/runtime/import/backup/txts
        mv $BASEPATH/runtime/import/lassa-exportacion-productos.txt $BASEPATH/runtime/import/backup/txts

        #AGREGADO PARA LA MODIFICACION SOBRE EL NUEVO ARCHIVO A TRABAJAR
        mv $BASEPATH/runtime/import/lassa-exportacion-soporte.txt $BASEPATH/runtime/import/backup/txts

        tar -zcf $BASEPATH/runtime/import/backup/`date +"%Y-%m-%d-%H-%M-%S"`-txts.tgz $BASEPATH/runtime/import/backup/txts
        # Leave only last 100 backup files
        BAKQTTY=`ls $BASEPATH/runtime/import/backup/*.tgz | wc -l`
        BAKQTTY=$((BAKQTTY - 100))
        if [ $BAKQTTY -gt 0 ]
        then
            ls $BASEPATH/runtime/import/backup/*.tgz | head -n $BAKQTTY | while read EACH
            do
                rm $EACH
            done
        fi
        rm $BASEPATH/runtime/import/web.flag
    fi
fi

# Export tickets
if [ ! -f $BASEPATH/runtime/export/tactica.flag ]
then
    echo "dummy" > $BASEPATH/runtime/export/web.flag
    $BASEPATH/yii export >> $BASEPATH/runtime/logs/export.log 2>&1
    echo `date` > $BASEPATH/runtime/logs/export.last
    if [ -f $BASEPATH/runtime/export/lassa-importacion-tickets.xls ]
    then
        chmod 666 $BASEPATH/runtime/export/lassa-importacion-tickets.xls
    fi
    rm $BASEPATH/runtime/export/web.flag
fi
sleep 60
