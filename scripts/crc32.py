#!/usr/bin/python
import os, sys
import zlib

def main():
    if len(sys.argv) != 2:
        print 'Usage: ', sys.argv[0], 'filename'
        exit()

    if os.path.isfile(sys.argv[1]):
        print crc32(sys.argv[1])
    else:
        print 'Usage: ', sys.argv[0], 'filename'
        exit()

def crc32(filename):
    prev = 0
    for eachline in open(filename,"rb"):
        prev = zlib.crc32(eachline, prev)
    return "%X" % (prev & 0xFFFFFFFF)

main()
