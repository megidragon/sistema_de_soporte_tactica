#!/usr/bin/python
import os
import time
import paramiko
import logging
import logging.handlers
import zlib
from shutil import move
from config import *

def main():
	try:
		client = get_client()
		send_txt_files(client)
		receive_xls_files(client)
		client.close()
	except Exception as e:
		print 'ERROR:', e.message
		logger.exception(e.message)

def send_txt_files(client):
	try:
		if not os.path.exists(local_tmp_dir):
			os.makedirs(local_tmp_dir)
		sftp = client.open_sftp()
		try:
			sftp.chdir(remote_tmp_dir)
		except IOError:
			sftp.mkdir(remote_tmp_dir)
		retry = 3
		while retry > 0:
			if not check_remote_file(sftp, remote_exp_dir + '/web.flag'):
				files = [{'name': 'lassa-exportacion-contactos.txt', 'req': True}, {'name': 'lassa-exportacion-empresas.txt', 'req': True}, {'name': 'lassa-exportacion-incidentestipos.txt', 'req': True}, {'name': 'lassa-exportacion-productos.txt', 'req': True}, {'name': 'lassa-exportacion-log-importacion.txt', 'req': False}]
				proceed = True
				for file in files:
					if file['req'] and not os.path.isfile(os.path.join(local_exp_dir, file['name'])):
						retry = 0
						proceed = False
				if proceed:
					for file in files:
						if os.path.isfile(os.path.join(local_exp_dir, file['name'])) and not is_file_accesible(os.path.join(local_exp_dir, file['name'])):
							time.sleep(30)
							retry -= 1
							proceed = False
				if proceed:
					for file in files:
						if os.path.isfile(os.path.join(local_exp_dir, file['name'])):
							try:
								move(os.path.join(local_exp_dir, file['name']), os.path.join(local_tmp_dir, file['name']))
							except Exception:
								retry = 0
								proceed = False
				if proceed:
					client.exec_command('echo x > ' + remote_exp_dir + '/tactica.flag')
					for file in files:
						name = file['name']
						if os.path.isfile(os.path.join(local_tmp_dir, name)):
							print 'Transferring', name, '...'
							sftp.put(os.path.join(local_tmp_dir, name), remote_tmp_dir + '/' + name)
							sftp.chmod(remote_tmp_dir + '/' + name, 0666)
							localcrc = crc32(os.path.join(local_tmp_dir, name))
							os.remove(os.path.join(local_tmp_dir, name))
							(stdin, stdout, stderr) = client.exec_command(remote_crc32 + ' ' + remote_tmp_dir + '/' + name)
							remotecrc = next(iter(stdout.readlines() or []), '').rstrip() # first line
							if localcrc == remotecrc:
								client.exec_command(''.join(['mv ', remote_tmp_dir, '/', name, ' ', remote_exp_dir, '/', name]))
								print '  CRC OK'
							else:
								logger.exception('CRC error on file ' + name)
								print '  CRC ERROR'
				try:
					sftp.remove(remote_exp_dir + '/tactica.flag')
				except:
					pass
			else:
				time.sleep(30)
				retry -= 1
	except Exception as e:
		print 'ERROR:', e.message
		logger.exception(e.message)

def receive_xls_files(client):
	try:
		if not os.path.exists(local_tmp_dir):
			os.makedirs(local_tmp_dir)
		sftp = client.open_sftp()
		retry = 3
		while retry > 0:
			if not check_remote_file(sftp, remote_imp_dir + '/web.flag'):
				retry = 0
				files = ['lassa-importacion-tickets.xls']
				client.exec_command('echo x > ' + remote_imp_dir + '/tactica.flag')
				for file in files:
					if check_remote_file(sftp, remote_imp_dir + '/' + file):
						print 'Transferring', file, '...'
						sftp.get(remote_imp_dir + '/' + file, os.path.join(local_tmp_dir, file))
						localcrc = crc32(os.path.join(local_tmp_dir, file))
						(stdin, stdout, stderr) = client.exec_command(remote_crc32 + ' ' + remote_imp_dir + '/' + file)
						remotecrc = next(iter(stdout.readlines() or []), '').rstrip() # first line
						if localcrc == remotecrc:
							sftp.remove(remote_imp_dir + '/' + file)
							move(os.path.join(local_tmp_dir, file), os.path.join(local_imp_dir, file))
							print '  CRC OK'
						else:
							logger.exception('CRC error on file ' + file)
							print '  CRC ERROR'
				try:
					sftp.remove(remote_imp_dir + '/tactica.flag')
				except:
					pass
			else:
				time.sleep(30)
				retry -= 1
	except Exception as e:
		print 'ERROR:', e.message
		logger.exception(e.message)

def crc32(filename):
    prev = 0
    for eachline in open(filename,"rb"):
        prev = zlib.crc32(eachline, prev)
    return "%X" % (prev & 0xFFFFFFFF)

def check_remote_file(sftp, file):
	try:
		dummy = sftp.stat(file)
		return True
	except IOError:
		return False

def is_file_accesible(file):
	try:
		fp = open(file)
		fp.close()
		return True
	except IOError:
		return False

def get_client():
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(host, username=username, password=password, allow_agent=False, look_for_keys=False)
	return client

def set_loggers():
	logger = logging.getLogger()
	smtp_handler = logging.handlers.SMTPHandler(mailhost=(smtp_host, smtp_port),
												credentials=(smtp_user, smtp_pass),
												fromaddr=smtp_user,
												toaddrs=email_to,
												subject=u"Python Script error!",
												secure=())
	file_handler = logging.FileHandler(log_file)
	formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
	file_handler.setFormatter(formatter)
	logger.addHandler(smtp_handler)
	logger.addHandler(file_handler)
	return logger

logger = set_loggers()
main()
