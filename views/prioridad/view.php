<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Prioridad */

$this->title = 'Prioridad';
$this->params['breadcrumbs'][] = ['label' => 'Prioridades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prioridad-view">

    <?php
    $unidad = [
        'HORA' => 'Hora(s)',
        'DIA' => 'Día(s)',
        'SEMANA' => 'Semana(s)',
        'MES' => 'Mes(es)',
    ];
?>
    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            'codigo',
            ['attribute' => 'unidad', 'value' => $unidad[$model->unidad]],
            'cantidad',

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
