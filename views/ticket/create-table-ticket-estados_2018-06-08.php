<?php

//use Yii;

echo "Creando tabla de ticket estados";
echo "<br>";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
            CREATE TABLE IF NOT EXISTS `ticket_estados` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `id_baja` int(11) NOT NULL ,
              `descripcion` TEXT CHARACTER SET utf8 DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
            
            INSERT INTO `ticket_estados` (id_baja, descripcion) VALUES
              ('0', 'ABIERTO'),
              ('1', 'ABIERTO'),
              ('2', 'ABIERTO'),
              ('3', 'ABIERTO'),
              ('4', 'CERRADO')
        ", [':start_date' => '1970-01-01']);

try{
    $result = $command->queryAll();
}catch (\Yii\db\Exception $exception){
//    echo $exception->getMessage();
}


echo "<br>";
echo "Fin de la creacion de tabla ticket estados";