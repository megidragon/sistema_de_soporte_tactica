<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use kartik\checkbox\CheckboxX;
use app\models\Empresa;
use app\models\Contacto;
use app\models\Tipo;
use app\models\Subtipo;
use app\models\Elemento;
use app\models\Producto;
use app\models\Societe;

$jsversion = Yii::$app->params['jsversion'];
?>

<script type="text/javascript">
    <?= "var action = '" . $action . "';\n" ?>
    <?= 'var readonly = ' . ($readonly ? 'true' : 'false') . ";\n" ?>
    <?= 'var ajax = ' . ($ajax ? 'true' : 'false') . ";\n" ?>
	validateNewTicket   = true;
</script>
<style>
    .input-container {
        margin: 3em auto;
        max-width: 300px;
        background-color: #EDEDED;
        border: 1px solid #DFDFDF;
        border-radius: 5px;
    }
    .file-info {
        font-size: 0.9em;
    }
    .browse-btn {
        background: #dd4b39;
        color: #fff;
        min-height: 35px;
        padding: 10px;
        border: none;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }
    .browse-btn:hover {
        background: #4ec0b4;
    }
    @media (max-width: 300px) {
        button {
            width: 100%;
            border-top-right-radius: 5px;
            border-bottom-left-radius: 0;
        }

        .file-info {
            display: block;
            margin: 10px 5px;
        }
    }

</style>
<?php if (!$ajax) { ?>
    
<?php } ?>
<div style="display: none;" id="crudTexts" data-confirm-duplicate="<?= Yii::t('app', 'Confirm save this element as new? (all pending changes will be saved in the new element only)') ?>" data-confirm-recover="<?= Yii::t('app', 'Confirm recover this element?') ?>" data-confirm-delete="<?= Yii::t('app', 'Confirm delete this element?') ?>" data-choose="<?= Yii::t('app', 'Choose') ?>"></div>

<div class="ticket-form">

    <?php 
        $validationUrl = '';
        if ($action == 'create') $validationUrl = 'ticket/validate-create';
        if ($action == 'update') $validationUrl = 'ticket/validate-update?id=' . $model->id;
        $form = ActiveForm::begin(['id' => 'crud-form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'options' => ['enctype' => 'multipart/form-data'],
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        ]);
        if ($action != 'create') {
            $this->registerJs("onChangeProduct();");
        }

        if($action == 'view')
        {
            $arrayTicketEstados = array();
            $searchModelTicketEstado = new \app\models\TicketEstadosSearch();
            $dataProviderTicketEstado = $searchModelTicketEstado->searchMin(-1);
            foreach($dataProviderTicketEstado->getModels() as $modelTicketEstado){
                $arrayTicketEstados[$modelTicketEstado->id_baja] = $modelTicketEstado->descripcion;
            }

            $arrayTicketUploads     = array();
            $searchTicketUploads    = new \app\models\TicketUpload();
            $arrayTicketUploads     = $searchTicketUploads->find()->where(['id_ticket' => $model->id])->all();
        }
    ?>
    <input type="hidden" name="aclose" class="neverReadonly" value="<?= $aclose ?>">
    <input type="hidden" id="submitType" name="submitType" class="neverReadonly" value="">
    <div class=" kv-fieldset-inline">
        <?php
        if($action == 'view')
        {
            ?>
            <div class="row">
                <?php $field = 'id' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>"Número de ticket",
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
            </div>
            <div class="row">
                <?php $field = 'baja' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $arrayTicketEstados[$model->$field], ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
            </div>
            <?php
        }
        ?>


        <div class="row">
            <?= Html::activeLabel($model, 'id_empresa', [
                'label'=>$model->getAttributeLabel('id_empresa'),
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'id_empresa', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Empresa::find()->andFilterWhere(['id' => $user->id_empresa])->all(),
                        'id', 'nombre'
                    ), 'options' => ['placeholder'=>Yii::t('app', 'Choose')], 'pluginOptions' => ['allowClear'=>true],
                    'disabled' => true,
                ]) ?>
            </div>
        </div>
    
        <div class="row">
            <?= Html::activeLabel($model, 'id_contacto', [
                'label'=>$model->getAttributeLabel('id_contacto'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'id_contacto', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Contacto::find()->andFilterWhere(['id' => $user->id_contacto])->all(), 
                        'id', function($model, $defaultValue) { return $model->nombre . ' ' . $model->apellido; }
                    ), 'options' => ['placeholder'=>Yii::t('app', 'Choose')], 'pluginOptions' => ['allowClear'=>true],
                    'disabled' => true,
                ]) ?>
            </div>
        </div>

        <div class="row">
            <?= Html::activeLabel($model, 'prioridad', [
                'label'=>$model->getAttributeLabel('prioridad'),
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <?php
            $prioridad = [
                ['id' => 'BAJA', 'descripcion' => 'Baja'],
                ['id' => 'MEDIA', 'descripcion' => 'Media'],
                ['id' => 'ALTA', 'descripcion' => 'Alta'],
            ];
            ?>
            <div class="col-sm-3">
                <?= $form->field($model, 'prioridad', ['showLabels'=>false])->dropDownList(
                    ArrayHelper::map($prioridad, 'id', 'descripcion'),
                    ['prompt'=>Yii::t('app', 'Choose')]
                ) ?>
            </div>
        </div>
        
        <div class="row">
            <?= Html::activeLabel($model, 'tipo', [
                'label'=>$model->getAttributeLabel('tipo'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <?php
                $tipo = [
                    ['id' => 'TECNICO', 'descripcion' => 'Soporte Técnico'],
                    ['id' => 'ADMINISTRACION', 'descripcion' => 'Comercial / Administración'],
                ];
            ?>
            <div class="col-sm-3">
                <?= $form->field($model, 'tipo', ['showLabels'=>false])->dropDownList(
                    ArrayHelper::map($tipo, 'id', 'descripcion'),
                    ['prompt'=>Yii::t('app', 'Choose')]
                ) ?>
            </div>
        </div>
        

        
        <div class="row">
            <?= Html::activeLabel($model, 'id_tipo', [
                'label'=>$model->getAttributeLabel('id_tipo'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-3">
                <?= $form->field($model, 'id_tipo', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Tipo::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])
                        ->orFilterWhere(['id' => $model->id_tipo])->all(), 
                        'id', 'descripcion'
                    ),
                    'options' => [
                        'placeholder'=>Yii::t('app', 'Choose'),
                        'onchange'=>
                            '$.post("' . Yii::$app->request->baseUrl . '/subtipo/get-for-tipo?id_tipo=" + $(this).val() + "&id=" + $(\'#ticket-id_subtipo\').val(), function(data) {
                                $( "select#ticket-id_subtipo" ).html(data);
                                $("#ticket-id_subtipo").change();
                            });'
                    ]
                ]) ?>
            </div>
            <?php
                $this->registerJS("$('#ticket-id_tipo').change();");
            ?>
        </div>
        
        <div class="row">
            <?= Html::activeLabel($model, 'id_subtipo', [
                'label'=>$model->getAttributeLabel('id_subtipo'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-3">
                <?= $form->field($model, 'id_subtipo', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Subtipo::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])
                        ->orFilterWhere(['id' => $model->id_subtipo])->all(), 
                        'id', 'descripcion'
                    ),
                    'options' => [
                        'placeholder'=>Yii::t('app', 'Choose'),
                        'onchange'=>
                            '$.post("' . Yii::$app->request->baseUrl . '/elemento/get-for-subtipo?id_subtipo=" + $(this).val() + "&id=" + $(\'#ticket-id_elemento\').val(), function(data) {
                                $( "select#ticket-id_elemento" ).html(data);
                            });'
                    ]
                ]) ?>
            </div>
            <?php
                $this->registerJS("$('#ticket-id_subtipo').change();");
            ?>
        </div>
        
        <div class="row">
            <?= Html::activeLabel($model, 'id_elemento', [
                'label'=>$model->getAttributeLabel('id_elemento'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-3">
                <?= $form->field($model, 'id_elemento', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Elemento::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])
                        ->orFilterWhere(['id' => $model->id_elemento])->all(), 
                        'id', 'descripcion'
                    ), 'options' => ['placeholder'=>Yii::t('app', 'Choose')]
                ]) ?>
            </div>
        </div>
        
        <div class="row">
            <?= Html::activeLabel($model, 'id_producto', [
                'label'=>$model->getAttributeLabel('id_producto'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'id_producto', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(
                        Producto::find()->andFilterWhere(['baja' => false, 'id_empresa' => $user->id_empresa])
                        ->orWhere(['baja' => false, 'id_empresa' => null])->orFilterWhere(['id' => $model->id_producto])->all(),
                        'id', function($model, $defaultValue) { return $model->codigo . ' - ' . $model->nombre_sector; }
                    ),
                    'options' => [
                        'placeholder'=>Yii::t('app', 'Choose'),
                        'onchange'=>'onChangeProduct()',
                    ]
                ]) ?>
            </div>
        </div>

        <div class="row">
            <?php $field = 'datos_red' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-10">
                <?= Html::textArea($field, '', [
                        'id' => $field,
                        'class' => 'form-control form-no-validate',
                        'readonly'=>'true',
                        'rows'  => 1,
                        'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
        </div>

        <div class="row">
            <?php $field = 'mac_address' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-10">
                <?= Html::textArea($field, '', [
                        'id' => $field,
                        'class' => 'form-control form-no-validate',
                        'readonly'=>'true',
                        'rows'  => 1,
                        'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
        </div>

        <div class="row">
            <?php $field = 'nro_serie' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-2">
                <?= Html::textInput($field, '', ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
            <?php $field = 'fabricante' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-1 control-label'
            ]) ?>
            <div class="col-sm-1">
                <?= Html::textInput($field, '', ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
            <?php $field = 'posicion' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-1 control-label'
            ]) ?>
            <div class="col-sm-2">
                <?= Html::textInput($field, '', ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
            <?php $field = 'prioridad_producto' ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>Yii::t('app/ticket', 'Priority'), 
                'class'=>'col-sm-1 control-label'
            ]) ?>
            <div class="col-sm-2">
                <?= Html::textInput($field, '', ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
            </div>
        </div>
        
        <div class="row">
            <?= Html::activeLabel($model, 'descripcion', [
                'label'=>$model->getAttributeLabel('descripcion'), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-10">
                <?= $form->field($model, 'descripcion',[
                    'showLabels'=>false
                ])->textArea(['placeholder'=>$model->getAttributeLabel('descripcion'), 'maxlength' => true, 'rows'  => 1,]); ?>
            </div>
        </div>

	    <?php
		    if($action == 'view') {
		    	?>
			    <div class="row">
				    <?php $field = 'fecha_creacion' ?>
				    <?= Html::activeLabel($model, $field, [
					    'label'=>$model->getAttributeLabel($field),
					    'class'=>'col-sm-2 control-label'
				    ]) ?>
				    <div class="col-sm-2">
					    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
				    </div>
				    <?php $field = 'hora_creacion' ?>
				    <?= Html::activeLabel($model, $field, [
					    'label'=>$model->getAttributeLabel($field),
					    'class'=>'col-sm-2 control-label'
				    ]) ?>
				    <div class="col-sm-2">
					    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
				    </div>
			    </div>
<?php //2018-09-19 CPS AGREGADO ?>
			    <div class="row">
				    <?php $field = 'fecha_vencimiento' ?>
				    <?= Html::activeLabel($model, $field, [
					    'label'=>$model->getAttributeLabel($field),
					    'class'=>'col-sm-2 control-label'
				    ]) ?>
				    <div class="col-sm-2">
					    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
				    </div>
				    <?php $field = 'hora_vencimiento' ?>
				    <?= Html::activeLabel($model, $field, [
					    'label'=>$model->getAttributeLabel($field),
					    'class'=>'col-sm-2 control-label'
				    ]) ?>
				    <div class="col-sm-2">
					    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
				    </div>
			    </div>
	            <?php
		    }
	    ?>
        
        <div class="row">
            <?php /*$field = 'fecha_vencimiento' */?><!--
            <?/*= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field),
                'class'=>'col-sm-2 control-label'
            ]) */?>
            <div class="col-sm-2">
                <?/*= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) */?>
            </div>
            <?php /*$field = 'hora_vencimiento' */?>
            <?/*= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field),
                'class'=>'col-sm-2 control-label'
            ]) */?>
            <div class="col-sm-2">
                <?/*= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) */?>
            </div>-->
	        <?php
		        if($action != 'create')
		        {
		        	?>
			        <?php $field = 'tactica_soporte_usuario_asign' ?>
			        <?= Html::activeLabel($model, $field, [
					        'label'=>$model->getAttributeLabel($field),
					        'class'=>'col-sm-2 control-label'
				        ]) ?>
			        <div class="col-sm-2">
				        <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
			        </div>
	                <?php
		        }
	        ?>
        </div>
        
        <?php
        if($action == 'create')
        {
            ?>
            <div class="row">
	            
                <div class="col-sm-12">
                    <?=
                        $form->field($model, 'file[]')
                            ->fileInput(['multiple' => true, 'style'=>'display:none;', 'id'=>'attack_file'])
                            ->label($model->getAttributeLabel('file'), ['class' => 'control-label col-sm-2']);
                    ?>
                    <div class="input-container">
                        <button class="browse-btn" type="button">
                            Buscar archivos
                        </button>
                        <span class="file-info">Subir un archivo</span>
                    </div>
                </div>
            </div>
            <?php
        }



        if($action == 'view')
        {
            ?>
            <div class="row">
                <?php $field = 'tactica_fecha_modificacion' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
                <?php $field = 'tactica_hora_modificacion' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
                <?php $field = 'tactica_user_modificacion' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
            </div>
            <div class="row">
                <?php $field = 'tactica_fecha_cierre' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
                <?php $field = 'tactica_hora_cierre' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
                <?php $field = 'tactica_user_cierre' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-2">
                    <?= Html::textInput($field, $model->$field, ['id' => $field, 'class' => 'form-control form-no-validate', 'readonly'=>'true', 'placeholder'=>$model->getAttributeLabel($field)]) ?>
                </div>
            </div>
            <div class="row">
                <?php $field = 'tactica_solucion_descr' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label',
                ]) ?>
                <div class="col-sm-10">
                    <?= Html::textArea($field, $model->$field, [
                            'id' => $field,
                            'class' => 'form-control form-no-validate',
                            'readonly'=>'true',
                            'rows'  => 1,
                            'placeholder'=>$model->getAttributeLabel($field)
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <?php $field = 'file_attached' ?>
                <?= Html::activeLabel($model, $field, [
                    'label'=>$model->getAttributeLabel($field),
                    'class'=>'col-sm-2 control-label'
                ]) ?>
                <div class="col-sm-6">
                    <?php
                    if(count($arrayTicketUploads) > 0)
                    {
                        foreach($arrayTicketUploads as $ticketUpload)
                        {
	                        //DETECTO EN QUE DIRECTORIO ESTÁ EL ARCHIVO
	                        $numDirectorio  = 1;
	                        $miles          = floor($model->id / 1000) * 1000;
	                        $numDirectorio  +=  $miles;
	                        $numCompletoDirectorio  = 100000000 + $numDirectorio;
	                        $numCompletoDirectorio  = substr($numCompletoDirectorio, 1, 8);
	                        $directorio = $numCompletoDirectorio;
                        	
                            $nombreArchivo  = $ticketUpload->upload;
                            $rutaFile       = Yii::$app->params['dir_uploads'].$directorio."/".$nombreArchivo;
                            $pathFile       = Yii::$app->basePath."/web/".$rutaFile;
                            
                            $downloadFile   = "../".$rutaFile;
                            ?>
                            <a href="<?php echo $downloadFile;?>" style="display: block;" download><?php echo $nombreArchivo;?></a>
                            <?php
                        }
                    }
                    else
                    {
                        ?>
                        <span style="display: block; padding-top: 7px;">No tiene archivos adjuntos</span>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="row">
            <?php if ($action == 'delete') { ?>
                <?php if (isset($ref_qtty) && $ref_qtty) { ?>
                    <label class="col-sm-4 control-label"><?= $ref_msg ?></label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'new_id', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(
                                \app\models\Ticket::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])->andFilterWhere(['not', ['id' => $model->id]])->all(), 
                                'id', 'descripcion'
                            ), 'options' => ['placeholder'=>Yii::t('app', 'Do Not Replace'), 'class' => 'neverReadonly'], 'pluginOptions' => ['allowClear'=>true]

                        ]) ?>
                    </div>
                <?php } elseif (isset($ref_msg)) { ?>
                    <label class="col-sm-7 control-label"><?= $ref_msg ?></label>
                <?php } else { ?>
                    <div class="col-sm-7"></div>
                <?php } ?>
                <div class="col-sm-5">
            <?php } else { ?>
                <div class="col-sm-12">
            <?php } ?>
                <?php
                    switch ($action) {
                        case 'create':
                            echo Html::button(Yii::t('app', 'Send'), ['class' => 'btn btn-success crudBtn', 'onclick' => 'setSubmitType(SUBMIT_MAIN, true);']);
                            break;
                        case 'update':
                            echo Html::button(Yii::t('app', 'Update'), ['class' => 'btn btn-primary crudBtn', 'onclick' => 'setSubmitType(SUBMIT_MAIN);']);
                            break;
                        case 'delete':
                            echo Html::button(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger crudBtn neverReadonly', 'onclick' => 'setSubmitType(SUBMIT_MAIN);']);
                            break;
                    }
                    if ($model->baja && $action == 'update' && Yii::$app->user->can('none')) {
                        echo Html::button(Yii::t('app', 'Recover'), ['class' => 'btn btn-info crudBtn', 'onclick' => 'setSubmitType(SUBMIT_RECOVER);']);
                    }
                    if (!$model->baja && $action == 'update' && $candelete && Yii::$app->user->can('none')) {
                        echo Html::button(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger crudBtn', 'onclick' => 'setSubmitType(SUBMIT_DELETE);']);
                    }
                    if ($ajax) {
                        echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default crudBtn neverReadonly', 'onclick' => "if ($('#crud-form').hasClass('dirty')) { if (confirm('Si continúa, se perderán los cambios. Confirma?')) { $('#crudModal').modal('hide');  $('#crud-form').trigger('reinitialize.areYouSure'); } } else { $('#crudModal').modal('hide'); }"]);
                    } else {
                        if ($aclose) {
                            echo Html::a(Yii::t('app', 'Close'), ['aclose'], ['class' => 'btn btn-default crudBtn']);
                        } elseif ($action == 'create') {
                            echo Html::a(Yii::t('app', 'Cancel'), ['site/index'], ['class' => 'btn btn-default crudBtn']);
                        } else {
                            echo Html::a(Yii::t('app', 'Return'), ['ticket/index'], ['class' => 'btn btn-default crudBtn']);
                        }
                    }
                    if ($action != 'create' && Yii::$app->user->can('history') && true && $model->baja != 2) {
                        echo '<div class="a-button">';
                        echo Html::a('<i class="glyphicon glyphicon-time"></i>', ['log', 'id'=>$model->id], ['target'=>'_blank', 'class'=>'btn crudBtn', 'title'=>Yii::t('app', 'History')]);
                        echo '</div>';
                    }
                    echo Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form']);
                ?>
            </div>
        </div>
    </div>
    
    <?php 
        $this->registerJsFile(Yii::$app->request->baseUrl . '/js/crud.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
        $this->registerJsFile(Yii::$app->request->baseUrl . '/js/ticket.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
        $this->registerJs("$('.required').parent().prev().addClass('required');");
        $this->registerJs("crudInit();");
        $this->registerJs("$('form#crud-form').on('beforeSubmit', function(e) { return crudSubmit(); });");
        $this->registerJs("$('#crud-form').areYouSure({message: '" . Yii::t('app', 'All changes will be lost. Confirm?') . "'});");
        $this->registerJs("textareaFitContent();");
        $this->registerJs("const uploadButton = document.querySelector('.browse-btn');
    const fileInfo = document.querySelector('.file-info');
    const realInput = document.getElementById('attack_file');
    uploadButton.addEventListener('click', () => {
        realInput.click();
    });
    realInput.addEventListener('change', () => {
        const name = realInput.value.split(/\\|\//).pop();
        const truncated = name.length > 20
            ? name.substr(name.length - 20)
            : name;

        fileInfo.innerHTML = truncated;
    });");
    ?>
    <?php ActiveForm::end(); ?>

</div>
<?php if (!$ajax) { ?>

<?php } ?>
