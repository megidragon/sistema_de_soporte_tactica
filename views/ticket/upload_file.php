<?php
use kartik\widgets\ActiveForm;

?>

<style>
    .input-container {
        margin: 3em auto;
        max-width: 300px;
        background-color: #EDEDED;
        border: 1px solid #DFDFDF;
        border-radius: 5px;
    }
    .file-info {
        font-size: 0.9em;
    }
    .browse-btn {
        background: #dd4b39;
        color: #fff;
        min-height: 35px;
        padding: 10px;
        border: none;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }
    .browse-btn:hover {
        background: #4ec0b4;
    }
    @media (max-width: 300px) {
        button {
            width: 100%;
            border-top-right-radius: 5px;
            border-bottom-left-radius: 0;
        }

        .file-info {
            display: block;
            margin: 10px 5px;
        }
    }
</style>
<div class="row">
 <div class="col-sm-12">
     <?php
     $form = ActiveForm::begin(['id' => 'crud-form',
         'type' => ActiveForm::TYPE_HORIZONTAL,
         'action' => ['ticket/upload-file', 'id' => $model->id],
         'options' => ['enctype' => 'multipart/form-data', 'method'=>'POST'],
         'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
     ]);
     ?>
        <?=
        $form->field($model, 'file[]')
            ->fileInput(['multiple' => true, 'style'=>'display:none;', 'id'=>'attack_file'])
            ->label($model->getAttributeLabel('file'), ['class' => 'control-label col-sm-2']);
        ?>
        <div class="input-container">
            <button class="browse-btn" type="button">
                Buscar archivos
            </button>
            <span class="file-info">Subir un archivo</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button type="submit" onclick="$('#crud-form').submit()" class="btn btn-success crudBtn">Subir</button>
        <button type="button" class="btn btn-default crudBtn neverReadonly" onclick="$('#crudModal').modal('hide');">Cerrar</button>
</div>
<?php
ActiveForm::end();
?>

<script>
    console.log('work');
    const uploadButton = document.querySelector('.browse-btn');
    const fileInfo = document.querySelector('.file-info');
    const realInput = document.getElementById('attack_file');
    uploadButton.addEventListener('click', () => {
    realInput.click();
    });
    realInput.addEventListener('change', () => {
    const name = realInput.value.split(/\\|\//).pop();
    const truncated = name.length > 20
    ? name.substr(name.length - 20)
    : name;

    fileInfo.innerHTML = truncated;
    });
</script>