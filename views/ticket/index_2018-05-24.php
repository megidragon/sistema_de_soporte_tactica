<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Ticket;
use app\models\Producto;
use app\utils\HtmlService;

$jsversion = Yii::$app->params['jsversion'];

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
$this->registerJS("$('#ticket-grid-filters').hide();");  // Search row is hidden by default
$this->registerJS("$(document).on('click', '.crudToggleSearchButton', function() { $('#ticket-grid-filters').toggle();} );"); // Toggle filter when click on magnifier

$this->title = Yii::t('app/ticket', 'Incidents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <p>
        <div class="row">
            <div class="col-sm-1">
        <?php 
        	$form = ActiveForm::begin(['id' => 'print-form', 'action'=>['print'], 'options' => ['target'=>'_blank']]);
            echo '<div style="display: none;">';
                echo $form->field($printParams, 'queryParams');
            echo '</div>';
            if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'com.arielvila.chofer') echo Html::submitButton('<i class="glyphicon glyphicon-print"></i> Imprimir', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px']);
        	ActiveForm::end();
        ?>
            </div>
			<div class="col-sm-1">
				<?php
				$form = ActiveForm::begin(['id' => 'export-form', 'action'=>['excel'], 'options' => ['target'=>'_blank']]);
				echo '<div style="display: none;">';
				echo $form->field($printParams, 'queryParams');
				echo '</div>';
				if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'com.arielvila.chofer') echo Html::submitButton('<i class="glyphicon glyphicon-save"></i> Exportar Excel', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px']);
				ActiveForm::end();
				?>
			</div>
            <div class="col-sm-10">
                
            </div>
        </div>
    </p>

    
	<?php
		Modal::begin([
				'header'=>'<div id="modalHeader"><h4></h4></div>',
				'id'=>'crudModal',
				'size'=>'modal-lg',
                'closeButton' => false,
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
                'options' => ['tabindex' => false], // For Select2 to work on Modals
			]);
		echo "<div id='modalContent'></div>";
		Modal::end();
	?>
	
	
	<!-- ?php Pjax::begin(); ? -->
	<div style="" class="">
    <?php
        $template = '{view}';
        $width = 42;
        $tipo = [
            'TECNICO' => 'Soporte Técnico',
            'ADMINISTRACION' => 'Comercial / Administración',
        ];
        $prioridad = [
            'BAJA' => 'Baja',
            'MEDIA' => 'Media',
            'ALTA' => 'Alta',
        ];
    ?>
    
    <?= GridView::widget([
        'id' => 'ticket-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'responsiveWrap' => false,
        
        'columns' => [
            'id',
            [
                'attribute' => 'fecha_creacion',
                'label' => Yii::t('app/ticket', 'Date'),
                'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->fecha_creacion) . ' ' . substr($model->hora_creacion, 0, 5);
                        },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fechaCreacionRange',
                    'pluginOptions'=>[
                        'locale'=>[
                            'format'=>'DD/MM/YYYY',
                            'separator'=>' - ',
                        ],
                        'opens'=>'right'
                    ]
                ]),
            ],
	        /*['attribute' => 'id_empresa', 'value' => 'idEmpresa.nombre','filter' => \kartik\select2\Select2::widget([
		        'model' => $searchModel,
		        'attribute' => 'id_empresa',
		        'value' => '',
		        'data' => ArrayHelper::map(\app\models\Empresa::find()->andFilterWhere(
						[
							'id_societe' => User::findIdentity(Yii::$app->user->getId())->id_societe,
							'baja' => 0
						])->all(), 'id', 'nombre'),
		        'options' => ['placeholder' => 'Empresa'],
		        'pluginOptions' => [
			        'allowClear' => true,
			        'width' => '150px'
		        ],
	        ])],
	        ['attribute' => 'id_contacto', 'value' => 'idContacto.nombre','filter' => \kartik\select2\Select2::widget([
		        'model' => $searchModel,
		        'attribute' => 'id_contacto',
		        'value' => '',
		        'data' => ArrayHelper::map(\app\models\Contacto::find()->andFilterWhere(['id_societe' => User::findIdentity(Yii::$app->user->getId())->id_societe, 'baja' => 0])->all(), 'id', function($model, $defaultValue) { return $model->nombre . ' ' . $model->apellido; }),
		        'options' => ['placeholder' => 'Contacto'],
		        'pluginOptions' => [
			        'allowClear' => true,
			        'width' => '150px'
		        ],
	        ])],*/
            ['attribute' => 'id_empresa', 'value' => 'idEmpresa.nombre','filter' => \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'id_empresa',
                'value' => '',
                'data' => ArrayHelper::map(\app\models\Empresa::find()->andFilterWhere([
                		'id_societe' => User::findIdentity(Yii::$app->user->getId())->id_societe,
                		'id' 		=> User::findIdentity(Yii::$app->user->getId())->id_empresa,
						'baja' => 0
				])->orderBy([
						'nombre'	=> "ASC"
				])->all(), 'id', 'nombre'),
                'options' => ['placeholder' => 'Empresa'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '150px'
                ],
            ])],
            ['attribute' => 'id_contacto', 'value' => 'idContacto.nombre','filter' => \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'id_contacto',
                'value' => '',
                'data' => ArrayHelper::map(\app\models\Contacto::find()->andFilterWhere([
                		'id_societe' => User::findIdentity(Yii::$app->user->getId())->id_societe,
                		'id_empresa' => User::findIdentity(Yii::$app->user->getId())->id_empresa,
						'baja' => 0
				])->orderBy([
	                'nombre'	=> "ASC",
					'apellido'	=> "ASC"
                ])->all(), 'id', function($model, $defaultValue) { return $model->nombre . ' ' . $model->apellido; }),
                'options' => ['placeholder' => 'Contacto'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '150px'
                ],
            ])],
            [
                'attribute' => 'tipo',
                'value' => function($model) use($tipo) {
                    return $tipo[$model->tipo];
                },
                'filter' => $tipo,
            ],
            [
                'attribute' => 'prioridad',
                'value' => function($model) use($prioridad) {
                    return $prioridad[$model->prioridad];
                },
                'filter' => $prioridad,
            ],
            ['attribute' => 'id_producto', 'value' => 'idProducto.codigo',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_producto',
                    'value' => '',
                    'data' => ArrayHelper::map(
                        Producto::find()->andFilterWhere([
	                        	'id_empresa' => User::findIdentity(Yii::$app->user->getId())->id_empresa,
                        		'baja' => false
						])->orWhere([
								'id_empresa' => null,
	                        	'baja' => false
						])->orderBy([
	                        'codigo'		=> "ASC",
							'nombre_sector'	=> "ASC"
                        ])->all(),
                        'id', function($model, $defaultValue) { return $model->codigo . ' - ' . $model->nombre_sector; }
                    ),
                    'options' => ['placeholder' => Yii::t('app/ticket', 'Id Producto')],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => '150px'
                    ],
                ])
            ],
            [
                'attribute' => 'fecha_vencimiento',
                'label' => Yii::t('app/ticket', 'Due Date'),
                'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->fecha_vencimiento) . ' ' . substr($model->hora_creacion, 0, 5);
                        },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fechaVencimientoRange',
                    'pluginOptions'=>[
                        'locale'=>[
                            'format'=>'DD/MM/YYYY',
                            'separator'=>' - ',
                        ],
                        'opens'=>'right'
                    ]
                ]),
            ],
            [
				'class' => 'kartik\grid\ActionColumn',
				'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray crudToggleSearchButton']),
				'template' => $template,
				'options' => ['width' => $width],
				'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', ['id' => 'view' . $model->id, 'onclick'=>'location.href="' . Url::to(['view', 'id' => $model->id]) . '"', 'class'=>'btn btn-xs btn-gray']);
                    },
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['id' => 'update' . $model->id, 'onclick'=>'location.href="' . Url::to(['update', 'id' => $model->id]) . '"', 'class'=>'btn btn-xs btn-gray']);
                    },
					'delete' => function ($url, $model) {
						return Html::button('<span class="glyphicon glyphicon-trash"></span>', ['id' => 'delete' . $model->id, 'onclick'=>'location.href="' . Url::to(['delete', 'id' => $model->id]) . '"', 'class'=>'btn btn-xs btn-gray']);
					},
				],  
			],
        ],
    ]); ?>
    <?php 
        HtmlService::clickGridRow($this, 'ticket-grid', (strpos($template, '{update}') === false) ? 'view' : 'update', true, -1);
        HtmlService::setWidthLastCol($this, 'ticket-grid', $width);
    ?>
	</div>
	<!-- ?php Pjax::end(); ? -->
</div>
<?php
$js = <<<JS
$('body').on('beforeSubmit', '#print-form', function () {
    if (@total@ > @maxRecordsPrint@) {
        return confirm('Se imprimirán @maxRecordsPrint@ registros como máximo. Efectúe un filtro para limitar la cantidad de registros a imprimir. Desea imprimir los primeros @maxRecordsPrint@ registros?');
    }  
    return true;
});
$('#print-form').on('keyup keypress', function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) { 
        e.preventDefault();
        return false;
    }
});
JS;
$js = str_replace('@total@', $dataProvider->getTotalCount(), $js);
$js = str_replace('@maxRecordsPrint@', Yii::$app->params['maxRecordsPrint'], $js);
$this->registerJs($js);
?>


