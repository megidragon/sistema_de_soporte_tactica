<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = 'Ticket';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            ['attribute' => 'id_empresa', 'value' => !is_null($model->idEmpresa) ? $model->idEmpresa->nombre : ''],
            ['attribute' => 'id_contacto', 'value' => !is_null($model->idContacto) ? $model->idContacto->nombre : ''],
            'tipo',
            ['attribute' => 'id_tipo', 'value' => !is_null($model->idTipo) ? $model->idTipo->descripcion : ''],
            ['attribute' => 'id_subtipo', 'value' => !is_null($model->idSubtipo) ? $model->idSubtipo->descripcion : ''],
            ['attribute' => 'id_elemento', 'value' => !is_null($model->idElemento) ? $model->idElemento->descripcion : ''],
            ['attribute' => 'id_producto', 'value' => !is_null($model->idProducto) ? $model->idProducto->descripcion : ''],
            'descripcion',

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
