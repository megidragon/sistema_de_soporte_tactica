<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = 'Ticket';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/ticket', 'Incidents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'view',
        'readonly' => true,
        'candelete' => false, // used for delete button on update action
        
    ]) ?>

</div>
