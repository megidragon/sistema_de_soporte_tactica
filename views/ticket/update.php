<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = 'Actualizar Ticket';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-update">

    <?= $this->render('_form', [
        'model' => $model,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'update',
        'readonly' => false,
        'candelete' => $candelete,
        
    ]) ?>

</div>
