<?php // AVTPL

$this->title = 'Historial de cambios de Ticket ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Ticket ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [
    'query' => $model->getAuditTrails(),
    'columns' => ['user_id', 'action', 'field', 'old_value', 'new_value', 'diff', 'created'],
]);
?>