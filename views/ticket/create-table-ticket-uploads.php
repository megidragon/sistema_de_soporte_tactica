<?php

//use Yii;

echo "Generando la tabla ticket_uploads";
echo "<br>";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
        CREATE TABLE `ticket_uploads` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
              `upload` varchar(50) DEFAULT NULL,
              `id_ticket` int(11),
              PRIMARY KEY (`id`),
              FOREIGN KEY (`id_ticket`) REFERENCES `ticket` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
        ", [':start_date' => '1970-01-01']);

try{
    $result = $command->queryAll();
}catch (\Yii\db\Exception $exception){
//    echo $exception->getMessage();
}


echo "<br>";
echo "Fin de generación tabla ticket_uploads";