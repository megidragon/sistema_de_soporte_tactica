<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = 'Dar de baja Ticket';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-delete">

    <?= $this->render('_form', [
        'model' => $model,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'delete',
        'readonly' => true,
        'candelete' => false, // used for delete button on update action
        
    ]) ?>

</div>
