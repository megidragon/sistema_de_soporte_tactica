﻿<?php

//use Yii;

echo "Agregando campos tactica tabla ticket";
echo "<br>";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("
        ALTER TABLE ticket
  ADD (
  `tactica_user_modificacion` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `tactica_fecha_modificacion` date DEFAULT NULL,
  `tactica_hora_modificacion` TIME DEFAULT NULL,
  `tactica_user_cierre` TEXT CHARACTER SET utf8 DEFAULT NULL,
  `tactica_fecha_cierre` date DEFAULT NULL,
  `tactica_hora_cierre` TIME DEFAULT NULL,
  `tactica_solucion_descr` TEXT CHARACTER SET utf8 DEFAULT NULL
  );

        ", [':start_date' => '1970-01-01']);

try{
    $result = $command->queryAll();
}catch (\Yii\db\Exception $exception){
//    echo $exception->getMessage();
}


echo "<br>";
echo "Fin de actualización tactica tabla ticket";