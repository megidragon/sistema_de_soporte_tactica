<?php // AVTPL
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\URL;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\Growl;
use app\models\User;
use app\models\Societe;
use app\utils\AlertsService;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$user = User::findIdentity(Yii::$app->user->getId());
$societe = Societe::findOne($user->id_societe);
$sidebar_collapse = $user->sidebar_collapse ? 'sidebar-collapse' : '';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= $societe->name ?> - <?= Html::encode($this->title) ?></title>
	<link rel="icon" href="<?php echo Yii::$app->request->baseUrl . '/socs/' . $societe->id . '/favicon.ico'?>">
    <?php $this->head() ?>
</head>
<body class="skin-red wysihtml5-supported <?= $sidebar_collapse ?>">
    <?php $this->beginBody() ?>
    <div id="baseUrl" style="display: none" data-url="<?= Yii::$app->request->baseUrl ?>"></div>
	<div class="wrapper">
		<?php $alertResult = AlertsService::getUserAlerts(); ?>
		<header class="main-header">
			<a href="<?= Yii::$app->homeUrl ?>" class="logo">
				<?= Html::img('@web/socs/' . $societe->id . '/logo.png', ['alt' => $societe->name]) ?>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only"><?= Yii::t('app', 'Toggle navigation') ?></span>
				</a>
		        <div class="navbar-custom-menu">
					<?= $this->render('//layouts/top-menu.php', ['alertResult' => $alertResult]) ?>
				</div>
			</nav>
		</header>
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<?= $this->render('//layouts/sidebar-menu') ?>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<?= $this->title ?>
					<?php if (isset($this->params['subtitle'])) : ?>
						<small><?= $this->params['subtitle'] ?></small>
					<?php endif; ?>
				</h1>
				<?= Breadcrumbs::widget(
					[
						'homeLink' => [
							'label' => '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Home'),
							'url' => ['/']
						],
						'encodeLabels' => false,
						'tag' => 'ol',
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
					]
				) ?>
			</section>

			<!-- Main content -->
			<section class="content" style="position: relative; overflow:hidden;">
				<!-- ?= Alert::widget() ? -->
				<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
					<?php
					echo Growl::widget([
						'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
						'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
						'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
						'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
						'showSeparator' => true,
						'delay' => 1, //This delay is how long before the message shows
						'pluginOptions' => [
							'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
							'placement' => [
								'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
								'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
							]
						]
					]);
					?>
				<?php endforeach; ?>
				<?= $content ?>
				
				<!-- AVTPL EVO INI MainLayoutAlertsAdd -->
				<div id="alert-ticket" style="display: none" data-title="<?= Yii::t('app/alerts', 'Support Answers') ?>" data-one-new="<?= Yii::t('app/alerts', 'ONE_NEW_F') ?>" data-many-new="<?= Yii::t('app/alerts', 'MANY_NEW_F') ?>" data-icon="fa-life-ring" data-url-one="<?= Url::to(['tickets/update', 'id' => '_ID_']) ?>" data-url-many="<?= Url::to(['tickets/index']) ?>"></div>
				<div id="alert-schedule" style="display: none" data-title="<?= Yii::t('app/alerts', 'Schedules') ?>" data-one-new="<?= Yii::t('app/alerts', 'ONE_NEW_F') ?>" data-many-new="<?= Yii::t('app/alerts', 'MANY_NEW_F') ?>" data-icon="fa-calendar" data-url-one="<?= Url::to(['viajes/view', 'id' => '_ID_']) ?>" data-url-many="<?= Url::to(['viajes/schedule']) ?>"></div>
				<!-- AVTPL EVO END MainLayoutAlertsAdd -->
				<a id="alert-link" href="#">
					<div id="alert-box" class="info-box bg-green" style="display: none; position: absolute; top: 11px; right: -30px; z-index: 99; max-width: 350px">
						<span class="info-box-icon"><i id="alert-icon" class="fa"></i></span>
						<div class="info-box-content">
							<span id="alert-text" class="info-box-text">Notifications</span>
							<span id="alert-number" class="info-box-number">1 New</span>
							<span id="alert-description" class="progress-description"><?= Yii::t('app/alerts', 'Press here to access') ?></span>
						</div>
					</div>
				</a>

				<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/js/jquery-ui.js?v=111', ['depends'=>'yii\web\YiiAsset']);?>
			</section><!-- /.content -->
		</aside><!-- /.right-side -->
	</div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
