<?php // AVTPL

// AVTPL EVO INI ViewLandingContent
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\Growl;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Latin America Solutions S.A.</title>
    <link rel="icon" href="<?= Yii::$app->request->baseUrl . '/socs/' . 1 . '/favicon.ico'?>">
    <?php $this->head() ?>
    <script>
        <?php if (YII_ENV_PROD && substr(Yii::$app->request->hostInfo, 0, 16) != 'http://localhost' && substr(Yii::$app->request->hostInfo, 0, 16) != 'http://127.0.0.1') { ?>
            // Replace with windows analytics for production
            function ga(send, event, category, action, value) {}
        <?php } else { ?>
            function ga(send, event, category, action, value) {}
        <?php } ?>
    </script>
</head>
<body class="skin-red wysihtml5-supported sidebar-collapse">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <a href="<?= Yii::$app->homeUrl ?>" class="logo" style="background-color: #dd4b39">
                <?= Html::img('@web/socs/' . 1 . '/logo.png', ['alt' => 'Prueba Yii2 Base']) ?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Without sidebar toggle button-->
                <div class="navbar-custom-menu">
                <?php
                    if (Yii::$app->params['homeAjaxDefault'] == 0) {
                        $menuItems = [
                            ['label' => 'Login', 'url' => ['/home/login']],
                            ['label' => 'Contacto', 'url' => ['/home/contact']],
                        ];
                    } else {
                        $menuItems = [
                            ['label' => 'Login', 'options' => ['class' => 'loginButton', 'value' => Url::to(['login'])]],
                            ['label' => 'Contacto', 'options' => ['class' => 'contactButton', 'value' => Url::to(['contact'])]],
                        ];
                    }
                    echo Nav::widget([
                        'options' => ['class' => 'nav navbar-nav'],
                        'items' => $menuItems,
                        'dropDownCaret' => '',
                        'encodeLabels' => false,
                    ]);
                ?>
                <?php $this->registerJsFile(Yii::$app->request->baseUrl . '/js/landing/landing.js?v=' . Yii::$app->params['jsversion'], ['depends'=>'yii\web\YiiAsset']); ?>
                </div>
            </nav>
        </header>
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <!-- ?= $this->render('//layouts/sidebar-menu') ? -->
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?= $this->title ?>
                    <?php if (isset($this->params['subtitle'])) : ?>
                        <small><?= $this->params['subtitle'] ?></small>
                    <?php endif; ?>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- ?= Alert::widget() ? -->
                <?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
                    <?php
                    echo Growl::widget([
                        'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                        'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
                        'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                        'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
                        'showSeparator' => true,
                        'delay' => 1, //This delay is how long before the message shows
                        'pluginOptions' => [
                            'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                            'placement' => [
                                'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                                'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                            ]
                        ]
                    ]);
                    ?>
                <?php endforeach; ?>
                <?= $content ?>
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div>
    <div style="display: none">
        <div id="landingLinks" data-images="<?= Url::to('@web/images') ?>"></div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
// AVTPL EVO END ViewLandingContent
