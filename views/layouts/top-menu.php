<?php // AVTPL
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;


/*NavBar::begin([
    'brandLabel' => 'My Company',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);*/

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;

$username = (Yii::$app->user->identity->nombre != '') ? Yii::$app->user->identity->nombre : Yii::$app->user->identity->username;

$userItems = [
    [
        'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Config'),
        'url' => ['/user/user-config'],
    ],
    [
        'label' => '<i class="fa fa-sign-out"></i> ' . Yii::t('app', 'Logout'),
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post', 'onclick' => 'this.disabled=true;']
    ],
];

// AVTPL EVO INI ViewTopmenuAfterUserDefinition
// AVTPL EVO END ViewTopmenuAfterUserDefinition

$menuItemsRight = [
    // AVTPL EVO INI ViewTopmenuAdditionalItems
    // AVTPL EVO END ViewTopmenuAdditionalItems
    
    [
        'options' => ['class' => 'karbooking-menu'],
        'label' => '<i class="fa fa-user"></i> ' . $username,
        'url' => ['#'],
        'active' => false,
        'items' => $userItems,
    ],
];

echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav'],
    'items' => $menuItemsRight,
    'dropDownCaret' => '',
    'encodeLabels' => false,
]);

//NavBar::end();

