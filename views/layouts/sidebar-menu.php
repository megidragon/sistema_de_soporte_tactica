<?php // AVTPL
use app\widgets\Menu;
use yii\helpers\Url;

echo Menu::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => [
            // AVTPL EVO INI ViewSidebarMenuContent
            [
                'label' => Yii::t('app', 'New Incident'),
                'url' => ['/ticket/create'],
                'icon' => 'fa fa-bug',
                'visible' => Yii::$app->user->can('incidente-create'),
            ],
            [
                'label' => Yii::t('app', 'Incidents'),
                'url' => ['#'],
                'icon' => 'fa fa-bug',
                'visible' => Yii::$app->user->can('incidente-view'),
                'options' => [
                    'class' => 'treeview'
                ],
                'items' => [
                    [
                        'label' => Yii::t('app', 'View Incidents'),
                        'url' => '/ticket/index?TicketSearch%5BfechaCreacionRange%5D=&TicketSearch%5Bid_empresa%5D=&TicketSearch%5Bid_contacto%5D=&TicketSearch%5Btipo%5D=&TicketSearch%5Bprioridad%5D=&TicketSearch%5Bid_producto%5D=&TicketSearch%5BfechaVencimientoRange%5D=&TicketSearch%5BfechaTacticaModificacionRange%5D=&TicketSearch%5BfechaTacticaCierreRange%5D=&TicketSearch%5Bbaja%5D=',
                        'icon' => 'fa fa-bug',
                        'visible' => Yii::$app->user->can('incidente-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'View Open Incidents'),
                        'url' => '/ticket/index?TicketSearch%5BfechaCreacionRange%5D=&TicketSearch%5Bid_empresa%5D=&TicketSearch%5Bid_contacto%5D=&TicketSearch%5Btipo%5D=&TicketSearch%5Bprioridad%5D=&TicketSearch%5Bid_producto%5D=&TicketSearch%5BfechaVencimientoRange%5D=&TicketSearch%5BfechaTacticaModificacionRange%5D=&TicketSearch%5BfechaTacticaCierreRange%5D=&TicketSearch%5Bbaja%5D=0',
                        'icon' => 'fa fa-bug',
                        'visible' => Yii::$app->user->can('incidente-view'),
                    ],
                ]
            ],
            [
                'label' => Yii::t('app', 'Imported Data'),
                'url' => ['#'],
                'icon' => 'fa fa-database',
                'options' => [
                    'class' => 'treeview',
                ],
                'visible' => Yii::$app->user->can('incidente-config-view'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Types'),
                        'url' => ['/tipo/index'],
                        'icon' => 'fa fa-th-large',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'Subtypes'),
                        'url' => ['/subtipo/index'],
                        'icon' => 'fa fa-th-list',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'Elements'),
                        'url' => ['/elemento/index'],
                        'icon' => 'fa fa-th',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'Companies'),
                        'url' => ['/empresa/index'],
                        'icon' => 'fa fa-building',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'Contacts'),
                        'url' => ['/contacto/index'],
                        'icon' => 'fa fa-group',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                    [
                        'label' => Yii::t('app', 'Products'),
                        'url' => ['/producto/index'],
                        'icon' => 'fa fa-cubes',
                        'visible' => Yii::$app->user->can('incidente-config-view'),
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'System'),
                'url' => ['#'],
                'icon' => 'fa fa-cog',
                'options' => [
                    'class' => 'treeview',
                ],
                'visible' => (Yii::$app->user->can('users-view') || Yii::$app->user->can('societe-view') || Yii::$app->user->can('permissions-view') || Yii::$app->user->can('email-view') || Yii::$app->user->can('add-param-view')),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Users'),
                        'url' => ['/user/index'],
                        'icon' => 'fa fa-user',
                        'visible' => (Yii::$app->user->can('users-view')),
                    ],
                    [
                        'label' => Yii::t('app', 'Product Priorities'),
                        'url' => ['/prioridad/index'],
                        'icon' => 'fa fa-exclamation-circle',
                        'visible' => (Yii::$app->user->can('incidente-config-view')),
                    ],
                    [
                        'label' => Yii::t('app', 'Company Parameters'),
                        'url' => ['/societe/view'],
                        'icon' => 'fa fa-home',
                        'visible' => Yii::$app->user->can('societe-view') && !Yii::$app->user->can('societe-upd'),
                    ],
                    [
                        'label' => Yii::t('app', 'Company Parameters'),
                        'url' => ['/societe/update'],
                        'icon' => 'fa fa-home',
                        'visible' => Yii::$app->user->can('societe-upd'),
                    ],
                    [
                        'label' => Yii::t('app', 'Permissions'),
                        'url' => ['/permissions/index'],
                        'icon' => 'fa fa-shield',
                        'visible' => (Yii::$app->user->can('permissions-view')),
                    ],
                    [
                        'label' => Yii::t('app', 'Sent Emails'),
                        'url' => ['/mail/index'],
                        'icon' => 'fa fa-envelope',
                        'visible' => (Yii::$app->user->can('email-view')),
                    ],
                    [
                        'label' => Yii::t('app', 'Additional Parameters'),
                        'url' => ['/societe-params/index'],
                        'icon' => 'fa fa-sliders',
                        'visible' => (Yii::$app->user->can('add-param-view')),
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'Admin Tools'),
                'url' => ['#'],
                'icon' => 'fa fa-wrench',
                'options' => [
                    'class' => 'treeview',
                ],
                'visible' => (Yii::$app->user->can('setup')),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Audit'),
                        'url' => ['/audit'],
                        'icon' => 'fa fa-check',
                        'visible' => (Yii::$app->user->can('audit')),
                    ],
                ],
            ],
            // AVTPL EVO END ViewSidebarMenuContent
        ]
    ]
);