<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = 'Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-view">

    
    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            'nombre',
            'tacticaid',

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
