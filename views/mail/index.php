<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use kartik\daterange\DateRangePicker;
use app\utils\HtmlService;

$jsversion = Yii::$app->params['jsversion'];

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
$this->registerJS("$('#mail-grid-filters').hide();");  // Search row is hidden by default
$this->registerJS("$(document).on('click', '.crudToggleSearchButton', function() { $('#mail-grid-filters').toggle();} );"); // Toggle filter when click on magnifier

$this->title = Yii::t('app', 'Sent Emails');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-index">

    <!-- ?php Pjax::begin(); ? -->
    <div>
    <?php
        $template = '{view}';
        $width = 42;
        $successful = [
            '0'=> Yii::t('app', 'No'),
            '1'=> Yii::t('app', 'Yes'),
        ];
    ?>
    
    <?= GridView::widget([
        'id' => 'mail-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'responsiveWrap' => false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            
            'to',
            [
                'attribute' => 'bcc',
                'value' => function($model) {
                    return ($model->bcc) ? $model->bcc : '';
                },
            ],
            'subject',
            [
                'attribute' => 'created',
                // TODO: See why this code is showing 3hs before DB time (1 hr after ARG time)
                'format' => ['date', 'dd/MM/Y HH:mm:ss'],
                // TODO: See why DB is storing mails 4hs later than ARG time. Code below shows that hour
                // 'value' => function ($model) {
                //         return Yii::$app->formatter->asDate(substr($model->created, 0, 10)) . substr($model->created, 10);
                //     },
                'options' => ['width' => '150px'],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createdRange',
                    'pluginOptions'=>[
                        'locale'=>[
                            'format'=>'DD/MM/YYYY',
                            'separator'=>' - ',
                        ],
                        'opens'=>'left'
                    ]
                ]),
            ],
            [
                'attribute' => 'successful',
                'options' => [
                    'width' => '100px',
                ],
                'value' => function ($model) use ($successful) {
                    return $successful[$model->successful];
                },
                'filter' => $successful
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray crudToggleSearchButton']),
                'template' => $template,
                'options' => ['width' => $width],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', 
                            ['id' => 'view' . $model->id, 'onclick'=>'location.href="' . Url::to(['view', 'id' => $model->id]) . '"', 'class'=>'btn btn-xs btn-gray']);
                    },
                ],  
            ],
        ],
    ]); ?>
    <?php 
        HtmlService::clickGridRow($this, 'mail-grid', 'view');
    ?>
    </div>
</div>


