<?php // AVTPL

/** @var View $this */
/** @var AuditMail $model */

use bedezign\yii2\audit\components\Helper;
use app\models\AuditMail;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = Yii::t('app/mail', 'Email #{id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sent Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$successful = [
    '0'=> Yii::t('app', 'No'),
    '1'=> Yii::t('app', 'Yes'),
];

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'to',
        'from',
        'reply',
        [
            'attribute' => 'cc',
            'value' => ($model->cc) ? $model->cc : '',
        ],
        [
            'attribute' => 'bcc',
            'value' => ($model->bcc) ? $model->bcc : '',
        ],
        'subject',
        [
            'label' => Yii::t('app/mail', 'Download'),
            'value' => Html::a(Yii::t('app/mail', 'Download eml file'), ['mail/download', 'id' => $model->id]),
            'format' => 'raw',
        ],
        [
            'attribute' => 'created',
            'format' => ['date', 'dd/MM/Y HH:mm:ss'],
        ],
        [
            'attribute' => 'successful',
            'value' => $successful[$model->successful],
        ],
    ],
]);

echo Html::tag('h3', Yii::t('app/mail', 'Text Body'));
echo '<div class="well">';
echo Yii::$app->formatter->asNtext($model->text);
echo '</div>';

echo Html::tag('h3', Yii::t('app/mail', 'HTML Body'));
echo '<div class="well">';
echo Yii::$app->formatter->asHtml($model->html);
echo '</div>';

echo '<div class="row"><div class="col-sm-12">';
echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
echo '</div></div>';
