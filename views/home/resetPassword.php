<?php // AVTPL
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\ResetPasswordForm */

?>
<div class="site-reset-password">
    <p>Hola <strong><?= $user->username ?></strong>, ingrese una nueva contraseña:</p>

	<?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
		<?= $form->field($model, 'password')->passwordInput() ?>
        <div class="row">
            <div class="col-md-12">
    			<?= Html::submitButton('Grabar', ['class' => 'btn btn-primary btnFloatR']) ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-reset-passwd-form']) ?>
            </div>
            <?php $this->registerJs("$('form#reset-password-form').on('beforeSubmit', function(e) { $('.spin-reset-passwd-form').show(); $('.btn').attr('disabled','disabled'); });"); ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>

