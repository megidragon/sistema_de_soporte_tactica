<?php // AVTPL
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
?>
<div class="site-login">
    <p>Ingrese los datos siguientes para iniciar sesión:</p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
	    <div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-12' ?>">
                <?= $form->field($model, 'username')->label('Usuario') ?>
                <?= $form->field($model, 'password')->label('Contrase&ntilde;a')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->label('Recordarme')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                	Si olvidó la contraseña, puede 
                    <?php 
                    	if ($ajax == 0) {
                        	echo Html::a('reestablecerla', ['request-password-reset']);
                    	} else {
                            echo Html::a('reestablecerla', null, ['id'=>'askResetButton', 'onclick'=>'showAskResetModal()', 'value'=>Url::to(['request-password-reset'])]);
                    	}
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-12' ?>">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btnFloatR', 'name' => 'login-button']) ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-login-form']) ?>
            </div>
            <?php $this->registerJs("$('form#login-form').on('beforeSubmit', function(e) { $('.spin-login-form').show(); $('.btn').attr('disabled','disabled'); });"); ?>
            <?php $this->registerJs("setTimeout(function() { $('#login-form input').not('[type=hidden]').first().focus(); }, 500);"); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>

