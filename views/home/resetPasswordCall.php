<?php // AVTPL

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\widgets\Growl;

?>
	<?php
		Modal::begin([
				'header'=>'<h4>Reestablecer Contrase&ntilde;a</h4>',
				'id'=>'resetModal',
				'size'=>'modal-sm',
			]);
		echo "<div id='resetModalContent'></div>";
		Modal::end();
	?>

	<div id="resetDiv" data-url="<?=Url::to(['reset-password', 'token'=>Yii::$app->getRequest()->getQueryParam('token')]);?>"></div>

	<?php $this->registerJS("showResetModal();"); ?>
