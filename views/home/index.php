<?php // AVTPL

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\widgets\Growl;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChoferesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

	<?php
		Modal::begin([
				'header'=>'<h4>Ingreso</h4>',
				'id'=>'loginModal',
				'size'=>'modal-sm',
			]);
		echo "<div id='loginModalContent'></div>";
		Modal::end();
		
		Modal::begin([
				'header'=>'<h4>Contacto</h4>',
				'id'=>'contactModal',
				'size'=>'modal-lg',
			]);
		echo "<div id='contactModalContent'></div>";
		Modal::end();

		Modal::begin([
				'header'=>'<h4>Demostración</h4>',
				'id'=>'demoModal',
				'size'=>'modal-lg',
			]);
		echo "<div id='demoModalContent'></div>";
		Modal::end();

		Modal::begin([
				'header'=>'<h4>Reestablecer Contrase&ntilde;a</h4>',
				'id'=>'askResetModal',
				'size'=>'modal-sm',
			]);
		echo "<div id='askResetModalContent'></div>";
		Modal::end();

	?>
