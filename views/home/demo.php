<?php // AVTPL
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Demo';
?>
<div class="site-demo">
	<?php $form = ActiveForm::begin([
		'id' => 'demo-form'
	]); ?>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-8' : 'col-lg-12' ?>">
				Ingresa tus datos y nos contactaremos a la brevedad<br><br>
			</div>
		</div>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-6' ?>">
				<?= $form->field($model, 'name') ?>
				<?= $form->field($model, 'phone') ?>
				<?= $form->field($model, 'email') ?>
			</div>
	        <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-6' ?>">
				<?= $form->field($model, 'agency') ?>
				<?= $form->field($model, 'address') ?>
				<?= $form->field($model, 'webpage') ?>
			</div>
		</div>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-8' : 'col-lg-12' ?>">
				<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
				]) ?>
			</div>
		</div>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-8' : 'col-lg-12' ?>">
				<?= Html::submitButton('Enviar', ['class' => 'btn btn-primary btnFloatR', 'name' => 'demo-button']) ?>
				<?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-demo-form']) ?>
			</div>
			<?php $this->registerJs("$('form#demo-form').on('beforeSubmit', function(e) { $('.spin-demo-form').show(); $('.btn').attr('disabled','disabled'); });"); ?>
	        <?php $this->registerJs("setTimeout(function() { $('#demo-form input').not('[type=hidden]').first().focus(); }, 500);"); ?>
		</div>
	<?php ActiveForm::end(); ?>
	

</div>
