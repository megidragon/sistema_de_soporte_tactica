<?php // AVTPL
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';
?>
<div class="site-contact">
	<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-6' : 'col-lg-12' ?>">
				<?= $form->field($model, 'name') ?>
				<?= $form->field($model, 'email') ?>
				<?= $form->field($model, 'body')->textArea(['rows' => 5]) ?>
				<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
				]) ?>
			</div>
		</div>
		<div class="row">
	        <div class="<?= ($ajax == 0) ? 'col-lg-6' : 'col-lg-12' ?>">
				<?= Html::submitButton('Enviar', ['class' => 'btn btn-primary btnFloatR', 'name' => 'contact-button']) ?>
				<?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-contact-form']) ?>
			</div>
			<?php $this->registerJs("$('form#contact-form').on('beforeSubmit', function(e) { $('.spin-contact-form').show(); $('.btn').attr('disabled','disabled'); });"); ?>
	        <?php $this->registerJs("setTimeout(function() { $('#contact-form input').not('[type=hidden]').first().focus(); }, 500);"); ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
