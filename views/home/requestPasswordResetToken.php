<?php // AVTPL
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

?>
<div class="site-request-password-reset">
    <p>Por favor ingrese su usuario y email. Se le enviará un link para reinicializar su contraseña.</p>

    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
        <div class="row">
            <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-12' ?>">
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
            </div>
        </div>
        <div class="row">
            <div class="<?= ($ajax == 0) ? 'col-lg-4' : 'col-lg-12' ?>">
                <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary btnFloatR']) ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-reset-pwd-form']) ?>
            </div>
            <?php $this->registerJs("$('form#request-password-reset-form').on('beforeSubmit', function(e) { $('.spin-reset-pwd-form').show(); $('.btn').attr('disabled','disabled'); });"); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
