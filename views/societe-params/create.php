<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SocieteParams */

$this->title = 'Crear Parámetro';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="societe-params-create">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'create',
        'readonly' => false,
        
    ]) ?>

</div>
