<?php // AVTPL

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;

$title = 'Listado de Parámetros' . (($searchModel->baja == "1") ? ' dados de baja' : '');
?>

<header>
    <table style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td width="50%"><?= Html::img('@web/socs/' . $societe->id . '/logo_print.jpg', ['height' => 40]) ?></td>
                <td style="text-align: right;" width="50%"><?= $title ?></td>
            </tr>
        </tbody>
    </table>
</header>
<footer>
    <table style="border-top: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td style="text-align: center;">Página {PAGENO}</td>
            </tr>
        </tbody>
    </table>
</footer>
<margintop>30</margintop>
<marginbottom>15</marginbottom>

    <?php
    $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    $nameField = Yii::$app->user->can('setup') ? 'name' : 'desc_es';
?>
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
// AVTPL views.GridColumn.fld.societe_params.name
            ['attribute' => $nameField, 'value' => function ($model) {
                if (false && Yii::$app->user->can('setup')) {
                    return $model->name . (!$model->only_admin ? ' *' : '');
                } else {
                    return $model->desc_es;
                }
            }],
['attribute' => 'value', 'value' => function ($model) use ($yes_no) {
                if ($model->type == 'BOOLEAN') {
                    return $yes_no[$model->value];
                } else {
                    return $model->value;
                }
            }],


        ],
    ]); ?>

