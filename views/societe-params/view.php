<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\SocieteParams */

$this->title = 'Parámetro';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="societe-params-view">

    <?php
    $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
?>
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            ['attribute' => 'value', 'value' => ($model->type == 'BOOLEAN') ? $yes_no[$model->value] : $model->value],

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
