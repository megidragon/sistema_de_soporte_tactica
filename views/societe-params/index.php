<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\utils\HtmlService;

$jsversion = Yii::$app->params['jsversion'];

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
$this->registerJS("$('#societe_params-grid-filters').hide();");  // Search row is hidden by default
$this->registerJS("$(document).on('click', '.crudToggleSearchButton', function() { $('#societe_params-grid-filters').toggle();} );"); // Toggle filter when click on magnifier

$this->title = 'Parámetros' . (($baja == "1") ? ' dados de baja' : '');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="societe-params-index">

    <p>
        <div class="row">
            <div class="col-sm-4">
        <?php 
        	$form = ActiveForm::begin(['id' => 'print-form', 'action'=>['print'], 'options' => ['target'=>'_blank']]);
            echo '<div style="display: none;">';
                echo $form->field($printParams, 'queryParams');
            echo '</div>';
			if ($baja == "0" && Yii::$app->user->can('none')) {
				echo Html::button('<i class="glyphicon glyphicon-plus-sign"></i> Crear Parámetro', ['value'=>Url::to(['create', 'ajax'=>1]), 'class'=>'btn btn-success crudModalButton', 'modal-title'=>'<h4>Crear Parámetro</h4>']);
			}
            if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'com.arielvila.chofer') echo Html::submitButton('<i class="glyphicon glyphicon-print"></i> Imprimir', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px']);
        	ActiveForm::end();
        ?>
            </div>
            <div class="col-sm-8">
                
            </div>
        </div>
    </p>

    
	<?php
		Modal::begin([
				'header'=>'<div id="modalHeader"><h4></h4></div>',
				'id'=>'crudModal',
				'size'=>'modal-sm',
                'closeButton' => false,
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
                'options' => ['tabindex' => false], // For Select2 to work on Modals
			]);
		echo "<div id='modalContent'></div>";
		Modal::end();
	?>
	
	
	<!-- ?php Pjax::begin(); ? -->
	<div style="" class="">
    <?php
        $template = '{view}';
        $width = 42;
        if ($baja == 0 && Yii::$app->user->can('add-param-upd')
            || $baja == 1 && Yii::$app->user->can('none')) {
            $template .= ' {update}';
            $width += 28;
        }
        if ($baja == 0 && Yii::$app->user->can('none')) {
            $template .= ' {delete}';
            $width += 28;
        }
    ?>
    <?php
    $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    $nameField = Yii::$app->user->can('setup') ? 'name' : 'desc_es';
?>
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= GridView::widget([
        'id' => 'societe_params-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'responsiveWrap' => false,
        
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            
            // AVTPL views.GridColumn.fld.societe_params.name
            ['attribute' => $nameField, 'value' => function ($model) {
                if (false && Yii::$app->user->can('setup')) {
                    return $model->name . (!$model->only_admin ? ' *' : '');
                } else {
                    return $model->desc_es;
                }
            }],
            ['attribute' => 'value', 'value' => function ($model) use ($yes_no) {
                if ($model->type == 'BOOLEAN') {
                    return $yes_no[$model->value];
                } else {
                    return $model->value;
                }
            }],
            
            [
				'class' => 'kartik\grid\ActionColumn',
				'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray crudToggleSearchButton']),
				'template' => $template,
				'options' => ['width' => $width],
				'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', ['id' => 'view' . $model->id, 'value'=>Url::to(['view', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Visualizar Parámetro</h4>']);
                    },
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['id' => 'update' . $model->id, 'value'=>Url::to(['update', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Actualizar Parámetro</h4>']);
                    },
					'delete' => function ($url, $model) {
						return Html::button('<span class="glyphicon glyphicon-trash"></span>', ['id' => 'delete' . $model->id, 'value'=>Url::to(['delete', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Confirmar Baja de Parámetro</h4>']);
					},
				],  
			],
        ],
    ]); ?>
    <?php 
        HtmlService::clickGridRow($this, 'societe_params-grid', (strpos($template, '{update}') === false) ? 'view' : 'update', true, -1);
        HtmlService::setWidthLastCol($this, 'societe_params-grid', $width);
    ?>
	</div>
	<!-- ?php Pjax::end(); ? -->
    <?php
        if ($baja == "0" && Yii::$app->user->can('none')) {
            echo Html::a('<i class="glyphicon glyphicon-trash"></i> Ver Dados de Baja', ['index', 'baja' => 1], ['class' => 'btn btn-danger', 'style' => 'margin-top: 12px;']);
        }
        
    ?>
</div>
<?php
$js = <<<JS
$('body').on('beforeSubmit', '#print-form', function () {
    if (@total@ > @maxRecordsPrint@) {
        return confirm('Se imprimirán @maxRecordsPrint@ registros como máximo. Efectúe un filtro para limitar la cantidad de registros a imprimir. Desea imprimir los primeros @maxRecordsPrint@ registros?');
    }  
    return true;
});
$('#print-form').on('keyup keypress', function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) { 
        e.preventDefault();
        return false;
    }
});
JS;
$js = str_replace('@total@', $dataProvider->getTotalCount(), $js);
$js = str_replace('@maxRecordsPrint@', Yii::$app->params['maxRecordsPrint'], $js);
$this->registerJs($js);
?>


