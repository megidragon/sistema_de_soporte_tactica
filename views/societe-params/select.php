<?php // AVTPL

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SocieteParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parámetros';
?>
<div class="societe-params-select">

	<div style="" class="table-responsive">
	<?php 
		Pjax::begin(['id' => 'selectsociete_params']); 
		$this->registerJS("$('#w0-filters').hide();");  // Search row is hidden by default
		$this->registerJS("$('.selectToggleSearchButton').click(function() { $('#w0-filters').toggle(); });");
	?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        
        'layout' => '{items}{pager}', // Clientes has a join with telefonos, so the number of rows may be wrong
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            

// AVTPL views.GridColumn.fld.societe_params.name
            ['attribute' => $nameField, 'value' => function ($model) {
                if (false && Yii::$app->user->can('setup')) {
                    return $model->name . (!$model->only_admin ? ' *' : '');
                } else {
                    return $model->desc_es;
                }
            }],
['attribute' => 'value', 'value' => function ($model) use ($yes_no) {
                if ($model->type == 'BOOLEAN') {
                    return $yes_no[$model->value];
                } else {
                    return $model->value;
                }
            }],

            [
				'class' => 'yii\grid\ActionColumn',
				'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray selectToggleSearchButton']),
				'template' => '{select}',
				'options' => ['width' => '41'],
				'buttons' => [
					'select' => function ($url, $model) {
						return Html::button('<span class="glyphicon glyphicon-ok"></span>', [
							'onclick'=>'selectItem(' . $model->id . ', "' . $model->descripcion . '")', 'class'=>'btn btn-xs btn-gray'
						]);
					},
				],  
			],
        ],
    ]); ?>
	<?php Pjax::end(); ?>
	</div>
	<script>
		function selectItem(id, description) {
			<?=$callbackFunction?>(id, description);
		}
        <?php
            if ($autoSelect && count($dataProvider->getKeys()) == 1) {
                $onlyObject = $dataProvider->getModels()[0];
                echo 'setTimeout(function() { selectItem(' . $onlyObject->id . ', "' . $onlyObject->descripcion . '"); }, 500);';
            }
        ?>
	</script>
</div>
