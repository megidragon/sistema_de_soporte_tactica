<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SocieteParams */

$this->title = 'Dar de baja Parámetro';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="societe-params-delete">

    <?= $this->render('_form', [
        'model' => $model,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'delete',
        'readonly' => true,
        
    ]) ?>

</div>
