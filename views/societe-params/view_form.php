<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SocieteParams */

$this->title = 'Parámetro';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="societe-params-view">

    <?= $this->render('_form', [
        'model' => $model,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'view',
        'readonly' => true,
        
    ]) ?>

</div>
