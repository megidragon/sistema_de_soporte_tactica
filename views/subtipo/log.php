<?php // AVTPL

$this->title = 'Historial de cambios de Subtipo ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Subtipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Subtipo ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [
    'query' => $model->getAuditTrails(),
    'columns' => ['user_id', 'action', 'field', 'old_value', 'new_value', 'diff', 'created'],
]);
?>