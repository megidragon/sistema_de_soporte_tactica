<?php // AVTPL

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;

$title = 'Listado de Subtipos' . (($searchModel->baja == "1") ? ' dados de baja' : '');
?>

<header>
    <table style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td width="50%"><?= Html::img('@web/socs/' . $societe->id . '/logo_print.jpg', ['height' => 40]) ?></td>
                <td style="text-align: right;" width="50%"><?= $title ?></td>
            </tr>
        </tbody>
    </table>
</header>
<footer>
    <table style="border-top: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td style="text-align: center;">Página {PAGENO}</td>
            </tr>
        </tbody>
    </table>
</footer>
<margintop>30</margintop>
<marginbottom>15</marginbottom>

    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
['attribute' => 'id_tipo', 'value' => 'idTipo.descripcion','filter' => \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'id_tipo',
                'value' => '',
                'data' => ArrayHelper::map(\app\models\Tipo::find()->andFilterWhere(['id_societe' => $user->id_societe, 'baja' => 0])->all(), 'id', 'descripcion'),
                'options' => ['placeholder' => 'Tipo'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '150px'
                ],
            ])],
'descripcion',


        ],
    ]); ?>

