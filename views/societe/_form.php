<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

$jsversion = Yii::$app->params['jsversion'];
?>

<script type="text/javascript">
    <?= 'var parameters = ' . json_encode(\app\utils\MainService::getAgencyParameters()) . ";\n" ?>
</script>

<div class="societe-form">
    <!-- AVTPL EVO INI SocieteMapModalDefinition -->
    <!-- AVTPL EVO END SocieteMapModalDefinition -->
    <?php 
		$form = ActiveForm::begin(['id' => 'societe-form',
			'type' => ActiveForm::TYPE_HORIZONTAL,
			'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
		]);
	?>
	<div class=" kv-fieldset-inline">
		<div class="row">
	
            <?php $field = 'name'; ?>
        	<?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
        	<div class="col-sm-3">
                <?= $form->field($model, $field,[
                    'showLabels'=>false
                ])->textInput(['placeholder'=>$model->getAttributeLabel($field), 'maxlength' => true]); ?>
            </div>

            <?php $field = 'url'; ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-4">
                <?= $form->field($model, $field,[
                    'showLabels'=>false
                ])->textInput(['placeholder'=>$model->getAttributeLabel($field)]); ?>
            </div>

            <!-- AVTPL EVO INI SocieteFormAddFields -->
            <!-- AVTPL EVO END SocieteFormAddFields -->

        </div>
        <div class="row">
            <?php $field = 'email'; ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-3">
                <?= $form->field($model, $field,[
                    'showLabels'=>false
                ])->textInput(['placeholder'=>$model->getAttributeLabel($field), 'maxlength' => true]); ?>
            </div>

            <?php $field = 'email_bcc'; ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-3">
                <?= $form->field($model, $field,[
                    'showLabels'=>false
                ])->textInput(['placeholder'=>$model->getAttributeLabel($field), 'maxlength' => true]); ?>
            </div>

        </div>
        <div class="row">
            <?php $field = 'sidebar_collapse'; ?>
            <?= Html::activeLabel($model, $field, [
                'label'=>$model->getAttributeLabel($field), 
                'class'=>'col-sm-2 control-label'
            ]) ?>
            <?php
                $values = [
                    ['id' => '0', 'descripcion' => 'Visible'],
                    ['id' => '1', 'descripcion' => 'Minimizado'],
                ];
            ?>
            <div class="col-sm-2">
                <?= $form->field($model, $field, ['showLabels'=>false])->dropDownList(
                    ArrayHelper::map($values, 'id', 'descripcion')
                ) ?>
            </div>

		</div>

        <!-- AVTPL EVO INI SocieteFormLastRow -->
        <!-- AVTPL EVO END SocieteFormLastRow -->

		<div class="row">
			<div class="col-sm-12">
				<?php 
                    echo Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success crudBtn' : 'btn btn-primary crudBtn']);
                    // AVTPL EVO INI SocieteFormAddButtons
                    // AVTPL EVO END SocieteFormAddButtons
                    if (Yii::$app->user->can('history')) {
                        echo '<div class="a-button">';
                        echo Html::a('<i class="glyphicon glyphicon-time"></i>', ['log'], ['target'=>'_blank', 'class' => 'btn crudBtn', 'title'=>Yii::t('app', 'History')]);
                        echo '</div>';
                    }
                ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form']) ?>
			</div>
		</div>
    </div>
	<?php 
        $this->registerJsFile(Yii::$app->request->baseUrl . '/js/societe.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
        $this->registerJs("$('.required').parent().prev().addClass('required');");
        $this->registerJs("$('form#societe-form').on('beforeSubmit', function(e) { $('.spin-form').show(); $('.btn').attr('disabled','disabled');});"); 
        $this->registerJs("setTimeout(function() { $('#societe-form input').not('[type=hidden]').first().focus(); }, 500);");
        $this->registerJs("$('#societe-form').areYouSure({message: 'Si continúa, se perderán los cambios. Confirma?'});");
    ?>

    <?php ActiveForm::end(); ?>

</div>
