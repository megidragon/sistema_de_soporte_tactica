<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Societe */

$this->title = 'Vizualizar Parámetros de Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros de Empresa', 'url' => ['view']];
?>
<div class="societe-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'url',
            'email',
            'email_bcc',
            // AVTPL EVO INI SocieteViewAddFields
            // AVTPL EVO END SocieteViewAddFields
            [
                'attribute' => 'sidebar_collapse',
                'value' => $model->sidebar_collapse ? 'Minimizado' : 'Visible',
            ]
        ],
    ]) ?>

</div>
