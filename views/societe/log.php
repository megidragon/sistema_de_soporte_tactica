<?php // AVTPL
$this->title = 'Historial de cambios de Parámetros de Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros de Empresa', 'url' => ['update']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [
    'query' => $model->getAuditTrails(),
    'columns' => ['user_id', 'action', 'field', 'old_value', 'new_value', 'diff', 'created'],
]);
?>