<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Societe */

$this->title = 'Actualizar Parámetros de Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Parámetros de Empresa', 'url' => ['update']];
?>
<div class="societe-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
