<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use app\models\User;
use app\models\UserAddForm;
use yii\helpers\Url;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\TipoDocumento;
use app\models\Clientes;
use app\models\Choferes;
use app\models\Societe;
use kartik\checkbox\CheckboxX;
use kartik\datecontrol\DateControl;

$this->title = Yii::t('app/user_add', 'User Configuration');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-form">

    <?php
        Modal::begin([
                'header'=>'<div id="modalHeader"><h4></h4></div>',
                'id'=>'crudModal',
                'size'=>'modal-sm',
            ]);
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>

    <?php 
        $form = ActiveForm::begin(['id' => 'user-form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'enableAjaxValidation' => true,
            'validationUrl' => [$model->isNewRecord ? 'user/validate-create' : 'user/validate-update?id=' . $model->id],
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        ]);     
    ?>
    <div class="kv-fieldset-inline">
        <div class="row">
            

    
    <?= Html::activeLabel($model, 'username', [
        'label'=>$model->getAttributeLabel('username'), 
        'class'=>'col-sm-2 control-label'
    ]) ?>
	<div class="col-sm-3">
        <?= $form->field($model, 'username',[
            'showLabels'=>false
        ])->textInput(['placeholder'=>$model->getAttributeLabel('username'), 'maxlength' => true, 'readonly' => true]); ?>
    </div>
    

    
    <?= Html::activeLabel($model, 'email', [
        'label'=>$model->getAttributeLabel('email'), 
        'class'=>'col-sm-2 control-label'
    ]) ?>
	<div class="col-sm-4">
        <?= $form->field($model, 'email',[
            'showLabels'=>false
        ])->textInput(['placeholder'=>$model->getAttributeLabel('email'), 'maxlength' => true, 'readonly' => true]); ?>
    </div>
    

    
    </div><div class="row"><?= Html::activeLabel($model, 'nombre', [
        'label'=>$model->getAttributeLabel('nombre'), 
        'class'=>'col-sm-2 control-label'
    ]) ?>
	<div class="col-sm-4">
        <?= $form->field($model, 'nombre',[
            'showLabels'=>false
        ])->textInput(['placeholder'=>$model->getAttributeLabel('nombre'), 'maxlength' => true, 'readonly' => true]); ?>
    </div>
    

    
    <?= Html::activeLabel($model, 'sidebar_collapse', [
        'label'=>$model->getAttributeLabel('sidebar_collapse'), 
        'class'=>'col-sm-2 control-label'
    ]) ?>
    <?php
        $values = [
            ['id' => '0', 'descripcion' => 'Visible'],
            ['id' => '1', 'descripcion' => 'Minimizado'],
        ];
    ?>
    <div class="col-sm-2">
        <?= $form->field($model, 'sidebar_collapse',[
            'showLabels'=>false
        ])->dropDownList(ArrayHelper::map($values, 'id', 'descripcion'), []); ?>
    </div>
    


        </div>


        <div class="row">
            <div class="col-sm-12">
                <?php
                    echo Html::button(Yii::t('app/user_add', 'Change Password'), [
                        'value'=>Url::to(['change-password']),
                        'class'=>'btn btn-primary crudModalButton', 
                        'style' => 'float: right;',
                        'modal-title'=>'<h4>' . Yii::t('app/user_add', 'Change Password') . '</h4>'
                    ]);
                    echo Html::submitButton(Yii::t('app', 'Update'), [
                        'class' => 'btn btn-primary', 
                        'style' => 'float: right; margin-right: 10px;',
                    ]);
                ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form']) ?>
            </div>
        </div>
    </div>
    <?php 
        $this->registerJs("$('form#user-form').on('beforeSubmit', function(e) { $('.spin-form').show(); $('.btn').attr('disabled','disabled');});"); 
        $this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js', ['depends'=>'yii\web\YiiAsset']);
        $this->registerJs("$('#user-form').areYouSure({message: 'Si continúa, se perderán los cambios. Confirma?'});");
        $this->registerJs("setTimeout(function() { $('#user-form input').not('[type=hidden]').first().focus(); }, 500);");
        ActiveForm::end(); 
    ?>
</div>
