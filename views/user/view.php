<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'User';
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            'username',
            'status',
            'role',
            ['attribute' => 'id_empresa', 'value' => !is_null($model->idEmpresa) ? $model->idEmpresa->nombre : ''],
            ['attribute' => 'id_contacto', 'value' => !is_null($model->idContacto) ? $model->idContacto->descripcion : ''],
            'email',
            'nombre',
            'sidebar_collapse',

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
