<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Dar de baja Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-delete">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'delete',
        'readonly' => true,
        'candelete' => false, // used for delete button on update action
        
    ]) ?>

</div>
