<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\utils\HtmlService;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
?>
<div class="user-select">

	<div style="" class="">
	<?php Pjax::begin(['id' => 'selectuser']); ?>
    <?php
        $this->registerJS("$('#select-grid-filters').hide();");  // Search row is hidden by default
        $this->registerJS("$('.selectToggleSearchButton').click(function() { $('#select-grid-filters').toggle(); });");
    ?>
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= GridView::widget([
        'id' => 'select-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'responsiveWrap' => false,
        
        'layout' => '{items}{pager}', // Clientes has a join with telefonos, so the number of rows may be wrong
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            
            'username',
            [
                'attribute' => 'id_empresa', 
                'format' => 'html',
                'value' => function ($model) {
                        if ($model->idEmpresa) {
                            if ($model->idEmpresa->baja == 0) {
                                $class = 'label-success';
                            } else {
                                $class = 'label-danger';
                            }
                            return '<span class="label ' . $class . '">' . $model->idEmpresa->nombre . '</span>';
                        } else {
                            return '';
                        }
                    },
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_empresa',
                    'value' => '',
                    'data' => ArrayHelper::map(\app\models\Empresa::find()->andFilterWhere(['id_societe' => $user->id_societe, 'baja' => 0])->all(), 'id', 'nombre'),
                    'options' => ['placeholder' => 'Empresa'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => '150px'
                    ],
                ])
            ],
            'nombre',
            'email',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($model) {
                        if ($model->status === $model::STATUS_ACTIVE) {
                            $class = 'label-success';
                        } elseif ($model->status === $model::STATUS_INACTIVE) {
                            $class = 'label-warning';
                        } else {
                            $class = 'label-danger';
                        }
                        return '<span class="label ' . $class . '">' . $model->statusLabel . '</span>';
                    },
                'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        $arrayStatus,
                        ['class' => 'form-control', 'prompt' => '']
                    )
            ],
            [
                'attribute' => 'role',
                'value' => function ($model) {
                            return $model->roleLabel;
                        },
                'filter' => $arrayRole,
            ],
            
            [
				'class' => 'kartik\grid\ActionColumn',
				'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray selectToggleSearchButton']),
				'template' => '{select}',
				'options' => ['width' => '41'],
				'buttons' => [
					'select' => function ($url, $model) {
						return Html::button('<span class="glyphicon glyphicon-ok"></span>', ['id' => 'select' . $model->id, 'onclick'=>'selectItem(' . $model->id . ', "' . $model->username . '")', 'class'=>'btn btn-xs btn-gray'
						]);
					},
				],  
			],
        ],
    ]); ?>
    <?php HtmlService::clickGridRow($this, 'select-grid', 'select'); ?>
    <?php Pjax::end(); ?>
	</div>
	<script>
		function selectItem(id, description) {
			<?=$callbackFunction?>(id, description);
		}
        <?php
            if ($autoSelect && count($dataProvider->getKeys()) == 1) {
                $onlyObject = $dataProvider->getModels()[0];
                echo 'setTimeout(function() { selectItem(' . $onlyObject->id . ', "' . $onlyObject->username . '"); }, 500);';
            }
        ?>
	</script>
</div>
