<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'view',
        'readonly' => true,
        'candelete' => false, // used for delete button on update action
        
    ]) ?>

</div>
