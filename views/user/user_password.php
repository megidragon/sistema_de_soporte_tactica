<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->title = Yii::t('app/user_add', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-changepassword">
    <?php 
        $form = ActiveForm::begin(['id'=>'changepassword-form',
            'enableAjaxValidation' => true,
            'type' => ActiveForm::TYPE_VERTICAL,
        ]);
    ?>
    <div class="form-group kv-fieldset-inline">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'oldpass')->passwordInput([
                    'placeholder'=>$model->getAttributeLabel('oldpass')
                ]); ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'newpass')->passwordInput([
                    'placeholder'=>$model->getAttributeLabel('newpass')
                ]); ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'repeatnewpass')->passwordInput([
                    'placeholder'=>$model->getAttributeLabel('repeatnewpass')
                ]); ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <?= Html::submitButton(Yii::t('app', 'Update'), [
                    'class' => 'btn btn-primary', 
                    'style' => 'float: right'
                ]) ?>
                <?= Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form-pwd']) ?>
            </div>
        </div>
    </div>
    <?php $this->registerJs("$('form#changepassword-form').on('beforeSubmit', function(e) { $('.spin-form-pwd').show(); $('.btn').attr('disabled','disabled'); });"); ?>
    <?php $this->registerJs("setTimeout(function() { $('#changepassword-form input').not('[type=hidden]').first().focus(); }, 500);"); ?>
    <?php ActiveForm::end(); ?>
</div>