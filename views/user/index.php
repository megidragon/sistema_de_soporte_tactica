<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\utils\HtmlService;

$jsversion = Yii::$app->params['jsversion'];

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
$this->registerJS("$('#user-grid-filters').hide();");  // Search row is hidden by default
$this->registerJS("$(document).on('click', '.crudToggleSearchButton', function() { $('#user-grid-filters').toggle();} );"); // Toggle filter when click on magnifier

$this->title = 'Usuarios' . (($baja == "1") ? ' dados de baja' : '');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <div class="row">
            <div class="col-sm-4">
        <?php 
        	$form = ActiveForm::begin(['id' => 'print-form', 'action'=>['print'], 'options' => ['target'=>'_blank']]);
            echo '<div style="display: none;">';
                echo $form->field($printParams, 'queryParams');
            echo '</div>';
			if ($baja == "0" && Yii::$app->user->can('users-new')) {
				echo Html::button('<i class="glyphicon glyphicon-plus-sign"></i> Crear Usuario', ['value'=>Url::to(['create', 'ajax'=>1]), 'class'=>'btn btn-success crudModalButton', 'modal-title'=>'<h4>Crear Usuario</h4>']);
			}
            if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'com.arielvila.chofer') echo Html::submitButton('<i class="glyphicon glyphicon-print"></i> Imprimir', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px']);
        	ActiveForm::end();
        ?>
            </div>
            <div class="col-sm-8">
                
            </div>
        </div>
    </p>

    
	<?php
		Modal::begin([
				'header'=>'<div id="modalHeader"><h4></h4></div>',
				'id'=>'crudModal',
				'size'=>'modal-lg',
                'closeButton' => false,
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
                'options' => ['tabindex' => false], // For Select2 to work on Modals
			]);
		echo "<div id='modalContent'></div>";
		Modal::end();
	?>
	
	
	<!-- ?php Pjax::begin(); ? -->
	<div style="" class="">
    <?php
        $template = '{view}';
        $width = 42;
        if ($baja == 0 && Yii::$app->user->can('users-upd')
            || $baja == 1 && Yii::$app->user->can('users-upd-del')) {
            $template .= ' {update}';
            $width += 28;
        }
        if ($baja == 0 && Yii::$app->user->can('users-del')) {
            $template .= ' {delete}';
            $width += 28;
        }
    ?>
    
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= GridView::widget([
        'id' => 'user-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'hover' => true,
        'responsiveWrap' => false,
        
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            
            'username',
            [
                'attribute' => 'id_empresa', 
                'format' => 'html',
                'value' => function ($model) {
                        if ($model->idEmpresa) {
                            if ($model->idEmpresa->baja == 0) {
                                $class = 'label-success';
                            } else {
                                $class = 'label-danger';
                            }
                            return '<span class="label ' . $class . '">' . $model->idEmpresa->nombre . '</span>';
                        } else {
                            return '';
                        }
                    },
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_empresa',
                    'value' => '',
                    'data' => ArrayHelper::map(\app\models\Empresa::find()->andFilterWhere(['id_societe' => $user->id_societe, 'baja' => 0])->all(), 'id', 'nombre'),
                    'options' => ['placeholder' => 'Empresa'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'width' => '150px'
                    ],
                ])
            ],
            'nombre',
            'email',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($model) {
                        if ($model->status === $model::STATUS_ACTIVE) {
                            $class = 'label-success';
                        } elseif ($model->status === $model::STATUS_INACTIVE) {
                            $class = 'label-warning';
                        } else {
                            $class = 'label-danger';
                        }
                        return '<span class="label ' . $class . '">' . $model->statusLabel . '</span>';
                    },
                'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        $arrayStatus,
                        ['class' => 'form-control', 'prompt' => '']
                    )
            ],
            [
                'attribute' => 'role',
                'value' => function ($model) {
                            return $model->roleLabel;
                        },
                'filter' => $arrayRole,
            ],
            
            [
				'class' => 'kartik\grid\ActionColumn',
				'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray crudToggleSearchButton']),
				'template' => $template,
				'options' => ['width' => $width],
				'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-eye-open"></span>', ['id' => 'view' . $model->id, 'value'=>Url::to(['view', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Visualizar Usuario</h4>']);
                    },
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['id' => 'update' . $model->id, 'value'=>Url::to(['update', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Actualizar Usuario</h4>']);
                    },
					'delete' => function ($url, $model) {
						return Html::button('<span class="glyphicon glyphicon-trash"></span>', ['id' => 'delete' . $model->id, 'value'=>Url::to(['delete', 'id' => $model->id, 'ajax' => 1]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>Confirmar Baja de Usuario</h4>']);
					},
				],  
			],
        ],
    ]); ?>
    <?php 
        HtmlService::clickGridRow($this, 'user-grid', (strpos($template, '{update}') === false) ? 'view' : 'update', true, -1);
        HtmlService::setWidthLastCol($this, 'user-grid', $width);
    ?>
	</div>
	<!-- ?php Pjax::end(); ? -->
    <?php
        if ($baja == "0" && Yii::$app->user->can('users-view-del')) {
            echo Html::a('<i class="glyphicon glyphicon-trash"></i> Ver Dados de Baja', ['index', 'baja' => 1], ['class' => 'btn btn-danger', 'style' => 'margin-top: 12px;']);
        }
        
    ?>
</div>
<?php
$js = <<<JS
$('body').on('beforeSubmit', '#print-form', function () {
    if (@total@ > @maxRecordsPrint@) {
        return confirm('Se imprimirán @maxRecordsPrint@ registros como máximo. Efectúe un filtro para limitar la cantidad de registros a imprimir. Desea imprimir los primeros @maxRecordsPrint@ registros?');
    }  
    return true;
});
$('#print-form').on('keyup keypress', function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) { 
        e.preventDefault();
        return false;
    }
});
JS;
$js = str_replace('@total@', $dataProvider->getTotalCount(), $js);
$js = str_replace('@maxRecordsPrint@', Yii::$app->params['maxRecordsPrint'], $js);
$this->registerJs($js);
?>


