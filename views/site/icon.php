<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = "Icon";
?>
<div class="site-icon" style="background: #fff">

<div style="padding: 10px">
    <?php
        $family = preg_replace('/-.*/', '', $icon);
        $colorClass = ($color != '') ? 'alert' . ucfirst($color) : '';
    ?>
    <i class="<?= $family ?> <?= $icon ?> <?= $colorClass ?>"></i>
</div>

</div>
