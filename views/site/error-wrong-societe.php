<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Información Restringida';

?>
<div class="site-error">

    <div class="alert alert-danger">
        Usted está intentando acceder a información restringida
    </div>

    <p>
        Por favor contáctenos si piensa que este es un error del sistema. Muchas gracias.
    </p>

</div>
