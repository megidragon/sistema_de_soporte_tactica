<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = 'Sitio fuera de línea por mantenimiento';

?>
<div class="site-warning">

    <div class="alert alert-warning">
        El sitio se encuentra en mantenimiento por el momento
    </div>

    <p>
        <?= $message ?>
    </p>

</div>
