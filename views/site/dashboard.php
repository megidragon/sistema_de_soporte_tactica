<?php
$jsversion = Yii::$app->params['jsversion'];
$this->registerJsFile('/js/jquery.canvasjs.min.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);

$pendingPoints = json_encode($pending_tickets, JSON_NUMERIC_CHECK);
$openPoints = json_encode($open_tickets, JSON_NUMERIC_CHECK);
$closedPoints = json_encode($closed_tickets, JSON_NUMERIC_CHECK);
$time_avg_points = json_encode($time_avg_points, JSON_NUMERIC_CHECK);
?>

<script>
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            exportEnabled: true,
            animationEnabled: true,
            title:{
                text: "Comparacion entre tickets abiertos y cerrados por mes (6 meses)"
            },
            axisX: {
                title: "Meses"
            },
            axisY: {
                title: "Cantidad de tickets ",
                titleFontColor: "#4F81BC",
                lineColor: "#4F81BC",
                labelFontColor: "#4F81BC",
                tickColor: "#4F81BC"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                itemclick: toggleDataSeries
            },
            data: [
                {
                    type: "column",
                    name: "Abiertos",
                    showInLegend: true,
                    color: '#20b2aa',
                    yValueFormatString: "#,##0.# tickets",
                    dataPoints: <?=$openPoints?>
                },
                {
                    type: "column",
                    name: "Cerrados",
                    color: '#d38583',
                    showInLegend: true,
                    yValueFormatString: "#,##0.# tickets",
                    dataPoints: <?=$closedPoints?>
                },
                {
                    type: "column",
                    name: "Pendientes",
                    showInLegend: true,
                    yValueFormatString: "#,##0.# tickets",
                    dataPoints: <?=$pendingPoints?>
                }
            ]
        });

        var lineChart = new CanvasJS.Chart("lineChartContainer", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Promedio de dias en cerrar tickets por mes (12 meses)"
            },
            axisY:{
                includeZero: false
            },
            data: [{
                type: "line",
                dataPoints: <?=$time_avg_points?>
            }]
        });
        chart.render();
        lineChart.render();

        function toggleDataSeries(e) {
            if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else {
                e.dataSeries.visible = true;
            }
            e.chart.render();
        }

    }
</script>


<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<div id="lineChartContainer" style="height: 370px; width: 100%;"></div>
