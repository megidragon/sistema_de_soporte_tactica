<?php // AVTPL
$this->title = Yii::t('app', 'History of changes of {type} {id}', ['type'=>\yii\helpers\StringHelper::basename(get_class($model)), 'id'=>$model->id]);
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [
    'query' => $model->getAuditTrails(),
    'columns' => ['user_id', 'action', 'field', 'old_value', 'new_value', 'diff', 'created'],
]);
?>