<?php // AVTPL

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;

$title = 'Listado de Contactos' . (($searchModel->baja == "1") ? ' dados de baja' : '');
?>

<header>
    <table style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td width="50%"><?= Html::img('@web/socs/' . $societe->id . '/logo_print.jpg', ['height' => 40]) ?></td>
                <td style="text-align: right;" width="50%"><?= $title ?></td>
            </tr>
        </tbody>
    </table>
</header>
<footer>
    <table style="border-top: 1px solid #000000; vertical-align: bottom; font-size: 10pt;" width="100%">
        <tbody>
            <tr>
                <td style="text-align: center;">Página {PAGENO}</td>
            </tr>
        </tbody>
    </table>
</footer>
<margintop>30</margintop>
<marginbottom>15</marginbottom>

    
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}',
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
['attribute' => 'id_empresa', 'value' => 'idEmpresa.nombre','filter' => \kartik\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'id_empresa',
                'value' => '',
                'data' => ArrayHelper::map(\app\models\Empresa::find()->andFilterWhere(['id_societe' => $user->id_societe, 'baja' => 0])->all(), 'id', 'nombre'),
                'options' => ['placeholder' => 'Empresa'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => '150px'
                ],
            ])],
'nombre',
'apellido',
'email',
['attribute' => 'supervisor_tecnico', 'value' => function ($model) use ($yes_no) {
                                        return $yes_no[$model->supervisor_tecnico];
                                    }, 'filter' => $yes_no, 'filterOptions' => ['style' => 'width: 85px;']],
['attribute' => 'supervisor_administ', 'value' => function ($model) use ($yes_no) {
                                        return $yes_no[$model->supervisor_administ];
                                    }, 'filter' => $yes_no, 'filterOptions' => ['style' => 'width: 85px;']],
'tacticaid',


        ],
    ]); ?>

