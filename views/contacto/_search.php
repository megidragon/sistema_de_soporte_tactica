<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContactoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

	<?= $form->field($model, 'id_empresa') ?>

	<?= $form->field($model, 'nombre') ?>

	<?= $form->field($model, 'apellido') ?>

	<?= $form->field($model, 'email') ?>

	<?= $form->field($model, 'supervisor_tecnico') ?>

	<?= $form->field($model, 'supervisor_administ') ?>

	<?= $form->field($model, 'tacticaid') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
