<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;


use kartik\checkbox\CheckboxX;


use app\models\Empresa;
use app\models\Societe;



$jsversion = Yii::$app->params['jsversion'];
?>

<script type="text/javascript">
    <?= "var action = '" . $action . "';\n" ?>
    <?= 'var readonly = ' . ($readonly ? 'true' : 'false') . ";\n" ?>
    <?= 'var ajax = ' . ($ajax ? 'true' : 'false') . ";\n" ?>
</script>
<?php if (!$ajax) { ?>
    
<?php } ?>
<div style="display: none;" id="crudTexts" data-confirm-duplicate="<?= Yii::t('app', 'Confirm save this element as new? (all pending changes will be saved in the new element only)') ?>" data-confirm-recover="<?= Yii::t('app', 'Confirm recover this element?') ?>" data-confirm-delete="<?= Yii::t('app', 'Confirm delete this element?') ?>" data-choose="<?= Yii::t('app', 'Choose') ?>"></div>

<div class="contacto-form">

    

    <?php 
        $validationUrl = '';
        if ($action == 'create') $validationUrl = 'contacto/validate-create';
        if ($action == 'update') $validationUrl = 'contacto/validate-update?id=' . $model->id;
		$form = ActiveForm::begin(['id' => 'crud-form',
			'type' => ActiveForm::TYPE_VERTICAL, 'options' => [],
            
		]);
	?>
    <input type="hidden" name="aclose" class="neverReadonly" value="<?= $aclose ?>">
    <input type="hidden" id="submitType" name="submitType" class="neverReadonly" value="">
	<div class="form-group kv-fieldset-inline">
		<div class="row">

	
			<div class="col-sm-12">
			<?= $form->field($model, 'id_empresa')->widget(\kartik\select2\Select2::classname(), [
				'data' => ArrayHelper::map(
					Empresa::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])
					->orFilterWhere(['id' => $model->id_empresa])->all(), 
					'id', 'nombre'
				), 'options' => ['placeholder'=>Yii::t('app', 'Choose')], 'pluginOptions' => ['allowClear'=>true]
			]) ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'nombre')->textInput(['placeholder'=>$model->getAttributeLabel('nombre')]); ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'apellido')->textInput(['placeholder'=>$model->getAttributeLabel('apellido')]); ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'email')->textInput(['placeholder'=>$model->getAttributeLabel('email')]); ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'supervisor_tecnico')->textInput(['placeholder'=>$model->getAttributeLabel('supervisor_tecnico')]); ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'supervisor_administ')->textInput(['placeholder'=>$model->getAttributeLabel('supervisor_administ')]); ?>
		</div>

	

	
			<div class="col-sm-12">
			<?= $form->field($model, 'tacticaid')->textInput(['placeholder'=>$model->getAttributeLabel('tacticaid')]); ?>
		</div>

	

		</div>

		<div class="row">
            <?php if ($action == 'delete') { ?>
                <?php if (isset($ref_qtty) && $ref_qtty) { ?>
                    <label class="col-sm-4 control-label"><?= $ref_msg ?></label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'new_id', ['showLabels'=>false])->widget(\kartik\select2\Select2::classname(), [
                            'data' => ArrayHelper::map(
                                \app\models\Contacto::find()->andFilterWhere(['baja' => false, 'id_societe' => $user->id_societe])->andFilterWhere(['not', ['id' => $model->id]])->all(), 
                                'id', 'nombre'
                            ), 'options' => ['placeholder'=>Yii::t('app', 'Do Not Replace'), 'class' => 'neverReadonly'], 'pluginOptions' => ['allowClear'=>true]

                        ]) ?>
                    </div>
                <?php } elseif (isset($ref_msg)) { ?>
                    <label class="col-sm-7 control-label"><?= $ref_msg ?></label>
                <?php } else { ?>
                    <div class="col-sm-7"></div>
                <?php } ?>
                <div class="col-sm-5">
            <?php } else { ?>
                <div class="col-sm-12">
            <?php } ?>
				<?php
                    switch ($action) {
                        case 'create':
                            echo Html::button(Yii::t('app', 'Create'), ['class' => 'btn btn-success crudBtn', 'onclick' => 'setSubmitType(SUBMIT_MAIN);']);
                            break;
                        case 'update':
                            echo Html::button(Yii::t('app', 'Update'), ['class' => 'btn btn-primary crudBtn', 'onclick' => 'setSubmitType(SUBMIT_MAIN);']);
                            break;
                        case 'delete':
                            echo Html::button(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger crudBtn neverReadonly', 'onclick' => 'setSubmitType(SUBMIT_MAIN);']);
                            break;
                    }
                    if ($model->baja && $action == 'update' && Yii::$app->user->can('none')) {
                        echo Html::button(Yii::t('app', 'Recover'), ['class' => 'btn btn-info crudBtn', 'onclick' => 'setSubmitType(SUBMIT_RECOVER);']);
                    }
                    if (!$model->baja && $action == 'update' && $candelete && Yii::$app->user->can('none')) {
                        echo Html::button(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger crudBtn', 'onclick' => 'setSubmitType(SUBMIT_DELETE);']);
                    }
                    if ($ajax) {
                        echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default crudBtn neverReadonly', 'onclick' => "if ($('#crud-form').hasClass('dirty')) { if (confirm('Si continúa, se perderán los cambios. Confirma?')) { $('#crudModal').modal('hide');  $('#crud-form').trigger('reinitialize.areYouSure'); } } else { $('#crudModal').modal('hide'); }"]);
                    } else {
                        if ($aclose) {
                            echo Html::a(Yii::t('app', 'Close'), ['aclose'], ['class' => 'btn btn-default crudBtn']);
                        } elseif ($model->baja == 2) {
                            echo Html::a(Yii::t('app', 'Close'), ['backup', 'id' => $model->id_backup], ['class' => 'btn btn-default crudBtn']);
                        } else {
                            echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default crudBtn']);
                        }
                    }
                    if ($action != 'create' && Yii::$app->user->can('history') && true && $model->baja != 2) {
                        echo '<div class="a-button">';
                        echo Html::a('<i class="glyphicon glyphicon-time"></i>', ['log', 'id'=>$model->id], ['target'=>'_blank', 'class'=>'btn crudBtn', 'title'=>Yii::t('app', 'History')]);
                        echo '</div>';
                    }
                    echo Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form']);
                ?>
			</div>
		</div>
    </div>
    
    <?php 
        $this->registerJsFile(Yii::$app->request->baseUrl . '/js/crud.js?v=' . $jsversion, ['depends'=>'yii\web\YiiAsset']);
        $this->registerJs("crudInit();");
        $this->registerJs("$('form#crud-form').on('beforeSubmit', function(e) { return crudSubmit(); });");
        $this->registerJs("$('#crud-form').areYouSure({message: '" . Yii::t('app', 'All changes will be lost. Confirm?') . "'});");
    ?>
    <?php ActiveForm::end(); ?>

</div>
<?php if (!$ajax) { ?>

<?php } ?>
