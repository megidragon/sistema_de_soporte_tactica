<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Contacto */

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = ['label' => 'Contactos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacto-view">

    
    <?php
        $yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            ['attribute' => 'id_empresa', 'value' => !is_null($model->idEmpresa) ? $model->idEmpresa->nombre : ''],
            'nombre',
            'apellido',
            'email',
            ['attribute' => 'supervisor_tecnico', 'value' => $yes_no[$model->supervisor_tecnico]],
            ['attribute' => 'supervisor_administ', 'value' => $yes_no[$model->supervisor_administ]],
            'tacticaid',

        ],
    ]) ?>


    <div class="row">
        <div class="col-sm-12">
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-top: 12px;']);
                }
            ?>
        </div>
    </div>

</div>
