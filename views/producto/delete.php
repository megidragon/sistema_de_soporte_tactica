<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = 'Dar de baja Producto';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-view">

    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            ['attribute' => 'id_empresa', 'value' => !is_null($model->idEmpresa) ? $model->idEmpresa->nombre : ''],
            'codigo',
            'nro_serie',
            'nombre_sector',
            ['attribute' => 'id_prioridad', 'value' => !is_null($model->idPrioridad) ? $model->idPrioridad->codigo : ''],
            'datos_red',
            'mac_address',
            'fabricante',
            'posicion',
            'tacticaid',

        ],
    ]) ?>


    <?php 
		$form = ActiveForm::begin([
			'type' => ActiveForm::TYPE_HORIZONTAL,
			'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
		]);
	?>
	<div class="row">
        <div class="col-sm-12" style="margin-top: 12px">
			<?= Html::submitButton(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger', 'style' => 'float: right']) ?>
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-right: 10px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-right: 10px;']);
                }
            ?>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
	
</div>
