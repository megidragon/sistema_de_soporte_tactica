<?php // AVTPL

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Elemento */

$this->title = 'Crear Elemento';
$this->params['breadcrumbs'][] = ['label' => 'Elementos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elemento-create">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'ajax' => $ajax,
        'aclose' => $aclose,
        'action' => 'create',
        'readonly' => false,
        
    ]) ?>

</div>
