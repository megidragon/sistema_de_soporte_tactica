<?php // AVTPL

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\ActiveForm;
use app\models\User;
use yii\data\ArrayDataProvider;
use app\utils\HtmlService;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/crudmodal.js', ['depends'=>'yii\web\YiiAsset']);
$this->registerJS("$('#permissions-grid-filters').hide();");  // Search row is hidden by default
$this->registerJS("$(document).on('click', '.crudToggleSearchButton', function() { $('#permissions-grid-filters').toggle();} );"); // Toggle filter when click on magnifier

$this->title = Yii::t('app/permissions', 'User Permissions per Role');
$this->params['breadcrumbs'][] = $this->title;

$yes_no = [true => Yii::t('app', 'Yes'), false => Yii::t('app', 'No')];
$role_names = array_keys($roles);

$permissionfilter = Yii::$app->request->getQueryParam('filterpermission', '');

$searchModel = ['permission' => $permissionfilter];
foreach ($role_names as $role_name) {
    $searchModel[$role_name] = Yii::$app->request->getQueryParam('filter' . $role_name, '');
}
$filteredRows = array_filter($rows, function($item) use($searchModel, $role_names) {
    $result = true;
    if (strlen($searchModel['permission']) > 0) {
        $result &= (strpos(strtolower($item['permission']), strtolower($searchModel['permission'])) !== false);
    }
    foreach ($role_names as $role_name) {
        if (strlen($searchModel[$role_name]) > 0) {
            $result &= ($item[$role_name] == $searchModel[$role_name]);
        }
    }
    return $result;
});

$dataProvider = new ArrayDataProvider([
    'allModels' => $filteredRows,
    'pagination' => ['pageSize' => 20],
    'sort' => [
        'attributes' => array_merge(['permission'], $role_names),
        'defaultOrder' => ['permission' => SORT_ASC],
    ],
]);

$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'attribute' => 'permission',
        'label' => Yii::t('app/permissions', 'Permission'),
        'filter' => '<input class="form-control" name="filterpermission" value="' . $searchModel['permission'] . '" type="text">',
    ]
];
foreach ($role_names as $role_name) {
    array_push($columns, [
        'attribute' => $role_name,
        'label' => $roles[$role_name],
        'value' => function ($model) use ($role_name, $yes_no) {
                return $yes_no[$model[$role_name]];
            },
        'filter' => Html::dropDownList('filter' . $role_name, $searchModel[$role_name], $yes_no, ['prompt'=>'', 'class' => 'form-control']),
    ]);
}

$template = '';
if (Yii::$app->user->can('permissions-upd')) {
    $template = '{update}';
}

array_push($columns, [
    'class' => 'kartik\grid\ActionColumn',
    'header' => Html::button('<span class="glyphicon glyphicon-search"></span>', ['class'=>'btn btn-xs btn-gray crudToggleSearchButton']),
    'template' => $template,
    'options' => ['width' => 42],
    'buttons' => [
        'update' => function ($url, $model) {
            if (in_array($model['name'], Yii::$app->params['permissions-system'])) {
                return Html::button('<span class="glyphicon glyphicon-ban-circle"></span>', ['class'=>'btn btn-xs btn-gray', 'title' => Yii::t('app/permissions', 'A system permission cannot be updated')]);
            } elseif (in_array($model['name'], Yii::$app->params['permissions-on-off'])) {
                return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['id' => 'update' . preg_replace('/.*id=/', '', $url), 'value'=>Url::to(['update-switch', 'name' => $model['name']]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>' . Yii::t('app/permissions', 'Update Permission') . '</h4>']);
            } else {
                return Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['id' => 'update' . preg_replace('/.*id=/', '', $url), 'value'=>Url::to(['update-minimum', 'name' => $model['name']]), 'class'=>'btn btn-xs btn-gray crudModalButton', 'modal-title'=>'<h4>' . Yii::t('app/permissions', 'Update Permission') . '</h4>']);
            }
        },
    ],
]);
?>

<?php
    Modal::begin([
            'header'=>'<div id="modalHeader"><h4></h4></div>',
            'id'=>'crudModal',
            'size'=>'modal-sm',
            'closeButton' => false,
            'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
            'options' => ['tabindex' => false], // For Select2 to work on Modals
        ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
?>

<div class="permissions-index">

    <!-- ?php Pjax::begin(); ? -->
    <div style="" class="table-responsive">

    <?= GridView::widget([
        'id' => 'permissions-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
        'hover' => true,
        'responsiveWrap' => false,
    ]); ?>
    <?php 
        HtmlService::clickGridRow($this, 'permissions-grid', 'update');
    ?>
    </div>
    <!-- ?php Pjax::end(); ? -->
</div>
<?php
$js = <<<JS
$('body').on('beforeSubmit', '#print-form', function () {
    if (@total@ > @maxRecordsPrint@) {
        return confirm('Se imprimirán @maxRecordsPrint@ registros como máximo. Efectúe un filtro para limitar la cantidad de registros a imprimir. Desea imprimir los primeros @maxRecordsPrint@ registros?');
    }  
    return true;
});
$('#print-form').on('keyup keypress', function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) { 
        e.preventDefault();
        return false;
    }
});
JS;
$js = str_replace('@total@', $dataProvider->getTotalCount(), $js);
$js = str_replace('@maxRecordsPrint@', Yii::$app->params['maxRecordsPrint'], $js);
$this->registerJs($js);
?>


