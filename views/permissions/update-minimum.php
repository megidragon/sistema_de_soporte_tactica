<?php // AVTPL

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\checkbox\CheckboxX;
use app\models\Societe;

$jsversion = Yii::$app->params['jsversion'];
?>

<div class="permisos-form">

    

    <?php 
        $form = ActiveForm::begin(['id' => 'permisos-form',
            'type' => ActiveForm::TYPE_VERTICAL, 'options' => [],
            
        ]);
    ?>
    <div class="form-group kv-fieldset-inline">
        <div style="display: none">
            <?= $form->field($model, 'name', ['showLabels'=>false])->hiddenInput() ?>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'desc')->textInput(['readonly'=>true]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'role')->dropDownList($roles); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php
                    echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right']);
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default neverReadonly', 'style' => 'float: right; margin-right: 10px;', 'onclick' => "if ($('#permisos-form').hasClass('dirty')) { if (confirm('Si continúa, se perderán los cambios. Confirma?')) { $('#crudModal').modal('hide');  $('#permisos-form').trigger('reinitialize.areYouSure'); } } else { $('#crudModal').modal('hide'); }"]);
                    echo Html::img('@web/images/spinner_34.gif', ['class'=>'spinHidden spinFloatR spin-form']);
                ?>
            </div>
        </div>
    </div>
    <?php 
        $this->registerJs("$('form#permisos-form').on('beforeSubmit', function(e) { $('.spin-form').show(); $('.btn').attr('disabled','disabled'); });");
        $this->registerJs("setTimeout(function() { $('#permisos-form input').not('[type=hidden]').first().focus(); }, 500);");
        $this->registerJs("$('#permisos-form').areYouSure({message: '" . Yii::t('app', 'All changes will be lost. Confirm?') . "'});");
    ?>
    <?php ActiveForm::end(); ?>

</div>
