<?php // AVTPL

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Tipo */

$this->title = 'Dar de baja Tipo';
$this->params['breadcrumbs'][] = ['label' => 'Tipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-view">

    
    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th>{label}</th><td>{value}</td></tr>',
        'attributes' => [
            'descripcion',

        ],
    ]) ?>


    <?php 
		$form = ActiveForm::begin([
			'type' => ActiveForm::TYPE_HORIZONTAL,
			'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
		]);
	?>
	<div class="row">
        <div class="col-sm-12" style="margin-top: 12px">
			<?= Html::submitButton(Yii::t('app', 'Remove'), ['class' => 'btn btn-danger', 'style' => 'float: right']) ?>
            <?php 
                if ($ajax) {
                    echo Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default', 'style' => 'float: right; margin-right: 10px;', 'onclick' => "$('#crudModal').modal('hide')"]);
                } else {
                    echo Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-default', 'style' => 'float: right; margin-right: 10px;']);
                }
            ?>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
	
</div>
