<?php

use yii\db\Migration;
use app\utils\MigrationService;

class m160724_201605_view_tickets extends Migration
{
    public function up() {
        MigrationService::createPermissionAllSocs('manager', 'incidente-view', 'Incidents: View Configuration');
        echo "\n******* REMEMBER TO ENTER TO /permissions/invalidate-cache PAGE TO INVALIDATE CACHE *******\n";
    }

    public function down() {
        MigrationService::removePermissionAllSocs('manager', 'incidente-view');
    }

}
