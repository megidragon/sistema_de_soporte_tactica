<?php

use yii\db\Migration;
use app\utils\MigrationService;

class m160724_201602_user_roles extends Migration
{
    public function up() {
        MigrationService::createUniqueRoleAllSocs('client', 'Client');
        MigrationService::assignUniqueSubroleAllSocs('client', 'user');
        MigrationService::createPermissionAllSocs('client', 'report', 'Report');
    }

    public function down() {
    }

}
