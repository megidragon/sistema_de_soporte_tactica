<?php

use yii\db\Migration;
use app\utils\MigrationService;

class m160724_201603_tablas_incidente extends Migration
{
    public function up() {
        MigrationService::runSqlFile('sql/migrations/base_add001_create.sql');
        MigrationService::createPermissionAllSocs('manager', 'incidente-config-view', 'Incidents: View Configuration');
        MigrationService::createPermissionAllSocs('manager', 'incidente-config-upd', 'Incidents: Update Configuration');
        MigrationService::createPermissionAllSocs('client', 'incidente-create', 'Incidents: Create New');
        echo "\n******* REMEMBER TO ENTER TO /permissions/invalidate-cache PAGE TO INVALIDATE CACHE *******\n";
    }

    public function down() {
        MigrationService::runSqlFile('sql/migrations/base_add001_drop.sql');
        MigrationService::removePermissionAllSocs('manager', 'incidente-config-view');
        MigrationService::removePermissionAllSocs('manager', 'incidente-config-upd');
        MigrationService::removePermissionAllSocs('client', 'incidente-create');
    }

}
