<?php

use yii\db\Migration;
use app\utils\MigrationService;

class m160724_201604_contacto_supervisor extends Migration
{
    public function up() {
        MigrationService::runSqlFile('sql/migrations/base_add002_create.sql');
    }

    public function down() {
    }

}
