<?php // AVTPL

use yii\db\Migration;
use app\utils\MigrationService;

class m160724_201601_base_create extends Migration
{
    public function up() {
        MigrationService::runSqlFile('sql/migrations/base_create.sql');
        MigrationService::runSqlFile('sql/commands/new_agency_create.sql', 1, 1);
        MigrationService::createUniqueRoleAllSocs('admin', 'Admin');
        MigrationService::createUniqueRoleAllSocs('manager', 'Manager');
        MigrationService::createUniqueRoleAllSocs('user', 'User');
        MigrationService::assignUniqueSubroleAllSocs('admin', 'manager');
        MigrationService::assignUniqueSubroleAllSocs('manager', 'user');
        MigrationService::createPermissionAllSocs('manager', 'manage', 'Manage');
        MigrationService::createPermissionAllSocs('admin', 'setup', 'Setup');
        MigrationService::createPermissionAllSocs('manager', 'history', 'History');
        MigrationService::createPermissionAllSocs('manager', 'email-view', 'Sent Emails: View');
        MigrationService::createPermissionAllSocs('manager', 'add-param-upd', 'Additional Params: Update');
        MigrationService::createPermissionAllSocs('manager', 'add-param-view', 'Additional Params: View');
        MigrationService::createPermissionAllSocs('manager', 'societe-upd', 'Company Params: Update');
        MigrationService::createPermissionAllSocs('manager', 'societe-view', 'Company Params: View');
        MigrationService::createPermissionAllSocs('manager', 'default-new', 'Conf. Others: Create');
        MigrationService::createPermissionAllSocs('manager', 'default-del', 'Conf. Others: Delete');
        MigrationService::createPermissionAllSocs('manager', 'default-upd', 'Conf. Others: Update');
        MigrationService::createPermissionAllSocs('manager', 'default-upd-del', 'Conf. Others: Update Deleted');
        MigrationService::createPermissionAllSocs('manager', 'default-view', 'Conf. Others: View');
        MigrationService::createPermissionAllSocs('manager', 'default-view-del', 'Conf. Others: View Deleted');
        MigrationService::createPermissionAllSocs('manager', 'permissions-upd', 'Permissions: Update');
        MigrationService::createPermissionAllSocs('manager', 'permissions-view', 'Permissions: View');
        MigrationService::createPermissionAllSocs('manager', 'users-new', 'Users: Create');
        MigrationService::createPermissionAllSocs('manager', 'users-del', 'Users: Delete');
        MigrationService::createPermissionAllSocs('manager', 'users-upd', 'Users: Update');
        MigrationService::createPermissionAllSocs('manager', 'users-view-del', 'Users: View Deleted');
        MigrationService::createPermissionAllSocs('manager', 'users-view', 'Users: View');
        MigrationService::createPermissionAllSocs('manager', 'users-upd-del', 'Users: Update Deleted');
        MigrationService::createPermissionAllSocs('admin', 'audit', 'Audit');
        
        MigrationService::runSqlFile('sql/commands/new_agency_create_admin.sql', 1, 1);
        echo "\nEXECUTE: yii migrate --migrationPath=@bedezign/yii2/audit/migrations\n";
    }

    public function down() {
        MigrationService::runSqlFile('sql/migrations/base_drop.sql');
    }

}
