<?php

use yii\db\Migration;
use app\utils\MigrationService;


/**
 * Class m190514_155639_create_tecnic_rol
 */
class m190514_155639_create_tecnic_rol extends Migration
{
    public function up()
    {
        MigrationService::createUniqueRoleAllSocs('technical', 'Client');
        MigrationService::assignUniqueSubroleAllSocs('technical', 'client');
        MigrationService::createPermissionAllSocs('technical', 'update_file', 'Update File');
    }

    public function down()
    {
        echo "m190514_155639_create_tecnic_rol cannot be reverted.\n";

        return false;
    }
}
