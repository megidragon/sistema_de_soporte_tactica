<?php // AVTPL

namespace app\models;

use Yii;

/**
 * This is the model class for table "errors".
 *
 * @property integer $id
 * @property string $type
 * @property integer $id_user
 * @property string $url
 * @property string $date_reg
 * @property string $time_reg
 * @property integer $id_societe
 *
 * @property User $idUser
 * @property Societe $idSociete
 */
class Errors extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'errors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['id'], 'integer'],
			[['type'], 'string', 'max' => 50],
			[['id_user'], 'safe'],
			[['url'], 'safe'],
			[['date_reg'], 'safe'],
			[['time_reg'], 'safe'],
			[['id_societe'], 'safe'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/errors', 'Id'),
			'type' => Yii::t('app/errors', 'Type'),
			'id_user' => Yii::t('app/errors', 'Id User'),
			'url' => Yii::t('app/errors', 'Url'),
			'date_reg' => Yii::t('app/errors', 'Date Reg'),
			'time_reg' => Yii::t('app/errors', 'Time Reg'),
			'id_societe' => Yii::t('app/errors', 'Id Societe')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idUser';
		$fields[] = 'idSociete';
		return $fields;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}