<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;

class Societe extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'societe';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior'
            ];
        } else {
            return [];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['name'], 'string', 'max' => 45],
            [['url'], 'url'],
            [['email', 'email_bcc'], 'email'],
            [['name', 'url', 'email'], 'required'],
            [['sidebar_collapse'], 'safe'],
            // AVTPL EVO INI SocieteModelAddRules
            // AVTPL EVO END SocieteModelAddRules
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'name' => 'Nombre',
            'url' => 'Dir. Web',
            'email' => 'Email Respuesta',
            'email_bcc' => 'Email Copia',
            'sidebar_collapse' => 'Estado Menú',
            // AVTPL EVO INI SocieteModelAddLabels
            // AVTPL EVO END SocieteModelAddLabels
			];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

}