<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;


/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
			[['username'], 'safe'],
			[['id_empresa'], 'safe'],
			[['nombre'], 'safe'],
			[['email'], 'safe'],
			[['status'], 'number'],
			[['role'], 'safe'],
			[['baja'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $baja, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $this->baja = $baja;
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = User::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;
        
        $dataProvider = new ActiveDataProvider($dataParams);

		$query->joinWith('idEmpresa idEmpresa');
		$query->joinWith('idContacto idContacto');

        $query->andFilterWhere([
			'user.id_societe' => $societe,
			'user.baja' => $this->baja,
            'user.id_contacto' => $this->id_contacto,

            'user.sidebar_collapse' => $this->sidebar_collapse,

            
            'user.id_empresa' => $this->id_empresa,

            
            
            'user.status' => $this->status,

            
        ]);
        if ($this->role == null) {
            $query->andFilterWhere(['role' => array_keys(self::getArrayRole($societe))]);
        } else {
            $query->andFilterWhere(['role' => $this->role]);
        }

        

        $query->andFilterWhere(['like', 'user.username', $this->username])
->andFilterWhere(['like', 'user.nombre', $this->nombre])
->andFilterWhere(['like', 'user.email', $this->email])
->andFilterWhere(['like', 'user.role', $this->role])
;

        return $dataProvider;
    }

	public function quickSearch($params, $text, $baja = -1, $pageSize = 20) {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = User::find();
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);
		
		if ($baja >= 0) {
			$query->andFilterWhere([
    			'user.baja' => $baja,
			]);
		}
        
        $query->andFilterWhere([
			'user.id_societe' => $societe,
            'user.id_contacto' => $this->id_contacto,

            'user.sidebar_collapse' => $this->sidebar_collapse,

            
            'user.id_empresa' => $this->id_empresa,

            
            
            'user.status' => $this->status,

            
        ]);
        if ($this->role == null) {
            $query->andFilterWhere(['role' => array_keys(self::getArrayRole($societe))]);
        } else {
            $query->andFilterWhere(['role' => $this->role]);
        }

        $query->andFilterWhere(['like', 'user.username', $this->username])
->andFilterWhere(['like', 'user.nombre', $this->nombre])
->andFilterWhere(['like', 'user.email', $this->email])
->andFilterWhere(['like', 'user.role', $this->role])
;

		
        return $dataProvider;
	}

    private static function explodeRangeAsDateYMD($dateRangeDMY) {
        list($start_date, $end_date) = explode(' - ', $dateRangeDMY);
        return [substr($start_date, 6, 4) . '-' . substr($start_date, 3, 2) . '-' . substr($start_date, 0, 2), substr($end_date, 6, 4) . '-' . substr($end_date, 3, 2) . '-' . substr($end_date, 0, 2)];
    }

}
