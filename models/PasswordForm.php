<?php // AVTPL
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Login;

class PasswordForm extends Model {
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules() {
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
        ];
    }

    public function findPasswords($attribute, $params) {
        $user = User::findOne(Yii::$app->user->getId());
        if (!Yii::$app->getSecurity()->validatePassword($this->oldpass, $user->password_hash)) {
            $this->addError($attribute, Yii::t('app/user_add', 'Old password is incorrect'));
        }
    }

    public function attributeLabels() {
        return [
            'oldpass'=>Yii::t('app/user_add', 'Old Password'),
            'newpass'=>Yii::t('app/user_add', 'New Password'),
            'repeatnewpass'=>Yii::t('app/user_add', 'Repeat New Password'),
        ];
    }

}

?>