<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


/**
 * This is the model class for table "elemento".
 *
 * @property number $id
 * @property string $descripcion
 * @property number $id_subtipo
 * @property number $id_societe
 * @property number $baja
 *
 * @property Subtipo $idSubtipo
 * @property Societe $idSociete
 */
class Elemento extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'elemento';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['id_subtipo'], 'safe'],
			[['descripcion'], 'string', 'max' => 500],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/elemento', 'Id'),
			'descripcion' => Yii::t('app/elemento', 'Descripcion'),
			'id_subtipo' => Yii::t('app/elemento', 'Id Subtipo'),
			'id_societe' => Yii::t('app/elemento', 'Id Societe'),
			'baja' => Yii::t('app/elemento', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idSubtipo';
		return $fields;
	}

    public function description() {
        return $this->descripcion;
    }

    public static function updateFromJsonString($string, $id_subtipo, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Elemento();
                    if (isset($value->id)) {
                        $submodel = Elemento::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->descripcion)) $submodel->descripcion = $value->descripcion;
                    if (isset($value->id_subtipo)) $submodel->id_subtipo = $value->id_subtipo;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_subtipo = $id_subtipo;
                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubtipo()
    {
        return $this->hasOne(Subtipo::className(), ['id' => 'id_subtipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}