<?php

namespace app\models;

use Yii;
use yii\base\Model;

class TicketExport extends Model
{
    public $dummy;
    public $id;
    public $empresa;
    public $contactonombre;
    public $contactoapellido;
    public $fechaorigenweb;
    public $origen;
    public $prioridad;
    public $tipo;
    public $incidentetipo;
    public $incidentesubtipo;
    public $incidenteelemento;
    public $codigoproducto;
    public $problemadescripcion;
    public $fecharequerida;
}