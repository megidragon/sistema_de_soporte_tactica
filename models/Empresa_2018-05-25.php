<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "empresa".
 *
 * @property number $id
 * @property string $tacticaid
 * @property string $nombre
 * @property number $id_societe
 * @property number $baja
 *
 * @property Societe $idSociete
 */
class Empresa extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $new_id;



    public static function tableName()
    {
        return 'empresa';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['nombre'], 'string', 'max' => 500],
			[['tacticaid'], 'string', 'max' => 20],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/empresa', 'Id'),
			'tacticaid' => Yii::t('app/empresa', 'Tacticaid'),
			'nombre' => Yii::t('app/empresa', 'Nombre'),
			'id_societe' => Yii::t('app/empresa', 'Id Societe'),
			'baja' => Yii::t('app/empresa', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		return $fields;
	}

    public function description() {
        return $this->nombre;
    }

    public static function updateFromJsonString($string, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Empresa();
                    if (isset($value->id)) {
                        $submodel = Empresa::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->tacticaid)) $submodel->tacticaid = $value->tacticaid;
                    if (isset($value->nombre)) $submodel->nombre = $value->nombre;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }


	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'baja' => 0]);
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		// TODO: Implement findIdentityByAccessToken() method.
	}

	public function getId()
	{
		return $this->getPrimaryKey();
	}

	public function getAuthKey()
	{
		return $this->auth_key;
	}

	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}
}