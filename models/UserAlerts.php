<?php // AVTPL

namespace app\models;

use Yii;

class UserAlerts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_alerts';
    }

}