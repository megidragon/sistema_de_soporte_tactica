<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property number $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $role
 * @property number $status
 * @property string $nombre
 * @property number $id_empresa
 * @property number $id_contacto
 * @property number $id_societe
 * @property number $sidebar_collapse
 * @property number $baja
 *
 * @property Empresa $idEmpresa
 * @property Contacto $idContacto
 * @property Societe $idSociete
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $new_id;


    const STATUS_DELETED = -1;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;
    const ROLES_UNDER = 1;
    const ROLES_ALL = 2;
    const ROLES_NO_BASIC = 3;
    public $password;
    public $repassword;
    private $_statusLabel;
    private $_roleLabel;

    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    public function scenarios()
    {
        return [
            'admin-create' => ['username', 'email', 'status', 'role', 'nombre', 'sidebar_collapse', 'id_empresa', 'id_contacto'],
            'admin-update' => ['username', 'email', 'status', 'role', 'nombre', 'sidebar_collapse', 'id_empresa', 'id_contacto'],
            'update-passwd' => ['password_hash'],
            'update-reset_token' => ['password_reset_token'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['id_contacto'], 'safe'],
			[['sidebar_collapse'], 'safe'],
			[['username'], 'string', 'max' => 255],
			[['id_empresa'], 'safe'],
			[['nombre'], 'string', 'max' => 45],
			[['email'], 'string', 'max' => 255],
			[['status'], 'number'],
			[['role'], 'string', 'max' => 64],
			[['username', 'email'], 'required'],
            [['username', 'email'], 'trim'],
            // Unique
            [['username'], 'unique'],
            // Username
            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/'],
            ['username', 'string', 'min' => 3, 'max' => 30],
            // E-mail
            ['email', 'string', 'max' => 100],
            ['email', 'email'],
            [['password_hash'], 'safe'],
            //['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
            [['id_empresa'], 'required', 'when' => function($model) {
                return substr($model->role, -7) == '-client';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user-role').val().substr(-7) === '-client';
            }"],
            [['id_contacto'], 'required', 'when' => function($model) {
                return substr($model->role, -7) == '-client';
            }, 'whenClient' => "function (attribute, value) {
                return $('#user-role').val().substr(-7) === '-client';
            }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/user', 'Id'),
			'username' => Yii::t('app/user', 'Username'),
			'auth_key' => Yii::t('app/user', 'Auth Key'),
			'password_hash' => Yii::t('app/user', 'Password Hash'),
			'password_reset_token' => Yii::t('app/user', 'Password Reset Token'),
			'email' => Yii::t('app/user', 'Email'),
			'role' => Yii::t('app/user', 'Role'),
			'status' => Yii::t('app/user', 'Status'),
			'nombre' => Yii::t('app/user', 'Nombre'),
			'id_empresa' => Yii::t('app/user', 'Id Empresa'),
			'id_contacto' => Yii::t('app/user', 'Id Contacto'),
			'id_societe' => Yii::t('app/user', 'Id Societe'),
			'sidebar_collapse' => Yii::t('app/user', 'Sidebar Collapse'),
			'baja' => Yii::t('app/user', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idEmpresa';
		$fields[] = 'idContacto';
		return $fields;
	}

    public function description() {
        return $this->username;
    }

    public static function updateFromJsonString($string, $id_empresa, $id_contacto, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new User();
                    if (isset($value->id)) {
                        $submodel = User::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->username)) $submodel->username = $value->username;
                    if (isset($value->auth_key)) $submodel->auth_key = $value->auth_key;
                    if (isset($value->password_hash)) $submodel->password_hash = $value->password_hash;
                    if (isset($value->password_reset_token)) $submodel->password_reset_token = $value->password_reset_token;
                    if (isset($value->email)) $submodel->email = $value->email;
                    if (isset($value->role)) $submodel->role = $value->role;
                    if (isset($value->status)) $submodel->status = $value->status;
                    if (isset($value->nombre)) $submodel->nombre = $value->nombre;
                    if (isset($value->id_empresa)) $submodel->id_empresa = $value->id_empresa;
                    if (isset($value->id_contacto)) $submodel->id_contacto = $value->id_contacto;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->sidebar_collapse)) $submodel->sidebar_collapse = $value->sidebar_collapse;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_empresa = $id_empresa;
                    $submodel->id_contacto = $id_contacto;
                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


    public function getStatusLabel()
    {
        if ($this->_statusLabel === null) {
            $statuses = self::getArrayStatus();
            $this->_statusLabel = $statuses[$this->status];
        }
        return $this->_statusLabel;
    }

    /**
     * @inheritdoc
     */
    public static function getArrayStatus()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app/user_add', 'STATUS_ACTIVE'),
            self::STATUS_INACTIVE => Yii::t('app/user_add', 'STATUS_INACTIVE'),
            // self::STATUS_DELETED => Yii::t('app/user_add', 'STATUS_DELETED'), // using 'baja' field
        ];
    }

    // Must return roles in order (from less important upwards)
    public static function getArrayRole($id_societe, $type = self::ROLES_UNDER)
    {
        // AVTPL models.ModelAdditionalFunctionsUserGetArrayRole BEGIN
        $basicRoles = [];
        if ($type != self::ROLES_NO_BASIC) $basicRoles = [
            's#' . $id_societe . '-client' => Yii::t('app/user_add', 'Client'),
        ];
        $manageRoles = [
            's#' . $id_societe . '-manager' => Yii::t('app/user_add', 'Manager'),
        ];
        $techRoles = [
            's#' . $id_societe . '-technical' => Yii::t('app/user_add', 'Technical'),
        ];
        $setupRoles = [
            's#' . $id_societe . '-admin' => Yii::t('app/user_add', 'Admin')
        ];
        if (Yii::$app->user->can('setup') || $type == self::ROLES_ALL) {
            return ArrayHelper::merge($basicRoles, $manageRoles, $techRoles, $setupRoles);
        } else if (Yii::$app->user->can('manage')) {
            return ArrayHelper::merge($basicRoles, $manageRoles);
        }
        return [];
        // AVTPL models.ModelAdditionalFunctionsUserGetArrayRole END
    }

    public function getRoleLabel()
    {
        if ($this->_roleLabel === null) {
            $roles = self::getArrayRole($this->id_societe);
            $this->_roleLabel = $roles[$this->role];
        }
        return $this->_roleLabel;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
                $this->generatePasswordResetToken();
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $id user_id from audit_entry table
     * @return mixed|string
     */
    public static function userIdentifierCallback($id)
    {
        $user = self::findOne($id);
        return $user ? $user->username . ' (' . $id . ')' : $id;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE, 'baja' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $result = static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE, 'baja' => 0]);
        if (!is_null($result) && $result->id_empresa) {
            $empresa = \app\models\Empresa::findOne($result->id_empresa);
            if (is_null($empresa) || $empresa->baja) $result = null;
        }
        return $result;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $result = static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'baja' => 0]);
        if (!is_null($result) && $result->id_empresa) {
            $empresa = \app\models\Empresa::findOne($result->id_empresa);
            if (is_null($empresa) || $empresa->baja) $result = null;
        }
        return $result;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        $result = static::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE, 'baja' => 0]);
        if (!is_null($result) && $result->id_empresa) {
            $empresa = \app\models\Empresa::findOne($result->id_empresa);
            if (is_null($empresa) || $empresa->baja) $result = null;
        }
        return $result;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContacto()
    {
        return $this->hasOne(Contacto::className(), ['id' => 'id_contacto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}