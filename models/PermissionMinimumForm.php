<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;

class PermissionMinimumForm extends Model
{
    public $name;
    public $desc;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'role'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'desc' => Yii::t('app/permissions', 'Permission'),
            'role' => Yii::t('app/permissions', 'Minimum Role with Access'),
        ];
    }
}