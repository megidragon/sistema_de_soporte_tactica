<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SocieteParams;
use app\models\User;

/**
 * SocieteParamsSearch represents the model behind the search form about `app\models\SocieteParams`.
 */
class SocieteParamsSearch extends SocieteParams
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
			[['name'], 'safe'],
			[['value'], 'safe'],
			[['baja'], 'safe'],
            [['desc_es'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $baja, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $this->baja = $baja;
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = SocieteParams::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;
        $dataParams['sort'] = (Yii::$app->user->can('setup')) ? ['defaultOrder' => ['name'=>SORT_ASC]] : ['defaultOrder' => ['desc_es'=>SORT_ASC]];
        $dataProvider = new ActiveDataProvider($dataParams);


        $query->andFilterWhere([
			'societe_params.id_societe' => $societe,
			'societe_params.baja' => $this->baja,
            
            
        ]);
        if (!Yii::$app->user->can('setup')) {
            $query->andFilterWhere(['only_admin' => 0]);
        }
        $query->andFilterWhere(['like', 'societe_params.desc_es', $this->desc_es]);

        

        $query->andFilterWhere(['like', 'societe_params.name', $this->name])
->andFilterWhere(['like', 'societe_params.value', $this->value])
;

        return $dataProvider;
    }

	public function quickSearch($params, $text, $baja = -1, $pageSize = 20) {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = SocieteParams::find();
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);
		
		if ($baja >= 0) {
			$query->andFilterWhere([
    			'societe_params.baja' => $baja,
			]);
		}
        
        $query->andFilterWhere([
			'societe_params.id_societe' => $societe,
            
            
        ]);
        if (!Yii::$app->user->can('setup')) {
            $query->andFilterWhere(['only_admin' => 0]);
        }
        $query->andFilterWhere(['like', 'societe_params.desc_es', $this->desc_es]);

        $query->andFilterWhere(['like', 'societe_params.name', $this->name])
->andFilterWhere(['like', 'societe_params.value', $this->value])
;

		
        return $dataProvider;
	}

    private static function explodeRangeAsDateYMD($dateRangeDMY) {
        list($start_date, $end_date) = explode(' - ', $dateRangeDMY);
        return [substr($start_date, 6, 4) . '-' . substr($start_date, 3, 2) . '-' . substr($start_date, 0, 2), substr($end_date, 6, 4) . '-' . substr($end_date, 3, 2) . '-' . substr($end_date, 0, 2)];
    }

}
