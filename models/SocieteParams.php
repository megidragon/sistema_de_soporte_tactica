<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


/**
 * This is the model class for table "societe_params".
 *
 * @property number $id
 * @property string $name
 * @property string $value
 * @property string $type
 * @property number $only_admin
 * @property string $desc_es
 * @property number $id_societe
 * @property number $baja
 *
 * @property Societe $idSociete
 */
class SocieteParams extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'societe_params';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['name'], 'string', 'max' => 250],
			[['value'], 'string', 'max' => 32768],
			[['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/societe_params', 'Id'),
			'name' => Yii::t('app/societe_params', 'Name'),
			'value' => Yii::t('app/societe_params', 'Value'),
			'type' => Yii::t('app/societe_params', 'Type'),
			'only_admin' => Yii::t('app/societe_params', 'Only Admin'),
			'desc_es' => Yii::t('app/societe_params', 'Desc Es'),
			'id_societe' => Yii::t('app/societe_params', 'Id Societe'),
			'baja' => Yii::t('app/societe_params', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		return $fields;
	}

    public function description() {
        return $this->descripcion;
    }

    public static function updateFromJsonString($string, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new SocieteParams();
                    if (isset($value->id)) {
                        $submodel = SocieteParams::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->name)) $submodel->name = $value->name;
                    if (isset($value->value)) $submodel->value = $value->value;
                    if (isset($value->type)) $submodel->type = $value->type;
                    if (isset($value->only_admin)) $submodel->only_admin = $value->only_admin;
                    if (isset($value->desc_es)) $submodel->desc_es = $value->desc_es;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}