<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 6/6/2018
 * Time: 19:17
 */

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use \bedezign\yii2\audit\models\AuditTrail;

class TicketUpload extends ActiveRecord
{
    const PATH  = "uploads/ticket/";

    public static function tableName()
    {
        return 'ticket_uploads';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /**
     * get trails for this model
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }



    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['id_ticket'], 'safe'],
            [['upload'], 'string', 'max' => 200],

        ];
        /*return [
            [['id'], 'safe'],
            [['id_ticket'], 'safe'],
            [['file'], 'file',
                'skipOnEmpty' => false,
                'uploadRequired' => 'No se ha seleccionado ningun archivo',
                'maxSize'   => 1024*1024*1,
                'tooBig'    => 'El tamaño maximo permitido es de 1MB',
                'minSize'   => 10,
                'tooSmall'  => 'El tamaño minimo es de 10 bytes',
                'extensiones'   => 'doc, pdf, docx, txt',
                'wrongExtension'    => 'El archivo {file} no contiene una extensión permitida',
                'maxFiles'          => 4,
                'tooMany'           => 'La cantidad máxima de archivos a subir es de {limit}'
            ]

        ];*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/ticketUpload', 'Id'),
            'id_ticket' => Yii::t('app/ticketUpload', 'Id ticket'),
            'upload' => Yii::t('app/ticketUpload', 'Upload'),

        ];
    }
}