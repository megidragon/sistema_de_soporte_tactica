<?php // AVTPL
/**
 * This model allows for storing of mail entries linked to a specific audit entry
 */

namespace app\models;

use bedezign\yii2\audit\components\db\ActiveRecord;
use Yii;

/**
 * AuditMail
 *
 * @package bedezign\yii2\audit\models
 * @property int    $id
 * @property int    $entry_id
 * @property string $created
 * @property int    $successful
 * @property string $from
 * @property string $to
 * @property string $reply
 * @property string $cc
 * @property string $bcc
 * @property string $subject
 * @property string $text
 * @property string $html
 * @property string $data
 *
 * @property AuditEntry    $entry
 */
class AuditMail extends ActiveRecord
{
    protected $serializeAttributes = ['text', 'html', 'data'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_mail';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntry()
    {
        return $this->hasOne(AuditEntry::className(), ['id' => 'entry_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/mail', 'ID'),
            'entry_id' => Yii::t('app/mail', 'Entry ID'),
            'created' => Yii::t('app/mail', 'Created'),
            'successful' => Yii::t('app/mail', 'Successful'),
            'from' => Yii::t('app/mail', 'From'),
            'to' => Yii::t('app/mail', 'To'),
            'reply' => Yii::t('app/mail', 'Reply'),
            'cc' => Yii::t('app/mail', 'CC'),
            'bcc' => Yii::t('app/mail', 'BCC'),
            'subject' => Yii::t('app/mail', 'Subject'),
            'text' => Yii::t('app/mail', 'Text Body'),
            'html' => Yii::t('app/mail', 'HTML Body'),
            'data' => Yii::t('app/mail', 'Data'),
        ];
    }

}