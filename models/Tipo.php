<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


/**
 * This is the model class for table "tipo".
 *
 * @property number $id
 * @property string $descripcion
 * @property number $id_societe
 * @property number $baja
 *
 * @property Societe $idSociete
 */
class Tipo extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'tipo';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['descripcion'], 'string', 'max' => 500],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/tipo', 'Id'),
			'descripcion' => Yii::t('app/tipo', 'Descripcion'),
			'id_societe' => Yii::t('app/tipo', 'Id Societe'),
			'baja' => Yii::t('app/tipo', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		return $fields;
	}

    public function description() {
        return $this->descripcion;
    }

    public static function updateFromJsonString($string, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Tipo();
                    if (isset($value->id)) {
                        $submodel = Tipo::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->descripcion)) $submodel->descripcion = $value->descripcion;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}