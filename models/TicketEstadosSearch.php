<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 6/6/2018
 * Time: 19:36
 */

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;


class TicketEstadosSearch extends TicketEstado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['id'], 'safe'],
            [['id_baja'], 'safe'],
            [['descripcion'], 'safe']

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
//        $user   = User::findIdentity(Yii::$app->user->getId());
//        $societe = $user->id_societe;
        $query = TicketEstado::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;

        $dataProvider = new ActiveDataProvider($dataParams);

        return $dataProvider;
    }
}