<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;

class PermissionSwitchForm extends Model
{
    public $name;
    public $desc;
    public $ison;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['ison'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'desc' => Yii::t('app/permissions', 'Permission'),
            'ison' => Yii::t('app/permissions', 'Enabled'),
        ];
    }
}