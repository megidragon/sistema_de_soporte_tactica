<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ticket;
use app\models\User;

/**
 * TicketSearch represents the model behind the search form about `app\models\Ticket`.
 */
class TicketSearch extends Ticket
{
    public $fechaCreacionRange;
    public $fechaVencimientoRange;
    public $fechaTacticaModificacionRange;
    public $fechaTacticaCierreRange;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
			[['id_empresa'], 'safe'],
			[['id_contacto'], 'safe'],
			[['baja'], 'safe'],
            [['id_producto', 'tipo', 'prioridad'], 'safe'],
            [['fechaCreacionRange', 'fechaVencimientoRange', 'fechaTacticaModificacionRange', 'fechaTacticaCierreRange'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
    	$user   = User::findIdentity(Yii::$app->user->getId());
        $societe = $user->id_societe;
        $query = Ticket::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;
        
        $dataProvider = new ActiveDataProvider($dataParams);

        if (!is_null($this->fechaCreacionRange) && strpos($this->fechaCreacionRange, ' - ') !== false ) {
            list($start_date, $end_date) = self::explodeRangeAsDateYMD($this->fechaCreacionRange);
            $query->andFilterWhere(['between', 'fecha_creacion', $start_date, $end_date]);
        }

        if (!is_null($this->fechaVencimientoRange) && strpos($this->fechaVencimientoRange, ' - ') !== false ) {
            list($start_date, $end_date) = self::explodeRangeAsDateYMD($this->fechaVencimientoRange);
            $query->andFilterWhere(['between', 'fecha_vencimiento', $start_date, $end_date]);
        }

        //AGREGADO PARA EL NUEVO CAMPO DE FECHA CIERRE TACTICA
        if (!is_null($this->fechaTacticaModificacionRange) && strpos($this->fechaTacticaModificacionRange, ' - ') !== false ) {
            list($start_date, $end_date) = self::explodeRangeAsDateYMD($this->fechaTacticaModificacionRange);
            $query->andFilterWhere(['between', 'tactica_fecha_modificacion', $start_date, $end_date]);
        }

        //AGREGADO PARA EL NUEVO CAMPO DE FECHA CIERRE TACTICA
        if (!is_null($this->fechaTacticaCierreRange) && strpos($this->fechaTacticaCierreRange, ' - ') !== false ) {
            list($start_date, $end_date) = self::explodeRangeAsDateYMD($this->fechaTacticaCierreRange);
            $query->andFilterWhere(['between', 'tactica_fecha_cierre', $start_date, $end_date]);
        }

		$query->joinWith('idEmpresa idEmpresa');
		$query->joinWith('idContacto idContacto');
		$query->joinWith('idTipo idTipo');
		$query->joinWith('idSubtipo idSubtipo');
		$query->joinWith('idElemento idElemento');
		$query->joinWith('idProducto idProducto');

		if($user->id_empresa != null){
			$query->andFilterWhere([
				'ticket.id_societe' => $societe,
				'ticket.baja' => $this->baja,
				'ticket.id_tipo' => $this->id_tipo,
				'ticket.id_subtipo' => $this->id_subtipo,
				'ticket.id_elemento' => $this->id_elemento,
				'ticket.id_producto' => $this->id_producto,
				'ticket.id_empresa' => $this->id_empresa,
				'ticket.id_contacto' => $this->id_contacto,
				'ticket.tipo' => $this->tipo,
				'ticket.prioridad' => $this->prioridad,
				'ticket.id_empresa' => $user->id_empresa,
			]);
		} else {
			$query->andFilterWhere([
				'ticket.id_societe' => $societe,
				'ticket.baja' => $this->baja,
				'ticket.id_tipo' => $this->id_tipo,
				'ticket.id_subtipo' => $this->id_subtipo,
				'ticket.id_elemento' => $this->id_elemento,
				'ticket.id_producto' => $this->id_producto,
				'ticket.id_empresa' => $this->id_empresa,
				'ticket.id_contacto' => $this->id_contacto,
				'ticket.tipo' => $this->tipo,
				'ticket.prioridad' => $this->prioridad
			]);
		}

	        /*->orWhere([
	        'idProducto.id_empresa' => null
        ]);*/


        $query->andFilterWhere(['like', 'ticket.tipo', $this->tipo])
				->andFilterWhere(['like', 'ticket.descripcion', $this->descripcion])
//				->andFilterWhere(['like', 'ticket.tactica_user_modificacion', $this->tactica_user_modificacion])
//				->andFilterWhere(['like', 'ticket.tactica_user_cierre', $this->tactica_user_cierre])
				->andFilterWhere(['like', 'ticket.tactica_solucion_descr', $this->tactica_solucion_descr]);

        return $dataProvider;
    }

	public function quickSearch($params, $text, $baja = -1, $pageSize = 20) {
        $societe = User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = Ticket::find();
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);
		
		if ($baja >= 0) {
			$query->andFilterWhere([
    			'ticket.baja' => $baja,
			]);
		}
        
        $query->andFilterWhere([
			'ticket.id_societe' => $societe,
            
            'ticket.id_tipo' => $this->id_tipo,

            'ticket.id_subtipo' => $this->id_subtipo,

            'ticket.id_elemento' => $this->id_elemento,

            'ticket.id_producto' => $this->id_producto,

            
            'ticket.id_empresa' => $this->id_empresa,

            'ticket.id_contacto' => $this->id_contacto,

        ]);


        $query->andFilterWhere(['like', 'ticket.tipo', $this->tipo])
->andFilterWhere(['like', 'ticket.descripcion', $this->descripcion])
;

		
        return $dataProvider;
	}

    private static function explodeRangeAsDateYMD($dateRangeDMY) {
        list($start_date, $end_date) = explode(' - ', $dateRangeDMY);
        return [substr($start_date, 6, 4) . '-' . substr($start_date, 3, 2) . '-' . substr($start_date, 0, 2), substr($end_date, 6, 4) . '-' . substr($end_date, 3, 2) . '-' . substr($end_date, 0, 2)];
    }

}
