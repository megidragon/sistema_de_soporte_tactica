<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producto;
use app\models\User;

/**
 * ProductoSearch represents the model behind the search form about `app\models\Producto`.
 */
class ProductoSearch extends Producto
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
			[['id_empresa'], 'safe'],
			[['codigo'], 'safe'],
			[['nro_serie'], 'safe'],
			[['nombre_sector'], 'safe'],
			[['id_prioridad'], 'safe'],
			[['baja'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $baja, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $this->baja = $baja;
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = Producto::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;
        
        $dataProvider = new ActiveDataProvider($dataParams);

		$query->joinWith('idEmpresa idEmpresa');
		$query->joinWith('idPrioridad idPrioridad');

        $query->andFilterWhere([
			'producto.id_societe' => $societe,
			'producto.baja' => $this->baja,
            
            
            
            
            
            'producto.id_empresa' => $this->id_empresa,

            
            
            
            'producto.id_prioridad' => $this->id_prioridad,

        ]);


        

        $query->andFilterWhere(['like', 'producto.datos_red', $this->datos_red])
->andFilterWhere(['like', 'producto.mac_address', $this->mac_address])
->andFilterWhere(['like', 'producto.fabricante', $this->fabricante])
->andFilterWhere(['like', 'producto.posicion', $this->posicion])
->andFilterWhere(['like', 'producto.tacticaid', $this->tacticaid])
->andFilterWhere(['like', 'producto.codigo', $this->codigo])
->andFilterWhere(['like', 'producto.nro_serie', $this->nro_serie])
->andFilterWhere(['like', 'producto.nombre_sector', $this->nombre_sector])
;

        return $dataProvider;
    }

	public function quickSearch($params, $text, $baja = -1, $pageSize = 20) {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = Producto::find();
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);
		
		if ($baja >= 0) {
			$query->andFilterWhere([
    			'producto.baja' => $baja,
			]);
		}
        
        $query->andFilterWhere([
			'producto.id_societe' => $societe,
            
            
            
            
            
            'producto.id_empresa' => $this->id_empresa,

            
            
            
            'producto.id_prioridad' => $this->id_prioridad,

        ]);


        $query->andFilterWhere(['like', 'producto.datos_red', $this->datos_red])
->andFilterWhere(['like', 'producto.mac_address', $this->mac_address])
->andFilterWhere(['like', 'producto.fabricante', $this->fabricante])
->andFilterWhere(['like', 'producto.posicion', $this->posicion])
->andFilterWhere(['like', 'producto.tacticaid', $this->tacticaid])
->andFilterWhere(['like', 'producto.codigo', $this->codigo])
->andFilterWhere(['like', 'producto.nro_serie', $this->nro_serie])
->andFilterWhere(['like', 'producto.nombre_sector', $this->nombre_sector])
;

		
        return $dataProvider;
	}

    private static function explodeRangeAsDateYMD($dateRangeDMY) {
        list($start_date, $end_date) = explode(' - ', $dateRangeDMY);
        return [substr($start_date, 6, 4) . '-' . substr($start_date, 3, 2) . '-' . substr($start_date, 0, 2), substr($end_date, 6, 4) . '-' . substr($end_date, 3, 2) . '-' . substr($end_date, 0, 2)];
    }

}
