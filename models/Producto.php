<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


/**
 * This is the model class for table "producto".
 *
 * @property number $id
 * @property string $tacticaid
 * @property string $codigo
 * @property string $nro_serie
 * @property string $nombre_sector
 * @property string $datos_red
 * @property string $mac_address
 * @property string $fabricante
 * @property string $posicion
 * @property number $id_prioridad
 * @property number $id_empresa
 * @property number $id_societe
 * @property number $baja
 *
 * @property Empresa $idEmpresa
 * @property Prioridad $idPrioridad
 * @property Societe $idSociete
 */
class Producto extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'producto';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['datos_red'], 'string', 'max' => 500],
			[['mac_address'], 'string', 'max' => 500],
			[['fabricante'], 'string', 'max' => 500],
			[['posicion'], 'string', 'max' => 500],
			[['tacticaid'], 'string', 'max' => 20],
			[['id_empresa'], 'safe'],
			[['codigo'], 'string', 'max' => 500],
			[['nro_serie'], 'string', 'max' => 500],
			[['nombre_sector'], 'string', 'max' => 500],
			[['id_prioridad'], 'safe'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/producto', 'Id'),
			'tacticaid' => Yii::t('app/producto', 'Tacticaid'),
			'codigo' => Yii::t('app/producto', 'Codigo'),
			'nro_serie' => Yii::t('app/producto', 'Nro Serie'),
			'nombre_sector' => Yii::t('app/producto', 'Nombre Sector'),
			'datos_red' => Yii::t('app/producto', 'Datos Red'),
			'mac_address' => Yii::t('app/producto', 'Mac Address'),
			'fabricante' => Yii::t('app/producto', 'Fabricante'),
			'posicion' => Yii::t('app/producto', 'Posicion'),
			'id_prioridad' => Yii::t('app/producto', 'Id Prioridad'),
			'id_empresa' => Yii::t('app/producto', 'Id Empresa'),
			'id_societe' => Yii::t('app/producto', 'Id Societe'),
			'baja' => Yii::t('app/producto', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idEmpresa';
		$fields[] = 'idPrioridad';
		return $fields;
	}

    public function description() {
        return $this->codigo . ' - ' . $this->nombre_sector;
    }

    public static function updateFromJsonString($string, $id_empresa, $id_prioridad, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Producto();
                    if (isset($value->id)) {
                        $submodel = Producto::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->tacticaid)) $submodel->tacticaid = $value->tacticaid;
                    if (isset($value->codigo)) $submodel->codigo = $value->codigo;
                    if (isset($value->nro_serie)) $submodel->nro_serie = $value->nro_serie;
                    if (isset($value->nombre_sector)) $submodel->nombre_sector = $value->nombre_sector;
                    if (isset($value->datos_red)) $submodel->datos_red = $value->datos_red;
                    if (isset($value->mac_address)) $submodel->mac_address = $value->mac_address;
                    if (isset($value->fabricante)) $submodel->fabricante = $value->fabricante;
                    if (isset($value->posicion)) $submodel->posicion = $value->posicion;
                    if (isset($value->id_prioridad)) $submodel->id_prioridad = $value->id_prioridad;
                    if (isset($value->id_empresa)) $submodel->id_empresa = $value->id_empresa;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_empresa = $id_empresa;
                    $submodel->id_prioridad = $id_prioridad;
                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPrioridad()
    {
        return $this->hasOne(Prioridad::className(), ['id' => 'id_prioridad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}