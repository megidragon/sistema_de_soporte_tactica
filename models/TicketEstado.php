<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 6/6/2018
 * Time: 19:17
 */

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use \bedezign\yii2\audit\models\AuditTrail;

class TicketEstado extends ActiveRecord
{
    public static function tableName()
    {
        return 'ticket_estados';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /**
     * get trails for this model
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }



    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['id_baja'], 'safe'],
            [['descripcion'], 'string', 'max' => 500],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/ticketEstado', 'Id'),
            'id_baja' => Yii::t('app/ticketEstado', 'Id baja'),
            'descripcion' => Yii::t('app/ticketEstado', 'Descripcion'),

        ];
    }
}