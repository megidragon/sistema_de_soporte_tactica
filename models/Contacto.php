<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "contacto".
 *
 * @property number $id
 * @property string $tacticaid
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property number $supervisor_tecnico
 * @property number $supervisor_administ
 * @property number $id_empresa
 * @property number $id_societe
 * @property number $baja
 *
 * @property Empresa $idEmpresa
 * @property Societe $idSociete
 */
class Contacto extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $new_id;



    public static function tableName()
    {
        return 'contacto';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['id_empresa'], 'safe'],
			[['nombre'], 'string', 'max' => 500],
			[['apellido'], 'string', 'max' => 500],
			[['email'], 'string', 'max' => 500],
			[['supervisor_tecnico'], 'safe'],
			[['supervisor_administ'], 'safe'],
			[['tacticaid'], 'string', 'max' => 20],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/contacto', 'Id'),
			'tacticaid' => Yii::t('app/contacto', 'Tacticaid'),
			'nombre' => Yii::t('app/contacto', 'Nombre'),
			'apellido' => Yii::t('app/contacto', 'Apellido'),
			'email' => Yii::t('app/contacto', 'Email'),
			'supervisor_tecnico' => Yii::t('app/contacto', 'Supervisor Tecnico'),
			'supervisor_administ' => Yii::t('app/contacto', 'Supervisor Administ'),
			'id_empresa' => Yii::t('app/contacto', 'Id Empresa'),
			'id_societe' => Yii::t('app/contacto', 'Id Societe'),
			'baja' => Yii::t('app/contacto', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idEmpresa';
		return $fields;
	}

    public function description() {
        return $this->nombre . ' ' . $this->apellido;
    }

    public static function updateFromJsonString($string, $id_empresa, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Contacto();
                    if (isset($value->id)) {
                        $submodel = Contacto::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->tacticaid)) $submodel->tacticaid = $value->tacticaid;
                    if (isset($value->nombre)) $submodel->nombre = $value->nombre;
                    if (isset($value->apellido)) $submodel->apellido = $value->apellido;
                    if (isset($value->email)) $submodel->email = $value->email;
                    if (isset($value->supervisor_tecnico)) $submodel->supervisor_tecnico = $value->supervisor_tecnico;
                    if (isset($value->supervisor_administ)) $submodel->supervisor_administ = $value->supervisor_administ;
                    if (isset($value->id_empresa)) $submodel->id_empresa = $value->id_empresa;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_empresa = $id_empresa;
                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'baja' => 0]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}