<?php // AVTPL
namespace app\models;

use app\models\User;
use yii\base\Model;
use app\models\Societe;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $username;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [['username', 'email'], 'exist', 
                'targetAttribute' => ['username', 'email'],
                'targetClass' => '\app\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE, 'baja' => 0],
            ],
            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/'],
            ['username', 'string', 'min' => 3, 'max' => 30],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'baja' => 0,
            'username' => $this->username,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            $societe = Societe::findOne($user->id_societe);

            $user->setScenario('update-reset_token');
            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => $societe->name])
                    ->setTo($this->email)
                    ->setReplyTo($societe->email)
                    ->setSubject('Reinicio de password para ' . $societe->name)
                    ->send();
            }
        }

        return false;
    }
}
