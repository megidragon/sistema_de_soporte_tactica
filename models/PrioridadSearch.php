<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prioridad;
use app\models\User;

/**
 * PrioridadSearch represents the model behind the search form about `app\models\Prioridad`.
 */
class PrioridadSearch extends Prioridad
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
			[['codigo'], 'safe'],
			[['unidad'], 'safe'],
			[['cantidad'], 'number'],
			[['baja'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $baja, $pageSize = 20)
    {
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $this->baja = $baja;
        return $this->searchMin($pageSize);
    }


    public function searchMin($pageSize = 20)
    {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = Prioridad::find();

        $dataParams = ['query' => $query];
        $dataParams['pagination'] = ($pageSize > 0) ? ['pageSize' => $pageSize] : false;
        
        $dataProvider = new ActiveDataProvider($dataParams);


        $query->andFilterWhere([
			'prioridad.id_societe' => $societe,
			'prioridad.baja' => $this->baja,
            
            
            'prioridad.cantidad' => $this->cantidad,

        ]);


        

        $query->andFilterWhere(['like', 'prioridad.codigo', $this->codigo])
->andFilterWhere(['like', 'prioridad.unidad', $this->unidad])
;

        return $dataProvider;
    }

	public function quickSearch($params, $text, $baja = -1, $pageSize = 20) {
        $societe = \app\models\User::findIdentity(Yii::$app->user->getId())->id_societe;
        $query = Prioridad::find();
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
        ]);
		
		if ($baja >= 0) {
			$query->andFilterWhere([
    			'prioridad.baja' => $baja,
			]);
		}
        
        $query->andFilterWhere([
			'prioridad.id_societe' => $societe,
            
            
            'prioridad.cantidad' => $this->cantidad,

        ]);


        $query->andFilterWhere(['like', 'prioridad.codigo', $this->codigo])
->andFilterWhere(['like', 'prioridad.unidad', $this->unidad])
;

		
        return $dataProvider;
	}

    private static function explodeRangeAsDateYMD($dateRangeDMY) {
        list($start_date, $end_date) = explode(' - ', $dateRangeDMY);
        return [substr($start_date, 6, 4) . '-' . substr($start_date, 3, 2) . '-' . substr($start_date, 0, 2), substr($end_date, 6, 4) . '-' . substr($end_date, 3, 2) . '-' . substr($end_date, 0, 2)];
    }

}
