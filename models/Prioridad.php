<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


/**
 * This is the model class for table "prioridad".
 *
 * @property number $id
 * @property string $codigo
 * @property string $unidad
 * @property number $cantidad
 * @property number $id_societe
 * @property number $baja
 *
 * @property Societe $idSociete
 */
class Prioridad extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'prioridad';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
			[['codigo'], 'string', 'max' => 500],
			[['unidad'], 'string', 'max' => 20],
			[['cantidad'], 'number'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/prioridad', 'Id'),
			'codigo' => Yii::t('app/prioridad', 'Codigo'),
			'unidad' => Yii::t('app/prioridad', 'Unidad'),
			'cantidad' => Yii::t('app/prioridad', 'Cantidad'),
			'id_societe' => Yii::t('app/prioridad', 'Id Societe'),
			'baja' => Yii::t('app/prioridad', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		return $fields;
	}

    public function description() {
        return $this->codigo;
    }

    public static function updateFromJsonString($string, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Prioridad();
                    if (isset($value->id)) {
                        $submodel = Prioridad::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->codigo)) $submodel->codigo = $value->codigo;
                    if (isset($value->unidad)) $submodel->unidad = $value->unidad;
                    if (isset($value->cantidad)) $submodel->cantidad = $value->cantidad;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}