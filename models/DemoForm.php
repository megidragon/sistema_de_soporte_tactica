<?php // AVTPL

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class DemoForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $agency;
    public $address;
    public $webpage;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, phone, agency and address are required
            [['name', 'email', 'phone', 'agency', 'address'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // webpage has to be a valid url
            ['webpage', 'url'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'name' => 'Nombre',
			'phone' => 'Teléfono',
			'email' => 'Email',
            'agency' => 'Empresa',
            'address' => 'Dirección',
            'webpage' => 'Página Web',
            'verifyCode' => 'Verificación',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom(Yii::$app->params['notifEmail'])
            ->setSubject("Solicitud de DEMO")
            ->setTextBody("Nombre: " . $this->name .
					"\nTeléfono: " . $this->phone .
					"\nEmail: " . $this->email .
                    "\nAgencia: " . $this->agency .
                    "\nDirección: " . $this->address .
                    "\nWeb: " . $this->webpage)
            ->send();
    }
}
