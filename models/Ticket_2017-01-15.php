<?php // AVTPL

namespace app\models;

use Yii;
use \bedezign\yii2\audit\models\AuditTrail;


class Ticket extends \yii\db\ActiveRecord
{
    public $new_id;



    public static function tableName()
    {
        return 'ticket';
    }

    public function behaviors()
    {
        if (PHP_SAPI !== 'cli') {
            return [
                'bedezign\yii2\audit\AuditTrailBehavior',
            ];
        } else {
            return [
            ];
        }
    }

    /** 
     * get trails for this model 
     */
    public function getAuditTrails()
    {
        return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
            ->andOnCondition(['model' => get_class($this)]);
    }

    

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = NULL) {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result) {
            Yii::error("Error saving: " . \yii\helpers\VarDumper::dumpAsString($this), 'app');
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id'], 'safe'],
            [['tipo'], 'string', 'max' => 20],
            [['prioridad'], 'string', 'max' => 20],
			[['id_tipo'], 'safe'],
			[['id_subtipo'], 'safe'],
			[['id_elemento'], 'safe'],
			[['id_producto'], 'safe'],
			[['descripcion'], 'string', 'max' => 32768],
			[['id_empresa'], 'safe'],
			[['id_contacto'], 'safe'],
            [['fecha_vencimiento', 'hora_vencimiento'], 'safe'],
			[['tipo', 'prioridad', 'id_tipo', 'id_subtipo', 'id_elemento', 'id_producto', 'descripcion'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app/ticket', 'Id'),
			'id_empresa' => Yii::t('app/ticket', 'Id Empresa'),
			'id_contacto' => Yii::t('app/ticket', 'Id Contacto'),
            'tipo' => Yii::t('app/ticket', 'Tipo'),
			'id_tipo' => Yii::t('app/ticket', 'Id Tipo'),
			'id_subtipo' => Yii::t('app/ticket', 'Id Subtipo'),
			'id_elemento' => Yii::t('app/ticket', 'Id Elemento'),
			'id_producto' => Yii::t('app/ticket', 'Id Producto'),
            'descripcion' => Yii::t('app/ticket', 'Descripcion'),
            'nro_serie' => Yii::t('app/ticket', 'Serial Number'),
            'fabricante' => Yii::t('app/ticket', 'Manufacturer'),
            'datos_red' => Yii::t('app/ticket', 'Network Data'),
            'nombre_sector' => Yii::t('app/ticket', 'Sector Name'),
            'posicion' => Yii::t('app/ticket', 'Position'),
            'mac_address' => Yii::t('app/ticket', 'MAC Address'),
            'prioridad' => Yii::t('app/ticket', 'Ticket Priority'),
            'prioridad_producto' => Yii::t('app/ticket', 'Product Priority'),
            'fecha_vencimiento' => Yii::t('app/ticket', 'Due Date'),
			'id_societe' => Yii::t('app/ticket', 'Id Societe'),
			'baja' => Yii::t('app/ticket', 'Baja')
			
			];
    }

	public function fields()
	{
		$fields = parent::fields();
		$fields[] = 'idEmpresa';
		$fields[] = 'idContacto';
		$fields[] = 'idTipo';
		$fields[] = 'idSubtipo';
		$fields[] = 'idElemento';
		$fields[] = 'idProducto';
		return $fields;
	}

    public function description() {
        return $this->descripcion;
    }

    public static function updateFromJsonString($string, $id_empresa, $id_contacto, $id_tipo, $id_subtipo, $id_elemento, $id_producto, $id_societe) {
        $array = json_decode($string);
        if ($array != null) {
            foreach ($array as $key => $value) {
                if (isset($value->_update) && $value->_update == 1) {
                    $submodel = new Ticket();
                    if (isset($value->id)) {
                        $submodel = Ticket::findOne($value->id);
                    }
                    if (isset($value->id)) $submodel->id = $value->id;
                    if (isset($value->id_empresa)) $submodel->id_empresa = $value->id_empresa;
                    if (isset($value->id_contacto)) $submodel->id_contacto = $value->id_contacto;
                    if (isset($value->tipo)) $submodel->tipo = $value->tipo;
                    if (isset($value->id_tipo)) $submodel->id_tipo = $value->id_tipo;
                    if (isset($value->id_subtipo)) $submodel->id_subtipo = $value->id_subtipo;
                    if (isset($value->id_elemento)) $submodel->id_elemento = $value->id_elemento;
                    if (isset($value->id_producto)) $submodel->id_producto = $value->id_producto;
                    if (isset($value->descripcion)) $submodel->descripcion = $value->descripcion;
                    if (isset($value->id_societe)) $submodel->id_societe = $value->id_societe;
                    if (isset($value->baja)) $submodel->baja = $value->baja;

                    $submodel->id_empresa = $id_empresa;
                    $submodel->id_contacto = $id_contacto;
                    $submodel->id_tipo = $id_tipo;
                    $submodel->id_subtipo = $id_subtipo;
                    $submodel->id_elemento = $id_elemento;
                    $submodel->id_producto = $id_producto;
                    $submodel->id_societe = $id_societe;

                    $submodel->save();
                }
            }
        }
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContacto()
    {
        return $this->hasOne(Contacto::className(), ['id' => 'id_contacto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'id_tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubtipo()
    {
        return $this->hasOne(Subtipo::className(), ['id' => 'id_subtipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElemento()
    {
        return $this->hasOne(Elemento::className(), ['id' => 'id_elemento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'id_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSociete()
    {
        return $this->hasOne(Societe::className(), ['id' => 'id_societe']);
    }




}