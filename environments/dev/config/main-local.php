<?php // AVTPL
return [
    // AVTPL config.ConfigMainLocalDev BEGIN
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=lassa',
            'username' => 'lassadb',
            'password' => 'lassadb',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
    // AVTPL config.ConfigMainLocalDev END
];
