<?php // AVTPL

// Changes to allow access to frontend without web (http://www.yiiframework.com/wiki/755/how-to-hide-frontend-web-in-url-addresses-on-apache/)
use \yii\web\Request;
$baseUrl = str_replace('web', '', (new Request)->getBaseUrl());

$config = [
    'timeZone' => 'America/Argentina/Buenos_Aires',
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => []
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
