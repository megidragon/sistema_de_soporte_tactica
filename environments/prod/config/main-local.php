<?php // AVTPL
return [
    // AVTPL config.ConfigMainLocalProd BEGIN
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=lassa',
            'username' => 'root',
            'password' => 'arielmysql',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.kaizen2b.net',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'lassa@kaizen2b.net',
                'password' => 'LAsol201616',
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
    ],
    // AVTPL config.ConfigMainLocalProd END
];
