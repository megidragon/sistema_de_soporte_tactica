<?php // AVTPL
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset', // agregado para que incluya los js de bootstrap incluso cuando no agrego ningún widget (ver comentario de kartik-v en https://github.com/yiisoft/yii2/issues/4145)
		'rmrevin\yii\fontawesome\AssetBundle'
    ];

    function __construct() {
        $this->css = [
            'css/custom.css?v=' . Yii::$app->params['jsversion'],
            'css/ramallo.css?v=' . Yii::$app->params['jsversion'],
            // AVTPL EVO INI LandingCSS
            // AVTPL EVO END LandingCSS
        ];
        $this->js = [
            'js/landing/landing.js?v=' . Yii::$app->params['jsversion'],
            // AVTPL EVO INI LandingJS
            // AVTPL EVO END LandingJS
        ];
    }
}
