<?php // AVTPL
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
	//public $sourcePath = '@bower/';
    public $css = [];
    public $js = [];
    public $depends = [
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'kartik\icons\FontAwesomeAsset'
    ];

    function __construct() {
        $this->css = [
            'adminlte/css/AdminLTE.css',
            'adminlte/css/skins/_all-skins.css',
            'css/bootstrap-switch.css',
            'css/custom.css?v=' . Yii::$app->params['jsversion'],
            'css/ramallo.css?v=' . Yii::$app->params['jsversion'],
        ];
        $this->js = [
            'js/bootstrap-switch.js',
            'adminlte/js/app.js',
            'js/jquery.are-you-sure.js',
            'js/ramallo.js?v=' . Yii::$app->params['jsversion'],
        ];
    }
}
