#!/bin/bash

FILENAME=$1

if [ "$FILENAME" == "" ]
then
    echo "Must enter a file name to create patch for"
    exit -1
fi

echo "Creating patch for $FILENAME ..."

find . -type f -name "$FILENAME" | while read EACH
do
    if [ -e "$EACH.patch-ori" ]
    then
        echo "$EACH.patch-ori already exist"
    else
        echo "$EACH => $EACH.patch-ori"
        cp "$EACH" "$EACH.patch-ori"
    fi
done

echo -e "\nEdit file(s)? ([y]/n) \c"
read EDIT
if [ "$EDIT" != "n" ] && [ "$EDIT" != "N" ]
then
    vi `find . -type f -name "$FILENAME"`
fi
