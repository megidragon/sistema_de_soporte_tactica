// AVTPL
function avtplLog(text) {
    $.ajax({
        type: "POST",
        url: $('#baseUrl').data('url') + '/log/log-info',
        data: {
            'text': text,
        }
    });
}
