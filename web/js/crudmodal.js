// AVTPL
$(function(){
	$('.crudModalButton').click(function() {
		$('#crudModal').find('#modalHeader').html($(this).attr('modal-title'));
		$('#crudModal').find('#modalContent').html('<img src="' + $('#baseUrl').data('url') + 'images/spinner_60.gif" class="spinAuto60"/>').load($(this).attr('value'));
		$('#crudModal').modal('show');
	});
});

