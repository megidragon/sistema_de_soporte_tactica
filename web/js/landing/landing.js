// AVTPL
$(document).ready(function() {
    $('.loginButton').click(function() {
        ga('send', 'event', 'button', 'click', 'login');
        $('#loginModal').modal('show')
            .find('#loginModalContent')
            .html('<img src="' + $('#landingLinks').data('images') + '/spinner_60.gif" class="spinAuto60"/>')
            .load($(this).attr('value'));
    });
    $('.contactButton').click(function() {
        ga('send', 'event', 'button', 'click', 'contact');
        $('#contactModal').modal('show')
            .find('#contactModalContent')
            .html('<img src="' + $('#landingLinks').data('images') + '/spinner_60.gif" class="spinAuto60"/>')
            .load($(this).attr('value'));
    });
    $('.demoButton').click(function() {
        ga('send', 'event', 'button', 'click', 'demo');
        $('#demoModal').modal('show')
            .find('#demoModalContent')
            .html('<img src="' + $('#landingLinks').data('images') + '/spinner_60.gif" class="spinAuto60"/>')
            .load($(this).attr('value'));
    });
    var offset = 220;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    $('.back-to-top').click(function(event) {
        ga('send', 'event', 'link', 'click', 'top');
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
});

function showAskResetModal() {
    $('#loginModal').modal('hide');
    $('#askResetModal').modal('show')
        .find('#askResetModalContent')
        .load($('#askResetButton').attr('value'));
}

function showResetModal() {
    $('#resetModal').modal('show')
        .find('#resetModalContent')
        .load($('#resetDiv').attr('data-url'));
}

$(document).ready(function() {
    $('.scrollTo').on('touchstart click', function(e) {
        e.preventDefault();
        var block = $(this).attr('href').split("#")[1];
        ga('send', 'event', 'link', 'click', block);
        $('html,body').animate({
            scrollTop: $('#' + block).offset().top
        }, 1000);
        return;
    });
});
