// AVTPL
function avtplLog(text) {
    $.ajax({
        type: "POST",
        url: $('#baseUrl').data('url') + '/log/log-info',
        data: {
            'text': text,
        }
    });
}

// On 24/08/2016 Export Button started to have ui classes that break its view
function restoreExportButtonStyle() {
    var button = $('.exportButton');
    button.removeClass('ui-button');
    button.removeClass('ui-widget');
    button.removeClass('ui-state-default');
    button.removeClass('ui-corner-all');
    button.removeClass('ui-button-text-only');
    button.removeClass('ui-state-hover');
}