// AVTPL
var submitType;
// Defined also in controller
var SUBMIT_MAIN = 1;
var SUBMIT_DUPLICATE = 2;
var SUBMIT_RECOVER = 3;
var SUBMIT_DELETE = 4;
var SUBMIT_PRINT = 5;

var validateNewTicket   = false;

function crudInit() {
    if (readonly) {
        if (ajax) {
            $('#crudModal :input').prop('disabled', true);
        } else {
            $(':input').prop('disabled', true);
        }
        $('input[name=_csrf]').prop('disabled', false);
        $('.neverReadonly').prop('disabled', false);
        $('.close').prop('disabled', false); // ie: close 'x' buttons on modals
    }
}

function beforeSetSubmitCreate($type) {
	var result = false;
	var id_producto = $('#ticket-id_producto');

	var data = {
		id_producto:    id_producto.val()
	};
	var sendArrayData   = JSON.stringify(data);

	if(id_producto.val() !== ""){
		$.ajax({
			async: true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url: "creating",
			data: "validateBeforeCreate=&array="+sendArrayData,
			beforeSend: function()
			{
			},
			success: function(datos)
			{
				var submit  = true;

				var numInfo = parseInt(datos);
				if(numInfo === 1){
					if(!confirm("Ya existe un ticket con estas caracteristicas. Cargar de todos modos?")){
						submit  = false
					}
				}

				if(submit){
					$('form#crud-form').submit();
				}
			},
			timeout: 50000,
			error: function(e)
			{
			    console.log("Error!");
                console.log(e);
			}
		});

		return false;
	}else{
		$('form#crud-form').submit();
	}

}

function setSubmitType(type, act) {
    submitType = type;
    var result = true;

    validateNewTicket = act || false;

    if(validateNewTicket && submitType === SUBMIT_MAIN){
	    beforeSetSubmitCreate();
	    result = false;
    }

    if (submitType == SUBMIT_DUPLICATE) {
        var message = $('#crudTexts').data('confirm-duplicate');
        result = confirm(message);
    } else if (submitType == SUBMIT_RECOVER) {
        var message = $('#crudTexts').data('confirm-recover');
        result = confirm(message);
    } else if (submitType == SUBMIT_DELETE) {
        var message = $('#crudTexts').data('confirm-delete');
        result = confirm(message);
    } else if (submitType == SUBMIT_PRINT) {
        $('#crud-form').prop('target', '_blank');
        setTimeout(function() {
            $('#crud-form').prop('target', '_self');
         }, 1000);
    }
    if (result) $('form#crud-form').submit();
}

function crudSubmit() {
    if (submitType != SUBMIT_PRINT) {
        $('.spin-form').show();
        $('.btn').attr('disabled','disabled');
    }
    $('#submitType').val(submitType);
    return true;
}
