// AVTPL
var droplist = null;

function initUser() {
    droplist = $('#user-role');
    changeDrop(droplist);
    droplist.change(function(e){
        changeDrop(droplist);
    });
    $('.selectModalButton').click(function() {
        $('#selectModal').find('#selectModalHeader').html($(this).attr('modal-title'));
        $('#selectModal').find('#selectModalContent').html('<img src="' + $('#baseUrl').data('url') + 'images/spinner_60.gif" class="spinAuto60"/>').load($(this).attr('value'));
        // Necessary to hide it because crudModal is defined after selectModal (see why in index.php)
        $('#crudModal').modal('hide');
        $('#selectModal').modal('show');
    });
    $('#selectModal').find('.close').click(function() {
        // Necessary to hide/show it because crudModal is defined after selectModal (see why in index.php)
        $('#crudModal').modal('show');
    });
}

String.prototype.endsWith = function(suffix) {
    return this.substr(-suffix.length) === suffix;
};

function changeDrop(droplist) {
    // AVTPL EVO INI UserJsOnChangeDropChecks
    if (droplist.val().endsWith('-client')) {
        $('.cliente').show();
        hideFieldEntry('#user-nombre');
        if (!$('#user-id_empresa').val()) {
            $('#user-id_empresa').change();
        }
        $('#user-email').prop('readonly', true);
    } else {
        $('.cliente').hide();
        showFieldEntry('#user-nombre');
        $('#user-id_empresa').val(null);
        $('#user-id_contacto').val(null);
        $('#user-id_empresa').trigger('change');
        $('#user-id_contacto').trigger('change');
        $('#user-email').prop('readonly', false);
    }
    // AVTPL EVO END UserJsOnChangeDropChecks
}

// AVTPL EVO INI UserJsAdditionalFunctions
function onChangeEmpresa() {
    if (!$('#user-role').val().endsWith('-client')) return;
    $.post($('#baseUrl').data('url') + 'contacto/get-for-empresa?id_empresa=' + $('#user-id_empresa').val() + '&id=' + $('#user-id_contacto').val(), function(data) {
        $('select#user-id_contacto').html(data);
        onChangeContacto();
    });
}

function onChangeContacto() {
    if (!$('#user-role').val().endsWith('-client')) return;
    var id = Number($('#user-id_contacto').val());
    if (isNaN(id) || id == 0) {
        cleanContacto();
        return;
    }
    $.ajax({
        method: "GET",
        url: $('#baseUrl').data('url') + 'user/exists-contact',
        data: 'id_empresa=' + $('#user-id_empresa').val() + '&id_contacto=' + id,
        success: function(result) {
            if (result.exists) {
                $('#user-email').val('');
                alert('Ya existe un usuario para este contacto');
            } else {
                $.ajax({
                    method: "POST",
                    url: $('#baseUrl').data('url') + 'contacto/get-contacto',
                    data: 'id=' + id,
                    success: function(result) {
                        processSetContacto(result);
                    }
                });
            }
        }
    });
}

function cleanContacto() {
    $('#user-email').val('');
    $('#user-nombre').val('');
}

function processSetContacto(record) {
    $('#user-email').val(record.email);
    $('#user-nombre').val(record.nombre + ' ' + record.apellido);
}

function showFieldEntry(elementId, focus) {
    $(elementId).parent().parent().parent().prev().show();
    $(elementId).parent().parent().parent().show();
    if (focus) {
        $(elementId).select();
        $(elementId).focus();
    }
}

function hideFieldEntry(elementId, newValue) {
    if (typeof newValue !== 'undefined') $(elementId).val(newValue);
    $(elementId).parent().parent().parent().prev().hide();
    $(elementId).parent().parent().parent().hide();
}
// AVTPL EVO END UserJsAdditionalFunctions
