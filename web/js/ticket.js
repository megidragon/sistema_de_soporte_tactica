function onChangeProduct() {
	var id = $('#ticket-id_producto').val();
    $.post($('#baseUrl').data('url') + 'producto/get-producto?id=' + id, function(data) {
		$('#nro_serie').val(data.nro_serie);
		$('#fabricante').val(data.fabricante);
		$('#datos_red').val(data.datos_red);
		$('#posicion').val(data.posicion);
		$('#mac_address').val(data.mac_address);
		$('#prioridad_producto').val(data.idPrioridad.codigo);
    });
}

function textareaFitContent() {
    $.each($('textarea.form-control'), function () {
        $(this).height($(this)[0].scrollHeight - 10);
    });
}