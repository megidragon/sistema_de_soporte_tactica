// AVTPL
function getCurrentDateDMY(addMinutes) {
    // new Date().toJSON().slice(0,10)
    return getDatePlusMinutesDMY(new Date(), addMinutes);
}

function getCurrentDateYMD(addMinutes) {
    // new Date().toJSON().slice(0,10)
    return getDatePlusMinutesYMD(new Date(), addMinutes);
}

function getDatePlusMinutesYMD(currDate, addMinutes) {
    if (addMinutes != 0) {
        currDate = new Date(currDate.getTime() + addMinutes * 60000);
    }
    var dd = currDate.getDate();
    var mm = currDate.getMonth() + 1;
    var yyyy = currDate.getFullYear();
    return '' + yyyy + '-' + ('0' + mm).slice(-2) + '-' + ('0' + dd).slice(-2);
}

function getDatePlusMinutesDMY(currDate, addMinutes) {
    if (addMinutes != 0) {
        currDate = new Date(currDate.getTime() + addMinutes * 60000);
    }
    var fmt = new DateFormatter();
    return fmt.formatDate(currDate, 'd/m/Y');
}

function getCurrDatePlusDaysYMD(addDays) {
    var currDate = new Date();
    currDate = new Date(currDate.getTime() + addDays * 24 * 60 * 60000);
    var dd = currDate.getDate();
    var mm = currDate.getMonth() + 1;
    var yyyy = currDate.getFullYear();
    return '' + yyyy + '-' + ('0' + mm).slice(-2) + '-' + ('0' + dd).slice(-2);
}

function getCurrDatePlusDaysDMY(addDays) {
    var currDate = new Date();
    var fmt = new DateFormatter();
    return fmt.formatDate(currDate, 'd/m/Y');
}

function getDatePlusDaysYMD(currDate, addDays) {
    currDate = new Date(currDate.getTime() + addDays * 24 * 60 * 60000);
    var dd = currDate.getDate();
    var mm = currDate.getMonth() + 1;
    var yyyy = currDate.getFullYear();
    return '' + yyyy + '-' + ('0' + mm).slice(-2) + '-' + ('0' + dd).slice(-2);
}

function getDatePlusDaysDMY(currDate, addDays) {
    currDate = new Date(currDate.getTime() + addDays * 24 * 60 * 60000);
    var fmt = new DateFormatter();
    return fmt.formatDate(currDate, 'd/m/Y');
}

function getCurrDatePlusMonthsYMD(addMonths) {
    var currDate = new Date();
    var dd = currDate.getDate();
    var mm = ((currDate.getMonth() + addMonths) % 12) + 1;
    var yyyy = currDate.getFullYear() + Math.floor((currDate.getMonth() + addMonths) / 12);
    return '' + yyyy + '-' + ('0' + mm).slice(-2) + '-' + ('0' + dd).slice(-2);
}

function getCurrDatePlusMonthsDMY(addMonths) {
    var currDate = new Date();
    currDate.setMonth(currDate.getMonth() + addMonths); // setMonth(14) increments the year as well
    var fmt = new DateFormatter();
    return fmt.formatDate(currDate, 'd/m/Y');
}

function getCurrDatePlusYearsYMD(addYears) {
    var currDate = new Date();
    var dd = currDate.getDate();
    var mm = currDate.getMonth() + 1;
    var yyyy = currDate.getFullYear() + addYears;
    return '' + yyyy + '-' + ('0' + mm).slice(-2) + '-' + ('0' + dd).slice(-2);
}

function getCurrDatePlusYearsDMY(addYears) {
    var currDate = new Date();
    currDate.setFullYear(currDate.getFullYear() + addYears);
    var fmt = new DateFormatter();
    return fmt.formatDate(currDate, 'd/m/Y');
}

function getCurrentTime(addMinutes) {
    return getTimePlusMinutes(new Date(), addMinutes);
}

function getTimePlusMinutes(currTime, addMinutes) {
    if (addMinutes != 0) {
        currTime = new Date(currTime.getTime() + addMinutes * 60000);
    }
    var hh = currTime.getHours();
    var mm = currTime.getMinutes();
    return ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2);
}

function getTimeFromMillisecs(timeMillisecs) {
    currTime = new Date(timeMillisecs);
    var hh = currTime.getHours();
    var mm = currTime.getMinutes();
    return ('0' + hh).slice(-2) + ':' + ('0' + mm).slice(-2);
}

function getDateFromYMD(dateYMD) {
    var y = parseInt(dateYMD.substring(0, 4));
    var m = parseInt(dateYMD.substring(5, 7)) - 1;
    var d = parseInt(dateYMD.substring(8, 10));
    return new Date(y, m, d);
}

function getDateFromDMY(dateDMY) {
    var d = parseInt(dateDMY.substring(0, 2));
    var m = parseInt(dateDMY.substring(3, 5)) - 1;
    var y = parseInt(dateDMY.substring(6, 10));
    return new Date(y, m, d);
}

function formatDateDMY(dateYMD) {
    var dateObj = getDateFromYMD(dateYMD);
    var fmt = new DateFormatter();
    return fmt.formatDate(dateObj, 'd/m/Y');
}

function formatDateYMD(dateDMY) {
    var dateObj = getDateFromDMY(dateDMY);
    var fmt = new DateFormatter();
    return fmt.formatDate(dateObj, 'Y-m-d');
}
