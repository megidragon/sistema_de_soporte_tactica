#!/bin/bash

find . -type f -name \*.patch-ori | while read EACH
do
	ORINAME=`echo $EACH | sed 's/.patch-ori//'`
	echo "Moving $EACH => $ORINAME"
	mv $EACH $ORINAME
done

find . -type f -name \*.patch-del | while read EACH
do
	ORINAME=`echo $EACH | sed 's/.patch-del//'`
	echo "Removing $EACH and $ORINAME"
	rm $EACH $ORINAME
done
